declare module 'env' {
  const API_BASE_URL: string;
  const GOOGLE_CLIENT_ID: string;
}
