import React from 'react';
import { render, waitFor } from '@testing-library/react-native';

import Provider from '.';

describe('Provider', () => {
  it('renders correctly', async () => {
    const { queryByText } = render(<Provider>children</Provider>);
    await waitFor(() => expect(queryByText(/nothing/i)).toBeFalsy());
  });
});
