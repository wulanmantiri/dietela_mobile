import { ApiResponse } from 'services/api';
import {
  LoginRequest,
  LoginResponse,
  RegistrationRequest,
  LinkUserDataResponse,
  AuthUserResponse,
} from 'services/auth/models';

export interface iUserContext {
  user: AuthUserResponse;
  isAuthenticated: boolean;
  isLoading: boolean;
  isFirstLoading: boolean;
  setUser: (_: AuthUserResponse) => void;
  getUser: () => Promise<void>;
  signup: (
    data: RegistrationRequest,
  ) => ApiResponse<LoginResponse | LinkUserDataResponse>;
  login: (data: LoginRequest) => ApiResponse<LoginResponse>;
  loginWithGoogle: (_?: boolean) => Promise<void>;
  logout: () => Promise<void>;
}
