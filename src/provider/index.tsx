import React, { FC, useEffect } from 'react';

import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { GOOGLE_CLIENT_ID } from 'env';
import { set401Callback } from 'services/api';

import { UserContext, useUserContext } from './UserContext';

const ContextProvider: FC = ({ children }) => {
  const { getUser, logout, ...user } = useUserContext();

  useEffect(() => {
    GoogleSignin.configure({
      webClientId: GOOGLE_CLIENT_ID,
    });

    getUser();
    set401Callback(logout);
  }, [getUser, logout]);

  return (
    <UserContext.Provider value={{ getUser, logout, ...user }}>
      {children}
    </UserContext.Provider>
  );
};

export default ContextProvider;
export { UserContext };
