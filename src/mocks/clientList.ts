import { UserRole } from 'services/auth/models';
import { Client } from 'services/profiles/models';

export const mockClientList: Client[] = [
  {
    user: {
      id: 1,
      name: 'Doan Di Dinding',
      email: 'doan@dinding.com',
      role: UserRole.CLIENT,
    },
    diet_profile_id: 1,
    diet_questionnaire_id: 1,
    diet_recommendation_id: 1,
  },
];
