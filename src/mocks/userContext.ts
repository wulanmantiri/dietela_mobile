import { jest } from '@jest/globals';
import { iUserContext } from 'provider/UserContext/types';

export const mockUserContext: iUserContext = {
  user: {
    id: null,
    email: '',
    name: '',
    role: null,
    transaction_status: null,
    is_finished_onboarding: false,
    cart_id: null,
    nutritionist: null,
  },
  isAuthenticated: false,
  isLoading: false,
  isFirstLoading: false,
  setUser: jest.fn(),
  getUser: jest.fn(),
  signup: jest.fn(),
  login: jest.fn(),
  loginWithGoogle: jest.fn(),
  logout: jest.fn(),
};
