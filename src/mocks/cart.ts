import { Nutritionist } from 'services/nutritionists/models';

export const nutritionistDummy: Nutritionist = {
  id: 1,
  full_name_and_degree: 'Wendy',
  registration_certificate_no: '432491859',
  university: 'UGM',
  mastered_nutritional_problems: 'Manajemen berat badan, hipertensi',
  handled_age_group: '12 - 17 tahun (Remaja)',
  another_practice_place: 'RSIU',
  languages: 'Bahasa Indonesia, Jepang',
};
