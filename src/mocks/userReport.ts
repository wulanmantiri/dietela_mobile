import {
  UserReportRequest,
  UserReportResponse,
} from 'services/progress/models';
import { UserRole } from 'services/auth/models';

export const mockUserReportRequest: UserReportRequest = {
  weight: 155,
  height: 188,
  waist_size: 100,
  changes_felt: 1,
  hunger_level: 1,
  fullness_level: 1,
  heavy_meal: 1,
  snacks: 1,
  sweet_beverages: 1,
  sugary_ingredients: 1,
  fried_snacks: 1,
  umami_snacks: 1,
  sweet_snacks: 1,
  fruits_portion: 1,
  vegetables_portion: 1,
  water_consumption: 1,
  physical_activity: [1, 2],
  physical_activity_other: 'n',
  time_for_activity: 1,
  feeling_rating: 2,
  lesson_learned: 'a',
  problem_faced_and_feedbacks: 'x',
};

export const mockUserReportResponse: UserReportResponse = {
  id: 4,
  nutritionist: 1,
  week_num: 2,
  client: {
    id: 9,
    name: 'Shin Ryujin',
    email: 'ryujin@itzy.com',
    role: UserRole.CLIENT,
  },
  ...mockUserReportRequest,
};

export const mockUserReportHistory = {
  has_submitted_this_week: false,
  today_week: 14,
  data: [
    {
      ...mockUserReportResponse,
      week_num: 12,
    },
    {
      ...mockUserReportResponse,
      week_num: 2,
    },
    {
      ...mockUserReportResponse,
      week_num: 1,
    },
  ],
};
