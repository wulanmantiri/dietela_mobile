import { LoginResponse, UserRole } from 'services/auth/models';
import { TransactionStatus } from 'services/payment/models';

export const validRegistrationValues: { [_: string]: any } = {
  name: 'Doan Didinding',
  email: 'doan@dietela.com',
  password1: 'g8ake1afig',
  password2: 'g8ake1afig',
};

export const invalidRegistrationValues: { [_: string]: any } = {
  name: 'Doan Didinding',
  email: 'doan@dietela.com',
  password1: '12345678',
  password2: '12345678',
};

export const validLoginValues: { [_: string]: any } = {
  email: 'doan@dietela.com',
  password: 'g8ake1afig',
};

export const invalidLoginValues: { [_: string]: any } = {
  email: 'doan',
  password: '12345678',
};

export const authResponse: LoginResponse = {
  access_token: 'ax41faf',
  refresh_token: '9tka0kfa',
  user: {
    id: 1,
    email: validRegistrationValues.email,
    name: validRegistrationValues.name,
    role: UserRole.CLIENT,
    transaction_status: TransactionStatus.UNPAID,
    is_finished_onboarding: false,
    cart_id: 1,
  },
};
