import dayjs, { Dayjs } from 'dayjs';
import { API_BASE_URL } from 'env';

export const convertDate = (dateString?: string): string => {
  return dateString ? dayjs(dateString).format('D MMM YYYY') : '';
};

export const dateToString = (date: Date): string =>
  dayjs(date).format('YYYY-MM-DD');

export const getAbsoluteUrl = (path?: string | null) =>
  path ? API_BASE_URL + path : '';

export const getDateRange = (
  weekDiff: number,
): {
  mon: Dayjs;
  sun: Dayjs;
} => {
  return {
    mon: dayjs().day(1).subtract(weekDiff, 'week'),
    sun: dayjs().day(7).subtract(weekDiff, 'week'),
  };
};

export const cleanPhoneNumber = (phoneNumber: string) => {
  const num =
    phoneNumber[0] === '0' ? phoneNumber.replace(/0/, '62') : phoneNumber;
  return num.replace(/\+|-/g, '');
};
