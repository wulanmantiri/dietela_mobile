import React, { FC, ReactElement } from 'react';
import { render, RenderOptions } from '@testing-library/react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { useUserContext, UserContext } from 'provider/UserContext';
import { testNavigation } from 'constants/navigation';

const Stack = createStackNavigator();

interface TestProvider {
  route: string;
  params?: {
    routeParams?: any;
    userContext?: any;
  };
  children: ReactElement;
}

const TestProvider: FC<TestProvider> = ({ route, params, children }) => {
  const { routeParams, userContext } = params || {};

  const userProviderValues = {
    ...useUserContext(),
    ...userContext,
  };

  return (
    <UserContext.Provider value={userProviderValues}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName={route}>
          {testNavigation.map((nav, i) => (
            <Stack.Screen
              key={`nav${i}`}
              name={nav.name}
              component={route === nav.name ? () => children : nav.component}
              initialParams={routeParams}
              options={{
                title: nav.header,
                headerShown: Boolean(nav.header),
              }}
            />
          ))}
        </Stack.Navigator>
      </NavigationContainer>
    </UserContext.Provider>
  );
};

const customRender = (
  ui: ReactElement,
  route: string,
  params?: {
    routeParams?: any;
    userContext?: any;
  },
  options?: Omit<RenderOptions, 'queries'>,
) =>
  render(ui, {
    wrapper: () => (
      <TestProvider route={route} params={params}>
        {ui}
      </TestProvider>
    ),
    ...options,
  });

export * from '@testing-library/react-native';
export { customRender as render };
