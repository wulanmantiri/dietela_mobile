import { Linking } from 'react-native';
import { cleanPhoneNumber } from './format';
import Toast from 'react-native-toast-message';

export const redirectToWA = (phoneNumber?: string | null) => {
  if (phoneNumber) {
    const url = 'https://wa.me/' + cleanPhoneNumber(phoneNumber);
    Linking.openURL(url);
  } else {
    Toast.show({
      type: 'error',
      text1: 'Nomor WhatsApp tidak diketahui.',
      text2:
        'Orang yang dituju tidak memiliki nomor WhatsApp. Mohon hubungi CS kami jika terdapat kesalahan.',
    });
  }
};
