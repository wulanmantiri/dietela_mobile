import * as Yup from 'yup';
import { ReactNode } from 'react';

export enum FieldType {
  TEXT = 'text',
  EMAIL = 'email',
  RADIO_BUTTON = 'radiobutton',
  CHECKBOX = 'checkbox',
  PASSWORD = 'password',
  CONFIRM_PASSWORD = 'confirmpassword',
  NUMBER = 'number',
}

export interface FieldValidation {
  name: string;
  required?: boolean;
  label?: ReactNode;
  errorMessage?: string;
  type: FieldType;
  min?: number;
  max?: number;
  matches?: string;
}

export const generateValidationSchema = (fields: FieldValidation[]) => {
  const validationSchema: any = {};
  let tempYup;
  fields.forEach((field) => {
    switch (field.type) {
      case FieldType.TEXT:
        tempYup = Yup.string();
        validationSchema[field.name] = field.required
          ? tempYup.required(`${field.errorMessage || field.label} harus diisi`)
          : tempYup;
        break;
      case FieldType.EMAIL:
        validationSchema[field.name] = Yup.string()
          .email('Email tidak valid')
          .required('Email harus diisi');
        break;
      case FieldType.RADIO_BUTTON:
        validationSchema[field.name] = Yup.number().min(
          1,
          'Pilihan harus diisi',
        );
        break;
      case FieldType.CHECKBOX:
        validationSchema[field.name] = Yup.array().min(
          1,
          'Pilih semua yang berlaku',
        );
        break;
      case FieldType.PASSWORD:
        validationSchema[field.name] = Yup.string()
          .required('Password harus diisi')
          .min(8, 'Minimal 8 karakter');
        break;
      case FieldType.CONFIRM_PASSWORD:
        validationSchema[field.name] = Yup.string().oneOf(
          [Yup.ref(field.matches!), null],
          'Konfirmasi password tidak sama',
        );
        break;
      case FieldType.NUMBER:
        validationSchema[field.name] = Yup.number()
          .min(field.min || 0, `${field.label} harus minimal ${field.min || 0}`)
          .max(
            field.max || 200,
            `${field.label} harus maksimal ${field.max || 200}`,
          );
        break;
      default:
        break;
    }
  });
  return Yup.object().shape(validationSchema);
};

export const filterArr = (arr: any[], elem: any) =>
  arr.filter((item) => item !== elem);
