import AsyncStorage from '@react-native-async-storage/async-storage';
import { ReactNode } from 'react';

export const setCache = async (
  key: string,
  value: ReactNode,
  callback?: (error?: Error) => void,
) => await AsyncStorage.setItem(key, `${value}`, callback);

export const getCache = async (
  key: string,
  callback?: (error?: Error) => void,
) => await AsyncStorage.getItem(key, callback);

export const removeCache = async (
  key: string,
  callback?: (error?: Error) => void,
) => await AsyncStorage.removeItem(key, callback);
