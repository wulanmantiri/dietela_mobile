import React, { FC } from 'react';
import { View, StyleSheet } from 'react-native';

const Section: FC = ({ children }) => (
  <View style={styles.margin}>{children}</View>
);

const styles = StyleSheet.create({
  margin: {
    marginTop: 15,
  },
});

export default Section;
