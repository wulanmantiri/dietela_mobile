import React, { FC } from 'react';
import { View } from 'react-native';

import { styles } from './styles';

const Column: FC = ({ children }) => (
  <View style={styles.column}>{children}</View>
);

export default Column;
