import React, { FC } from 'react';
import { View } from 'react-native';

import { styles } from './styles';

const Row: FC = ({ children }) => <View style={styles.row}>{children}</View>;

export default Row;
