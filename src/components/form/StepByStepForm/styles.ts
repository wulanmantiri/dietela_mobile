import { StyleSheet } from 'react-native';
import { colors, typography } from 'styles';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    paddingHorizontal: 20,
  },
  bottomContainer: {
    marginTop: 20,
  },
  pageContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  notfilled: {
    color: colors.formLabel,
  },
  filled: {
    color: colors.textBlack,
    ...typography.headingMedium,
    fontSize: 18,
  },
  page: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 14,
    borderBottomColor: colors.border,
    borderBottomWidth: 1,
  },
  number: {
    ...typography.bodyLarge,
    flex: 0.1,
  },
  buttonContainer: {
    paddingLeft: 10,
    flex: 0.27,
  },
  md: {
    ...typography.bodyLarge,
    flex: 0.63,
  },
  button: {
    padding: 10,
    paddingHorizontal: 0,
    backgroundColor: colors.primaryYellow,
  },
  finishButton: {
    borderRadius: 20,
    width: 150,
    backgroundColor: colors.secondaryVariant,
  },
  buttonTitle: {
    color: colors.textBlack,
  },
});
