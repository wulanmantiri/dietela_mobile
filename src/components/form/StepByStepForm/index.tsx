import React, { FC } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, ScrollView } from 'react-native';
import { Button, Text, Icon } from 'react-native-elements';

import { styles } from './styles';
import { Props } from './types';
import { colors } from 'styles';

const StepByStepForm: FC<Props> = ({
  pages,
  currentPage,
  defaultValues,
  finishRedirectRoute,
}) => {
  const navigation = useNavigation();

  const getButton = (i: number, route: string) => {
    if (i === 0 && currentPage !== 0) {
      return <Icon name="lock" color={colors.primaryYellow} size={30} />;
    }
    let buttonLabel;
    if (i < currentPage) {
      buttonLabel = 'Ubah';
    } else if (i === currentPage) {
      buttonLabel = 'Isi';
    }
    return buttonLabel ? (
      <Button
        title={buttonLabel}
        onPress={() => navigation.navigate(route, defaultValues)}
        buttonStyle={styles.button}
        titleStyle={styles.buttonTitle}
      />
    ) : null;
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.pageContainer}>
        {pages.map((page, i) => (
          <View style={styles.page} key={i}>
            <Text
              style={[
                styles.number,
                currentPage < i ? styles.notfilled : styles.filled,
              ]}>
              {i + 1}
            </Text>
            <Text
              style={[
                styles.md,
                currentPage < i ? styles.notfilled : styles.filled,
              ]}>
              {page.name}
            </Text>
            <View style={styles.buttonContainer}>
              {getButton(i, page.route)}
            </View>
          </View>
        ))}
      </View>
      {finishRedirectRoute ? (
        <View style={styles.bottomContainer}>
          <Button
            title="Selesai"
            onPress={() =>
              navigation.reset({
                index: 0,
                routes: [{ name: finishRedirectRoute }],
              })
            }
            buttonStyle={styles.finishButton}
            titleStyle={styles.buttonTitle}
            disabled={currentPage !== pages.length}
          />
        </View>
      ) : null}
    </ScrollView>
  );
};

export default StepByStepForm;
