export interface Props {
  pages: {
    name: string;
    route: string;
  }[];
  currentPage: number;
  defaultValues?: any;
  finishRedirectRoute?: string;
}
