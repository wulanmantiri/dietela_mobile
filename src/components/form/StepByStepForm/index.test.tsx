import React from 'react';
import { fireEvent, render } from '@testing-library/react-native';

import StepByStepForm from '.';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: mockedNavigate,
      reset: mockedNavigate,
    }),
  };
});

describe('StepByStepForm component', () => {
  const pages = ['hai', 'hello', 'hei'];
  const props = {
    pages: pages.map((name) => ({
      name,
      route: name,
    })),
    currentPage: 0,
  };

  it('only has "Isi" button when form is never filled', () => {
    const { getByText, queryByText } = render(<StepByStepForm {...props} />);
    const nextButton = getByText(/Isi/i);
    expect(nextButton).toBeTruthy();
    expect(queryByText(/Ubah/i)).toBeFalsy();
  });

  it('has "Ubah" button when form has been filled', () => {
    const { queryByText } = render(
      <StepByStepForm {...props} currentPage={2} />,
    );
    expect(queryByText(/Ubah/i)).toBeTruthy();
  });

  it('redirects to designated route when button "Ubah" or "Isi" is pressed', () => {
    const { getByText } = render(<StepByStepForm {...props} currentPage={1} />);
    const nextButton = getByText(/Isi/i);
    expect(nextButton).toBeTruthy();
    fireEvent.press(nextButton);

    expect(mockedNavigate).toHaveBeenCalled();
  });

  it('redirects to finish route when button "Selesai" is pressed', () => {
    const { getByText } = render(
      <StepByStepForm
        {...props}
        currentPage={pages.length}
        finishRedirectRoute="finish"
      />,
    );
    const finishButton = getByText(/Selesai/i);
    expect(finishButton).toBeTruthy();
    fireEvent.press(finishButton);

    expect(mockedNavigate).toHaveBeenCalled();
  });
});
