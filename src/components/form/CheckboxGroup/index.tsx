import React, { FC } from 'react';
import { View } from 'react-native';
import { CheckBox, Text, Input } from 'react-native-elements';
import { typographyStyles } from 'styles';

import { styles } from './styles';
import { Props } from './types';
import FormLabel from '../FormLabel';

const CheckboxGroup: FC<Props> = ({
  label,
  required,
  choices,
  value,
  onChange,
  hasOtherChoice,
  otherChoiceValue,
  otherValue,
  setOtherValue,
  errorMessage,
}) => {
  const handlePress = (choiceValue?: string | number) => {
    if (value.includes(choiceValue)) {
      onChange(value.filter((item: number | string) => item !== choiceValue));
    } else {
      onChange([...value, choiceValue]);
    }
  };

  return (
    <View>
      <FormLabel label={label} required={required} />
      <Text style={[typographyStyles.bodySmall, styles.helperText]}>
        Dapat memilih lebih dari satu
      </Text>
      {choices.map((choice) => (
        <CheckBox
          key={choice.value}
          title={choice.label}
          checked={value.includes(choice.value)}
          onPress={() => handlePress(choice.value)}
        />
      ))}
      {hasOtherChoice ? (
        <CheckBox
          title={
            <View style={styles.otherContainer}>
              <Text style={styles.textStyle}>Lainnya:</Text>
              <Input
                onChangeText={setOtherValue}
                value={otherValue}
                inputContainerStyle={styles.inputContainerStyle}
                labelStyle={styles.removed}
                errorStyle={styles.removed}
              />
            </View>
          }
          checked={value.includes(otherChoiceValue)}
          onPress={() => handlePress(otherChoiceValue)}
        />
      ) : null}
      {errorMessage ? (
        <Text style={[typographyStyles.caption, styles.red]}>
          {errorMessage}
        </Text>
      ) : null}
    </View>
  );
};

export default CheckboxGroup;
