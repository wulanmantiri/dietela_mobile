import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import CheckboxGroup from '.';

describe('CheckboxGroup component', () => {
  const props = {
    choices: [
      {
        label: 'Choice 1',
        value: 1,
      },
      {
        label: 'Choice 2',
        value: 2,
      },
    ],
  };

  it('is able to select multiple', () => {
    let value: Array<string | number> = [];

    const { getByText } = render(
      <CheckboxGroup
        {...props}
        value={value}
        onChange={(v: typeof value) => (value = v)}
      />,
    );

    expect(value).toStrictEqual([]);

    fireEvent.press(getByText(/Choice 1/i));
    expect(value).toContain(1);

    fireEvent.press(getByText(/Choice 2/i));
    expect(value).toContain(2);
  });

  it('is able to unselect checkbox', () => {
    let value: Array<string | number> = [2];

    const { getByText } = render(
      <CheckboxGroup
        {...props}
        errorMessage="some error"
        value={value}
        onChange={(v: typeof value) => (value = v)}
      />,
    );

    expect(value).toContain(2);

    fireEvent.press(getByText(/Choice 2/i));
    expect(value).toStrictEqual([]);
  });

  it('shows other options when hasOtherChoice prop is true', () => {
    let value: Array<string | number> = [];

    const { getByText } = render(
      <CheckboxGroup
        {...props}
        value={value}
        onChange={(v: typeof value) => (value = v)}
        hasOtherChoice
        otherChoiceValue={1}
      />,
    );

    const otherChoice = getByText(/Lainnya/i);
    expect(otherChoice).toBeTruthy();

    fireEvent.press(otherChoice);
    expect(value).toContain(1);
  });
});
