import { StyleSheet } from 'react-native';
import { colors, typography } from 'styles';

export const styles = StyleSheet.create({
  helperText: {
    color: colors.formLabel,
    marginBottom: 6,
  },
  otherContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 12,
    width: '76%',
  },
  textStyle: {
    fontWeight: '400',
    ...typography.bodyMedium,
    marginRight: 12,
  },
  inputContainerStyle: {
    height: 22,
  },
  removed: {
    height: 0,
    marginVertical: 0,
    marginHorizontal: 0,
    paddingVertical: 0,
  },
  red: {
    color: 'red',
    marginTop: 4,
  },
});
