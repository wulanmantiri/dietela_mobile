import { Choice } from '../MultipleChoice/types';
import { Props as FormLabelProps } from '../FormLabel/types';

export interface Props extends FormLabelProps {
  choices: Choice[];
  value: any;
  onChange: (_: any) => void;
  hasOtherChoice?: boolean;
  otherChoiceValue?: number | string;
  otherValue?: string;
  setOtherValue?: (_: string | number) => void;
  errorMessage?: string;
}
