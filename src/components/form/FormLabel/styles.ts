import { StyleSheet } from 'react-native';
import { typography } from 'styles';

export const styles = StyleSheet.create({
  red: {
    color: 'red',
  },
  label: {
    color: 'black',
    marginBottom: 4,
    ...typography.bodyMedium,
  },
});
