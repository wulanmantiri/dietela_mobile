import React, { FC } from 'react';
import { Text } from 'react-native-elements';

import { styles } from './styles';
import { Props } from './types';

const FormLabel: FC<Props> = ({ label, required }) => (
  <Text style={styles.label}>
    {label}
    {required ? <Text style={styles.red}> *</Text> : null}
  </Text>
);

export default FormLabel;
