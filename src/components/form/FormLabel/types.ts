export interface Props {
  label?: string;
  required?: boolean;
}
