import { Choice } from '../MultipleChoice/types';
import { Props as FormLabelProps } from '../FormLabel/types';

export interface Props extends FormLabelProps {
  placeholder?: string;
  errorMessage?: any;
  choices: Choice[];
  value: any;
  onChange: (_: any) => void;
}
