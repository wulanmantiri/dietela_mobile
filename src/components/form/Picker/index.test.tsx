import React from 'react';
import { render } from '@testing-library/react-native';

import Picker from '.';

describe('Picker component', () => {
  let value: any = null;

  const props = {
    choices: [
      {
        label: 'Choice 1',
        value: 1,
      },
      {
        label: 'Choice 2',
        value: 2,
      },
    ],
    value: value,
    onChange: (v: any) => (value = v),
  };

  it('renders correctly', () => {
    render(<Picker {...props} />);
  });

  it('renders correctly with error message', () => {
    render(<Picker {...props} errorMessage="error" />);
  });
});
