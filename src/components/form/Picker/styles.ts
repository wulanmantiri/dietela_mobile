import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  red: {
    color: 'red',
    marginLeft: 4,
    marginTop: 4,
  },
  container: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.border,
  },
  placeholder: {
    color: colors.formLabel,
  },
});
