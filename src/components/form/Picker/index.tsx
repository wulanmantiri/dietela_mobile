import React, { FC } from 'react';
import { Picker as RNPicker } from '@react-native-picker/picker';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

import { typographyStyles } from 'styles';

import { styles } from './styles';
import { Props } from './types';
import FormLabel from '../FormLabel';

const Picker: FC<Props> = ({
  label,
  required,
  placeholder,
  errorMessage,
  choices,
  value,
  onChange,
}) => (
  <View>
    <FormLabel label={label} required={required} />
    <View style={styles.container}>
      <RNPicker selectedValue={value} onValueChange={(v, _) => onChange(v)}>
        <RNPicker.Item
          label={placeholder || `Pilih ${label?.toLowerCase()}`}
          value={0}
          style={styles.placeholder}
        />
        {choices.map((choice) => (
          <RNPicker.Item
            key={choice.value}
            label={choice.label}
            value={choice.value}
          />
        ))}
      </RNPicker>
    </View>
    {errorMessage ? (
      <Text style={[typographyStyles.caption, styles.red]}>{errorMessage}</Text>
    ) : null}
  </View>
);

export default Picker;
