import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import MultipleChoice from '.';

describe('MultipleChoice component', () => {
  let value: string | number | null = null;

  const props = {
    questionNumber: 1,
    questionLabel: 'Question',
    totalQuestions: 2,
    choices: [
      {
        label: 'Choice 1',
        value: 1,
      },
      {
        label: 'Choice 2',
        value: 2,
      },
    ],
    value: value,
    onChange: (v: typeof value) => (value = v),
  };

  it('only selects one choice at a time', () => {
    const { getByText } = render(<MultipleChoice {...props} />);

    fireEvent.press(getByText(/Choice 1/i));
    expect(value).toEqual(1);

    fireEvent.press(getByText(/Choice 2/i));
    expect(value).toEqual(2);
  });

  it('shows red helper text when there is error message props', () => {
    const { getByText } = render(
      <MultipleChoice {...props} errorMessage="error" />,
    );

    const helperText = getByText(/Pilih satu yang paling cocok/i);
    expect(helperText).toBeTruthy();
  });
});
