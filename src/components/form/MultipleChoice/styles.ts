import { StyleSheet } from 'react-native';
import { typography } from 'styles';

export const styles = StyleSheet.create({
  spacing: {
    marginBottom: 14,
  },
  bigSpacing: {
    marginBottom: 24,
  },
  red: {
    color: 'red',
  },
  textStyle: {
    fontWeight: '500',
    color: 'black',
    ...typography.bodyLarge,
    paddingRight: 14,
  },
});
