import React, { FC } from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import { typographyStyles } from 'styles';

import RadioButton from '../RadioButton';
import { styles } from './styles';
import { Props } from './types';

const MultipleChoice: FC<Props> = ({
  questionNumber,
  totalQuestions,
  questionLabel,
  helperText,
  errorMessage,
  choices,
  value,
  onChange,
}) => (
  <View>
    <Text style={[typographyStyles.overlineBig, styles.spacing]}>
      Pertanyaan {questionNumber} / {totalQuestions}
    </Text>
    <Text style={[typographyStyles.headingLarge, styles.spacing]}>
      {questionLabel} <Text style={styles.red}>*</Text>
    </Text>
    <Text
      style={[
        typographyStyles.bodyMedium,
        styles.bigSpacing,
        errorMessage ? styles.red : null,
      ]}>
      {helperText || 'Pilih satu yang paling cocok'}
    </Text>
    {choices.map((choice) => (
      <RadioButton
        key={choice.value}
        title={choice.label}
        checked={value === choice.value}
        onPress={() => onChange(choice.value)}
        textStyle={styles.textStyle}
      />
    ))}
  </View>
);

export default MultipleChoice;
