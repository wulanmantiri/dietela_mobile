import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import MultipleCheckbox from '.';

describe('MultipleCheckbox component', () => {
  const props = {
    questionNumber: 1,
    questionLabel: 'Question',
    totalQuestions: 2,
    choices: [
      {
        label: 'Choice 1',
        value: 1,
      },
      {
        label: 'Choice 2',
        value: 2,
      },
    ],
  };

  it('is able to select multiple', () => {
    let value: Array<string | number> = [];

    const { getByText } = render(
      <MultipleCheckbox
        {...props}
        value={value}
        onChange={(v: typeof value) => (value = v)}
      />,
    );

    expect(value).toStrictEqual([]);

    fireEvent.press(getByText(/Choice 1/i));
    expect(value).toContain(1);

    fireEvent.press(getByText(/Choice 2/i));
    expect(value).toContain(2);
  });

  it('is able to unselect checkbox', () => {
    let value: Array<string | number> = [2];

    const { getByText } = render(
      <MultipleCheckbox
        {...props}
        value={value}
        onChange={(v: typeof value) => (value = v)}
      />,
    );

    expect(value).toContain(2);

    fireEvent.press(getByText(/Choice 2/i));
    expect(value).toStrictEqual([]);
  });

  it('shows red helper text when there is error message props', () => {
    const { getByText } = render(
      <MultipleCheckbox
        {...props}
        value={[]}
        onChange={jest.fn()}
        errorMessage="error"
      />,
    );

    const helperText = getByText(/Pilih semua yang berlaku/i);
    expect(helperText).toBeTruthy();
  });
});
