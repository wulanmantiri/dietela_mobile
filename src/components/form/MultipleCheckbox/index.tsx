import React, { FC } from 'react';
import { View } from 'react-native';
import { CheckBox, Text } from 'react-native-elements';
import { typographyStyles } from 'styles';

import { styles } from '../MultipleChoice/styles';
import { Props, Choice } from '../MultipleChoice/types';

const MultipleCheckbox: FC<Props> = ({
  questionNumber,
  totalQuestions,
  questionLabel,
  helperText,
  errorMessage,
  choices,
  value,
  onChange,
}) => {
  const handlePress = (choice: Choice) => {
    if (value.includes(choice.value)) {
      onChange(value.filter((item: number) => item !== choice.value));
    } else {
      onChange([...value, choice.value]);
    }
  };

  return (
    <View>
      <Text style={[typographyStyles.overlineBig, styles.spacing]}>
        Pertanyaan {questionNumber} / {totalQuestions}
      </Text>
      <Text style={[typographyStyles.headingLarge, styles.spacing]}>
        {questionLabel}
      </Text>
      <Text
        style={[
          typographyStyles.bodyMedium,
          styles.bigSpacing,
          errorMessage ? styles.red : null,
        ]}>
        {helperText || 'Pilih semua yang berlaku (opsional)'}
      </Text>
      {choices.map((choice) => (
        <CheckBox
          key={choice.value}
          title={choice.label}
          checked={value.includes(choice.value)}
          onPress={() => handlePress(choice)}
          textStyle={styles.textStyle}
        />
      ))}
    </View>
  );
};

export default MultipleCheckbox;
