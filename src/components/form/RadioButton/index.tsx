import React, { FC } from 'react';
import { View } from 'react-native';
import { Text, CheckBox, CheckBoxProps } from 'react-native-elements';

import { styles } from './styles';
import FormLabel from '../FormLabel';
import { RadioButtonGroupProps } from './types';
import { typographyStyles } from 'styles';

const RadioButton: FC<CheckBoxProps> = (props) => (
  <CheckBox {...props} checkedIcon="dot-circle-o" uncheckedIcon="circle-o" />
);

export const RadioButtonGroup: FC<RadioButtonGroupProps> = ({
  choices,
  label,
  required,
  errorMessage,
  value,
  onChange,
}) => (
  <View>
    <FormLabel label={label} required={required} />
    {choices.map((choice) => (
      <RadioButton
        key={choice.value}
        title={choice.label}
        checked={value === choice.value}
        onPress={() => onChange(choice.value)}
      />
    ))}
    {errorMessage ? (
      <Text style={[typographyStyles.caption, styles.red]}>{errorMessage}</Text>
    ) : null}
  </View>
);

export default RadioButton;
