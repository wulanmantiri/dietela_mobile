import { Props as FormLabelProps } from '../FormLabel/types';
import { Choice } from '../MultipleChoice/types';

export interface RadioButtonGroupProps extends FormLabelProps {
  choices: Choice[];
  errorMessage?: any;
  value: any;
  onChange: (_: any) => void;
}
