import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  spacing: {
    marginBottom: 24,
  },
  red: {
    color: 'red',
    marginLeft: 4,
    marginTop: 4,
  },
  container: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.border,
    paddingHorizontal: 16,
    paddingVertical: 12,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  placeholder: {
    color: colors.formLabel,
  },
});
