import { Props as FormLabelProps } from '../FormLabel/types';

export interface Props extends FormLabelProps {
  placeholder?: string;
  errorMessage?: any;
  value: any;
  onChange: (_: any) => void;
}
