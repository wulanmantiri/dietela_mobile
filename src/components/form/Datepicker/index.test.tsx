import React from 'react';
import { render } from '@testing-library/react-native';

import Datepicker from '.';

describe('Datepicker component', () => {
  let value: Date = new Date();

  const props = {
    value,
    onChange: (v: Date) => (value = v),
  };

  it('renders correctly', () => {
    render(<Datepicker {...props} />);
  });

  it('renders correctly with error message', () => {
    render(<Datepicker {...props} errorMessage="error" />);
  });

  it('opens dialog when pressed', () => {
    const { getByTestId } = render(<Datepicker {...props} />);

    const datepickerButton = getByTestId('datepickertrigger');
    expect(datepickerButton).toBeTruthy();
  });
});
