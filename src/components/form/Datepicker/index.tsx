import React, { FC, useState } from 'react';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import { View, TouchableOpacity } from 'react-native';
import { Text, Icon } from 'react-native-elements';
import { typographyStyles } from 'styles';

import { styles } from './styles';
import { Props } from './types';
import FormLabel from '../FormLabel';
import { convertDate } from 'utils/format';

const Datepicker: FC<Props> = ({
  label,
  required,
  errorMessage,
  value,
  onChange,
}) => {
  const [show, setShow] = useState(false);

  const onPress = () => {
    setShow(true);
  };

  const handleChange = (_: any, date?: Date) => {
    setShow(false);
    onChange(date);
  };

  return (
    <View style={styles.spacing}>
      <FormLabel label={label} required={required} />
      <TouchableOpacity
        style={styles.container}
        onPress={onPress}
        testID="datepickertrigger">
        <Text style={typographyStyles.bodyMedium}>{convertDate(value)}</Text>
        <Icon name="calendar" type="material-community" />
      </TouchableOpacity>
      {errorMessage ? (
        <Text style={[typographyStyles.caption, styles.red]}>
          {errorMessage}
        </Text>
      ) : null}
      {show ? (
        <RNDateTimePicker
          value={new Date(value)}
          onChange={handleChange}
          locale="id-ID"
        />
      ) : null}
    </View>
  );
};

export default Datepicker;
