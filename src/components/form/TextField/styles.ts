import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  red: {
    color: 'red',
  },
  smallMargin: {
    height: 10,
  },
  bigMargin: {
    height: 30,
  },
  inputContainerStyle: {
    borderColor: colors.border,
    borderWidth: 1,
    borderRadius: 4,
    paddingHorizontal: 10,
  },
});
