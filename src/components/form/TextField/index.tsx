import React, { FC } from 'react';
import { Input } from 'react-native-elements';

import { styles } from './styles';
import { Props } from './types';
import FormLabel from '../FormLabel';

const TextField: FC<Props> = ({ label, required, errorMessage, ...props }) => (
  <Input
    {...props}
    label={<FormLabel label={label} required={required} />}
    errorStyle={[
      styles.red,
      errorMessage ? styles.bigMargin : styles.smallMargin,
    ]}
    errorMessage={errorMessage}
    inputContainerStyle={styles.inputContainerStyle}
  />
);

export default TextField;
