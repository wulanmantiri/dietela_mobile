import { InputProps } from 'react-native-elements';

export interface Props extends InputProps {
  label?: string;
  required?: boolean;
  errorMessage?: any;
}
