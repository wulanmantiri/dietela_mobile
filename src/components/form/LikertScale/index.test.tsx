import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import LikertScale from '.';
import RadioButton from '../RadioButton';

describe('LikertScale component', () => {
  let value: any = null;

  const props = {
    choices: [
      {
        label: 'Choice 1',
        value: 1,
      },
      {
        label: 'Choice 2',
        value: 2,
      },
    ],
    value: value,
    onChange: (v: any) => (value = v),
  };

  it('only selects one choice at a time', () => {
    const { UNSAFE_getAllByType } = render(
      <LikertScale
        {...props}
        helperText="helper"
        scaleDescription={{ lowest: 'hai', highest: 'hello' }}
      />,
    );

    const buttons = UNSAFE_getAllByType(RadioButton);
    fireEvent.press(buttons[0]);
    expect(value).toEqual(1);

    fireEvent.press(buttons[1]);
    expect(value).toEqual(2);
  });

  it('shows error message when there is error message props', () => {
    const errorMessage = 'error';
    const { getByText } = render(
      <LikertScale {...props} errorMessage={errorMessage} />,
    );

    const errorText = getByText(errorMessage);
    expect(errorText).toBeTruthy();
  });

  it('shows middle scale description', () => {
    const { getByText } = render(
      <LikertScale
        {...props}
        scaleDescription={{ lowest: 'hai', middle: 'hi', highest: 'hello' }}
      />,
    );

    const middleText = getByText(/hi/i);
    expect(middleText).toBeTruthy();
  });
});
