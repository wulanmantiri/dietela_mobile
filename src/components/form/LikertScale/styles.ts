import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  helperText: {
    color: colors.formLabel,
    marginBottom: 6,
  },
  red: {
    color: 'red',
    marginTop: 4,
  },
  buttonGroup: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
  },
  textGroup: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginTop: 6,
  },
  radioText: {
    textAlign: 'center',
  },
});
