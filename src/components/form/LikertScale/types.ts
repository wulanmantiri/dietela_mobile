import { Choice } from '../MultipleChoice/types';
import { Props as FormLabelProps } from '../FormLabel/types';

export interface Props extends FormLabelProps {
  errorMessage?: any;
  helperText?: string;
  choices: Choice[];
  value: any;
  onChange: (_: any) => void;
  scaleDescription?: {
    lowest: string;
    middle?: string;
    highest: string;
  };
}
