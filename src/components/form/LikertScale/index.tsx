import React, { FC } from 'react';

import { View } from 'react-native';
import { typographyStyles } from 'styles';
import { Text } from 'react-native-elements';

import { styles } from './styles';
import { Props } from './types';
import FormLabel from '../FormLabel';
import RadioButton from '../RadioButton';

const LikertScale: FC<Props> = ({
  label,
  required,
  errorMessage,
  helperText,
  scaleDescription,
  choices,
  value,
  onChange,
}) => (
  <View>
    <FormLabel label={label} required={required} />
    {helperText ? (
      <Text style={[typographyStyles.bodySmall, styles.helperText]}>
        {helperText}
      </Text>
    ) : null}
    {scaleDescription ? (
      <Text style={[typographyStyles.bodySmall, styles.helperText]}>
        {choices[0].label}: {scaleDescription.lowest} {'\n'}
        {scaleDescription.middle ? scaleDescription.middle + '\n' : ''}
        {choices[choices.length - 1].label}: {scaleDescription.highest}
      </Text>
    ) : null}
    <View style={styles.textGroup}>
      {choices.map((choice) => (
        <View style={{ flex: 1 / choices.length }} key={`text${choice.value}`}>
          <Text style={styles.radioText}>{choice.label}</Text>
        </View>
      ))}
    </View>
    <View style={styles.buttonGroup}>
      {choices.map((choice) => (
        <View
          style={{ flex: 1 / choices.length }}
          key={`button${choice.value}`}>
          <RadioButton
            key={choice.value}
            checked={value === choice.value}
            onPress={() => onChange(choice.value)}
            center
          />
        </View>
      ))}
    </View>
    {errorMessage ? (
      <Text style={[typographyStyles.caption, styles.red]}>{errorMessage}</Text>
    ) : null}
  </View>
);

export default LikertScale;
