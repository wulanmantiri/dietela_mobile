import React, { FC } from 'react';
import { Text } from 'react-native-elements';
import { typographyStyles } from 'styles';

const BodyMedium: FC<{ text: string }> = ({ text }) => (
  <Text style={typographyStyles.bodyMedium}>{text}</Text>
);

export default BodyMedium;
