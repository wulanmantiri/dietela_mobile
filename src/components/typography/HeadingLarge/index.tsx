import React, { FC } from 'react';
import { Text } from 'react-native-elements';
import { typographyStyles } from 'styles';

const HeadingLarge: FC<{ text: string }> = ({ text }) => (
  <Text style={typographyStyles.headingLarge}>{text}</Text>
);

export default HeadingLarge;
