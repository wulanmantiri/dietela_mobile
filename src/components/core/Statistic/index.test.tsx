import React from 'react';
import { render } from '@testing-library/react-native';

import Statistic from '.';

describe('Statistic', () => {
  it('renders correctly', () => {
    render(<Statistic title="berat badan" emote="⚖️" content="77 kg" />);
  });
});
