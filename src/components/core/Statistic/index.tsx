import { Column } from 'components/layout';
import React, { FC } from 'react';
import { View, Text } from 'react-native';
import { typographyStyles } from 'styles';
import { styles } from './styles';

interface Props {
  title?: string;
  emote: string;
  content: string;
  testID?: string;
}

const Statistic: FC<Props> = ({ title, emote, content, testID }) => {
  return (
    <View testID={testID}>
      {title && (
        <Text style={[typographyStyles.overlineBig, styles.title]}>
          {title}
        </Text>
      )}
      <View>
        <Column>
          <View style={styles.row}>
            <Text style={[typographyStyles.bodyMedium, styles.emote]}>
              {emote}
            </Text>
            <Text style={typographyStyles.bodyMedium}>{content}</Text>
          </View>
        </Column>
      </View>
    </View>
  );
};

export default Statistic;
