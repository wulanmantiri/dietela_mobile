import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  title: {
    marginBottom: 4,
  },
  contentContainer: {
    flexDirection: 'row',
  },
  emote: {
    marginRight: 8,
  },
  row: {
    flexDirection: 'row',
    width: '90%',
  },
});
