import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  card: {
    borderRadius: 8,
    padding: 15,
    backgroundColor: colors.neutralLight,
  },
});
