import React, { FC } from 'react';
import { View, Text } from 'react-native';
import { typographyStyles } from 'styles';

import { styles } from './styles';

interface Props {
  content: string;
  testID?: string;
}

const InfoCard: FC<Props> = ({ content, testID }) => {
  return (
    <View style={[styles.card]} testID={testID}>
      <Text style={typographyStyles.bodyMedium}>{content}</Text>
    </View>
  );
};

export default InfoCard;
