import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  titleStyle: { color: colors.textBlack, textTransform: 'capitalize' },
  containerStyle: {
    padding: 12,
    overflow: 'hidden',
    borderRadius: 4,
    backgroundColor: colors.buttonYellow,
  },
});
