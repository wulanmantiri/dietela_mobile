import React, { FC } from 'react';
import { Button } from 'react-native-elements';

import { styles } from './styles';
import { Props } from './types';
import { typographyStyles } from 'styles';

const BigButton: FC<Props> = ({
  title,
  onPress,
  disabled,
  loading,
  testID,
}) => (
  <Button
    titleStyle={[typographyStyles.bodyMedium, styles.titleStyle]}
    disabled={disabled}
    buttonStyle={styles.containerStyle}
    onPress={onPress}
    title={title}
    loading={loading}
    testID={testID}
  />
);

export default BigButton;
