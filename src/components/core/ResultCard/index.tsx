import React, { FC } from 'react';
import { View, Text } from 'react-native';

import { typographyStyles } from 'styles';
import { styles } from './styles';

interface Props {
  infos: { label: string; content: string }[];
  status: 'healthy' | 'warning' | 'danger' | 'catastrophe';
  testID?: string;
}

const ResultCard: FC<Props> = ({ infos, status, testID }) => {
  const getColorFromStatus = (cardStatus: string) => {
    if (cardStatus === 'healthy') {
      return '#1DD1A1';
    } else if (status === 'warning') {
      return '#FF9A3E';
    } else if (status === 'danger') {
      return '#F44771';
    } else {
      return '#980000';
    }
  };

  const cardColor = getColorFromStatus(status);

  const infoPairs = infos.reduce(function (result, value, index, array) {
    if (index % 2 === 0) {
      result.push(array.slice(index, index + 2));
    }
    return result;
  }, []);

  return (
    <View style={[styles.card, { backgroundColor: cardColor }]} testID={testID}>
      {infoPairs.map((pair, ii) => (
        <View style={styles.row} key={ii}>
          {pair.map((info, jj) => {
            const padding = jj % 2 === 0 ? styles.leftSide : styles.rightSide;

            return (
              <View style={[styles.content, padding]} key={info.label}>
                <Text style={[typographyStyles.overlineBig, styles.text]}>
                  {info.label}
                </Text>
                <Text
                  style={[
                    typographyStyles.headingMedium,
                    styles.text,
                    styles.marginTop,
                  ]}>
                  {info.content}
                </Text>
              </View>
            );
          })}
        </View>
      ))}
    </View>
  );
};

export default ResultCard;
