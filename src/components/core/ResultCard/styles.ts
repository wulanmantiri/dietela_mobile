import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  card: {
    borderRadius: 8,
  },
  row: {
    padding: 15,
    flexDirection: 'row',
  },
  content: {
    flex: 1,
  },
  text: {
    color: colors.neutralLight,
  },
  marginTop: {
    marginTop: 4,
  },
  leftSide: {
    flex: 1,
    paddingRight: 10,
  },
  rightSide: {
    flex: 1,
    paddingLeft: 10,
  },
});
