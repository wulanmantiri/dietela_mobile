import React from 'react';
import { render } from '@testing-library/react-native';

import ResultCard from '.';

describe('ResultCard', () => {
  const info = [
    {
      label: 'Energi per hari',
      content: '1953 kkal',
    },
    {
      label: 'Protein per hari',
      content: '23 kkal',
    },
  ];

  it('renders correctly when healthy', () => {
    render(<ResultCard status="healthy" infos={info} />);
  });

  it('renders correctly when warning', () => {
    render(<ResultCard status="warning" infos={info} />);
  });

  it('renders correctly when danger', () => {
    render(<ResultCard status="danger" infos={info} />);
  });
});
