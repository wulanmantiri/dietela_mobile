import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import LogoutButton from '.';

describe('LogoutButton', () => {
  const userContextMock = {
    user: {
      id: 1,
    },
    isAuthenticated: true,
    logout: jest.fn(),
  };

  it('renders nothing when user is not authenticated', () => {
    render(<LogoutButton />, ROUTES.initial);
  });

  it('renders correctly', () => {
    render(<LogoutButton />, ROUTES.checkout, { userContext: userContextMock });
  });

  it('calls logout and redirects to initial page when clicked', async () => {
    const { getByTestId, queryByText } = render(
      <LogoutButton />,
      ROUTES.checkout,
      {
        userContext: userContextMock,
      },
    );

    const logoutButton = getByTestId('logoutButton');
    expect(logoutButton).toBeTruthy();
    await waitFor(() => fireEvent.press(logoutButton));

    expect(queryByText(/konsultasi sekarang/i)).toBeTruthy();
  });
});
