import React, { FC, useContext } from 'react';
import { UserContext } from 'provider';
import { Button, Icon } from 'react-native-elements';
import { StyleSheet, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import * as ROUTES from 'constants/routes';

const LogoutButton: FC<{
  tintColor?: string;
}> = () => {
  const { logout, isAuthenticated } = useContext(UserContext);
  const navigation = useNavigation();

  const handlePress = async () => {
    await logout();
    navigation.reset({
      index: 0,
      routes: [{ name: ROUTES.initial }],
    });
  };

  return isAuthenticated ? (
    <Button
      icon={<Icon name="logout" type="material" />}
      buttonStyle={styles.button}
      onPress={handlePress}
      testID="logoutButton"
    />
  ) : (
    <View />
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#fff',
  },
});

export default LogoutButton;
