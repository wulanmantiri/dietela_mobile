import React from 'react';
import { render } from '@testing-library/react-native';

import ClientCardNutritionist from '.';

describe('ClientCardNutritionist', () => {
  it('renders correctly', () => {
    render(
      <ClientCardNutritionist
        clientName="test"
        onPressClientChat={() => {}}
        onPressClientDietReport={() => {}}
        onPressClientProfile={() => {}}
      />,
    );
  });

  it('renders correctly when given a testID', () => {
    render(
      <ClientCardNutritionist
        onPressClientChat={() => {}}
        onPressClientDietReport={() => {}}
        onPressClientProfile={() => {}}
        clientName="test"
        testID="clientCard"
      />,
    );
  });
});
