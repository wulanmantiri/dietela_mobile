import React, { FC } from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

import { typographyStyles } from 'styles';

import { styles } from './styles';
import { Props } from './types';
import DietReportButton from '../DietReportButton';
import DietProfileButton from '../DietProfileButton';
import ChatButton from '../ChatButton';

const ClientCardNutritionist: FC<Props> = ({
  clientName,
  onPressClientProfile,
  onPressClientDietReport,
  onPressClientChat,
}) => {
  return (
    <View style={styles.cardContainer}>
      <Text
        style={[typographyStyles.headingMedium, styles.spacing]}
        onPress={onPressClientProfile}>
        {clientName}
      </Text>
      <DietReportButton
        onPress={onPressClientDietReport}
        testID="dietReportButton"
      />
      <View style={styles.buttonsContainer}>
        <DietProfileButton
          onPress={onPressClientProfile}
          testID="dietProfileButton"
        />
        <ChatButton onPress={onPressClientChat} testID="chatButton" />
      </View>
    </View>
  );
};

export default ClientCardNutritionist;
