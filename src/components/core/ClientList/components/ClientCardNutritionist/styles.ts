import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  spacing: {
    marginBottom: 14,
  },
  cardContainer: {
    alignSelf: 'stretch',
    padding: 20,
    marginBottom: 24,
    borderRadius: 10,
    backgroundColor: colors.lightYellow,
  },
  buttonsContainer: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'space-between',
  },
});
