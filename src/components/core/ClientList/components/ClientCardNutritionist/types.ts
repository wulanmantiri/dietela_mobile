export interface Props {
  clientName: string;
  onPressClientProfile: () => void;
  onPressClientDietReport: () => void;
  onPressClientChat: () => void;
  testID?: string;
}
