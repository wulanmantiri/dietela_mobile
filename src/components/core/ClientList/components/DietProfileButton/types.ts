export interface Props {
  onPress: () => void;
  loading?: boolean;
  disabled?: true;
  testID?: string;
}
