import React, { FC } from 'react';
import { TouchableOpacity, Text } from 'react-native';

import { styles } from './styles';
import { Props } from './types';
import { typographyStyles } from 'styles';
import { Icon } from 'react-native-elements';

const DietProfileButton: FC<Props> = ({ onPress, testID }) => (
  <TouchableOpacity
    onPress={onPress}
    testID={testID}
    style={styles.containerStyle}>
    <Icon style={styles.img} name="user" type="feather" />
    <Text style={[typographyStyles.bodyMedium, styles.titleStyle]}>Profil</Text>
  </TouchableOpacity>
);

export default DietProfileButton;
