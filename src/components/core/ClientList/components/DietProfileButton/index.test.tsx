import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import DietProfileButton from '.';

describe('DietProfileButton', () => {
  it('renders correctly when active', () => {
    render(<DietProfileButton onPress={() => console.log('cool')} />);
  });

  it('renders correctly when disabled', () => {
    render(<DietProfileButton onPress={() => console.log('cool')} disabled />);
  });

  it('renders correctly when given a testID', () => {
    render(
      <DietProfileButton onPress={() => console.log('cool')} testID="button" />,
    );
  });

  it('executes onPress callback when button is pressed', async () => {
    let isPressed = false;

    const { getByText } = render(
      <DietProfileButton onPress={() => (isPressed = true)} testID="button" />,
    );

    const button = getByText(/profil/i);
    fireEvent.press(button);
    expect(isPressed).toBeTruthy();
  });
});
