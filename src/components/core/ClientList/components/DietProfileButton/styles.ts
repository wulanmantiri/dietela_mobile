import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  titleStyle: { color: colors.textBlack, textTransform: 'capitalize' },
  containerStyle: {
    marginTop: 12,
    marginRight: 5,
    padding: 12,
    overflow: 'hidden',
    borderRadius: 4,
    borderWidth: 1,
    backgroundColor: undefined,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flexGrow: 1,
  },
  img: {
    marginRight: 5,
  },
});
