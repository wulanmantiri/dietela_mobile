import React, { FC } from 'react';
import { TouchableOpacity, Text } from 'react-native';

import { styles } from './styles';
import { Props } from './types';
import { typographyStyles } from 'styles';
import { Icon } from 'react-native-elements';

const DietReportButton: FC<Props> = ({ onPress, testID }) => (
  <TouchableOpacity
    onPress={onPress}
    testID={testID}
    style={styles.containerStyle}>
    <Icon style={styles.img} name="ios-document-text" type="ionicon" />
    <Text style={[typographyStyles.bodyMedium, styles.titleStyle]}>
      Laporan Diet
    </Text>
  </TouchableOpacity>
);

export default DietReportButton;
