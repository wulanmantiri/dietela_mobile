import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import DietReportButton from '.';

describe('DietReportButton', () => {
  it('renders correctly when active', () => {
    render(<DietReportButton onPress={() => console.log('cool')} />);
  });

  it('renders correctly when disabled', () => {
    render(<DietReportButton onPress={() => console.log('cool')} disabled />);
  });

  it('renders correctly when given a testID', () => {
    render(
      <DietReportButton onPress={() => console.log('cool')} testID="button" />,
    );
  });

  it('executes onPress callback when button is pressed', async () => {
    let isPressed = false;

    const { getByText } = render(
      <DietReportButton onPress={() => (isPressed = true)} testID="button" />,
    );

    const button = getByText(/laporan diet/i);
    fireEvent.press(button);
    expect(isPressed).toBeTruthy();
  });
});
