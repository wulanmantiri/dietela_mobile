import React, { FC } from 'react';
import { TouchableOpacity, Text } from 'react-native';

import { styles } from './styles';
import { Props } from './types';
import { typographyStyles } from 'styles';
import { Icon } from 'react-native-elements';

const ChatButton: FC<Props> = ({ onPress, testID }) => (
  <TouchableOpacity
    onPress={onPress}
    testID={testID}
    style={styles.containerStyle}>
    <Icon style={styles.img} name="chatbox-ellipses-outline" type="ionicon" />
    <Text style={[typographyStyles.bodyMedium, styles.titleStyle]}>Chat</Text>
  </TouchableOpacity>
);

export default ChatButton;
