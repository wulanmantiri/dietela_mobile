import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import ChatButton from '.';

describe('ChatButton', () => {
  it('renders correctly when active', () => {
    render(<ChatButton onPress={() => console.log('cool')} />);
  });

  it('renders correctly when disabled', () => {
    render(<ChatButton onPress={() => console.log('cool')} disabled />);
  });

  it('renders correctly when given a testID', () => {
    render(<ChatButton onPress={() => console.log('cool')} testID="button" />);
  });

  it('executes onPress callback when button is pressed', async () => {
    let isPressed = false;

    const { getByText } = render(
      <ChatButton onPress={() => (isPressed = true)} testID="button" />,
    );

    const button = getByText(/chat/i);
    fireEvent.press(button);
    expect(isPressed).toBeTruthy();
  });
});
