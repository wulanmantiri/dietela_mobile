import React, { FC, useState } from 'react';
import { SearchBar } from 'react-native-elements';
import { ScrollView, View, StyleSheet, Dimensions } from 'react-native';
import { layoutStyles } from 'styles';
import { ClientCardNutritionist } from './components';
import { useNavigation } from '@react-navigation/native';
import { retrieveClientListApi } from 'services/profiles';
import { useApi, useDownloadFiles } from 'hooks';
import { getAbsoluteUrl } from 'utils/format';
import BigButton from '../../core/BigButton';
import Loader from '../../core/Loader';
import EmptyDataPage from '../../core/EmptyDataPage';
import { Section } from 'components/layout';
import { UserRole } from 'services/auth/models';
import { FileType } from 'hooks/useDownloadFiles/schema';
import { redirectToWA } from 'utils/chat';

interface Props {
  role: string;
  clientProfileRoute: string;
  clientDietReportRoute: string;
}

const ClientList: FC<Props> = ({
  role,
  clientProfileRoute,
  clientDietReportRoute,
}) => {
  const navigation = useNavigation();
  const { isLoading, data: clients = [] } = useApi(retrieveClientListApi);
  const { download } = useDownloadFiles(
    getAbsoluteUrl('/exportcsv'),
    'client list CSV',
    FileType.CSV,
    'export.csv',
  );
  const [searchedText, setSearchedText] = useState('');

  const updateSearch = (search: string) => {
    setSearchedText(search);
  };

  if (isLoading) {
    return <Loader />;
  }
  if (!clients.length) {
    return <EmptyDataPage text="Belum ada klien" />;
  }

  return (
    <View style={[layoutStyles, styles.listContainer]}>
      <SearchBar
        inputStyle={styles.backgroundWhite}
        inputContainerStyle={styles.backgroundWhite}
        rightIconContainerStyle={styles.backgroundWhite}
        leftIconContainerStyle={styles.backgroundWhite}
        containerStyle={styles.searchContainer}
        onChangeText={updateSearch}
        placeholder="Cari nama klien..."
        value={searchedText}
      />
      <ScrollView style={styles.container}>
        {clients
          .filter((c) =>
            c.user.name.toLowerCase().includes(searchedText.toLowerCase()),
          )
          .map((client, idx) => (
            <ClientCardNutritionist
              key={idx}
              clientName={client.user.name}
              onPressClientProfile={() => {
                navigation.navigate(clientProfileRoute, {
                  id: client.diet_questionnaire_id,
                  id_recommendation: client.diet_recommendation_id,
                  role: role,
                });
              }}
              onPressClientDietReport={() => {
                navigation.navigate(clientDietReportRoute, {
                  id: client.user.id,
                });
              }}
              onPressClientChat={() => redirectToWA(client.user.phone_number)}
            />
          ))}
      </ScrollView>
      {role === UserRole.ADMIN ? (
        <Section>
          <BigButton title="Download CSV" onPress={download} />
        </Section>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: { height: Dimensions.get('window').height * 0.83 },
  listContainer: {
    position: 'relative',
    flex: 1,
  },
  searchContainer: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    justifyContent: 'center',
    height: 58,
  },
  backgroundWhite: { backgroundColor: 'white' },
});

export default ClientList;
