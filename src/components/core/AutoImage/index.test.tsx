import React from 'react';
import { render } from '@testing-library/react-native';

import AutoImage from '.';
import { logo_white_small } from 'assets/images';

describe('AutoImage', () => {
  it('renders correctly', () => {
    render(<AutoImage source={logo_white_small} />);
  });
});
