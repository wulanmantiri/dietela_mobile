import React, { FC } from 'react';
import { View, Image, ImageSourcePropType } from 'react-native';
import FullWidthImage from 'react-native-fullwidth-image';

interface Props {
  source: ImageSourcePropType;
  ratio?: number;
  testID?: string;
}

const AutoImage: FC<Props> = ({ source, ratio, testID }) => {
  const { width, height } = Image.resolveAssetSource(source);

  return (
    <View testID={testID}>
      <FullWidthImage
        source={source}
        width={width}
        height={height}
        ratio={ratio}
      />
    </View>
  );
};

export default AutoImage;
