import React from 'react';
import { render } from '@testing-library/react-native';

import Loader, { DietelaCoverLoader } from '.';

describe('Loader component', () => {
  it('renders correctly', () => {
    render(<Loader />);
  });
});

describe('DietelaCoverLoader component', () => {
  it('renders correctly', () => {
    render(<DietelaCoverLoader />);
  });
});
