import { ReactNode } from 'react';

export interface Props {
  components: ReactNode[];
  currentStep: number;
  setCurrentStep: (_: number) => void;
  finishButtonLabel?: string;
  onFinish: () => void;
  isLoading?: boolean;
  isNextDisabled?: boolean;
}

export interface NextButtonProps {
  goNext: () => void;
  disabled?: boolean;
}
