import React from 'react';
import { fireEvent, render } from '@testing-library/react-native';

import WizardContainer from '.';
import { Text } from 'react-native';

describe('WizardContainer component', () => {
  const components = [<Text>hai</Text>, <Text>hello</Text>, <Text>hei</Text>];
  let currentStep = 1;
  const props = {
    components,
    currentStep,
    setCurrentStep: (v: typeof currentStep) => (currentStep = v),
    onFinish: jest.fn(),
  };

  it('displays first component as default if step props is not given', () => {
    const { getByText } = render(<WizardContainer {...props} />);
    const haiText = getByText(/hai/i);
    expect(haiText).toBeTruthy();
  });

  it('can go back and forth when "Kembali" or "Lanjut" button is pressed', () => {
    const { getByText, rerender } = render(<WizardContainer {...props} />);

    const nextButton = getByText(/Lanjut/i);
    fireEvent.press(nextButton);

    rerender(<WizardContainer {...props} currentStep={currentStep} />);
    const helloText = getByText(/hello/i);
    expect(helloText).toBeTruthy();
    const backButton = getByText(/Kembali/i);
    expect(backButton).toBeTruthy();
    fireEvent.press(backButton);

    rerender(<WizardContainer {...props} currentStep={currentStep} />);
    const haiText = getByText(/hai/i);
    expect(haiText).toBeTruthy();
  });

  it('shows finish button in the last component', () => {
    const { queryByText } = render(
      <WizardContainer {...props} currentStep={components.length} />,
    );
    const finishButton = queryByText(/Selesai/i);
    expect(finishButton).toBeTruthy();
  });
});
