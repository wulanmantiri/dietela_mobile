import { StyleSheet } from 'react-native';
import { colors, layoutStyles } from 'styles';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    flexGrow: 1,
  },
  bottomContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    ...layoutStyles,
  },
  flexEnd: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    ...layoutStyles,
  },
  backButton: {
    paddingLeft: 0,
  },
  nextButton: {
    width: 140,
    justifyContent: 'space-between',
    backgroundColor: colors.primaryYellow,
    borderRadius: 20,
  },
  finishButton: {
    borderRadius: 30,
    width: 140,
    backgroundColor: colors.secondaryVariant,
  },
});
