import React, { FC } from 'react';
import { ScrollView, View } from 'react-native';
import { Button } from 'react-native-elements';
import { colors } from 'styles';

import { styles } from './styles';
import { NextButtonProps, Props } from './types';

const NextButton: FC<NextButtonProps> = ({ goNext, disabled }) => (
  <Button
    icon={{
      name: 'caretright',
      type: 'antdesign',
      color: disabled ? 'gray' : colors.textBlack,
      size: 20,
    }}
    title="Lanjut"
    onPress={goNext}
    iconRight
    titleStyle={{
      color: colors.textBlack,
    }}
    buttonStyle={styles.nextButton}
    disabled={disabled}
  />
);

const WizardContainer: FC<Props> = ({
  components,
  currentStep,
  setCurrentStep,
  finishButtonLabel,
  onFinish,
  isLoading,
  isNextDisabled,
}) => {
  const goBack = () => {
    setCurrentStep(currentStep - 1);
  };

  const goNext = () => {
    setCurrentStep(currentStep + 1);
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {components[currentStep - 1]}
      {currentStep > 1 ? (
        <View style={styles.bottomContainer}>
          <Button
            icon={{
              name: 'caretleft',
              type: 'antdesign',
              color: 'gray',
              size: 20,
            }}
            title="Kembali"
            type="clear"
            onPress={goBack}
            titleStyle={{
              color: colors.textBlack,
            }}
            buttonStyle={styles.backButton}
          />
          {currentStep === components.length ? (
            <Button
              title={finishButtonLabel || 'Selesai'}
              onPress={onFinish}
              buttonStyle={styles.finishButton}
              loading={isLoading}
              titleStyle={{
                color: colors.textBlack,
              }}
              disabled={isNextDisabled}
            />
          ) : (
            <NextButton goNext={goNext} disabled={isNextDisabled} />
          )}
        </View>
      ) : (
        <View style={styles.flexEnd}>
          <NextButton goNext={goNext} disabled={isNextDisabled} />
        </View>
      )}
    </ScrollView>
  );
};

export default WizardContainer;
