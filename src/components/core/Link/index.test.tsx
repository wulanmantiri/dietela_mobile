import React from 'react';
import { render } from '@testing-library/react-native';

import Link from '.';
import { colors } from 'styles';

describe('Link', () => {
  it('renders correctly', () => {
    render(<Link title="hai" />);
  });

  it('renders correctly given color', () => {
    render(<Link title="hai" color={colors.primary} />);
  });
});
