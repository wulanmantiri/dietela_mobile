import React from 'react';
import { render } from '@testing-library/react-native';

import EmptyDataPage from '.';

describe('EmptyDataPage', () => {
  it('renders correctly', () => {
    render(<EmptyDataPage text="No data" />);
  });
});
