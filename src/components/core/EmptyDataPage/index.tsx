import React, { FC } from 'react';

import { styles } from './styles';
import { View } from 'react-native';
import { Text, Icon } from 'react-native-elements';

import { colors } from 'styles';

interface Props {
  text: string;
}

const EmptyDataPage: FC<Props> = ({ text }) => (
  <View style={styles.center}>
    <Icon name="smile" type="feather" size={75} color={colors.primaryVariant} />
    <Text style={styles.noRecomText}>{text}</Text>
  </View>
);

export default EmptyDataPage;
