import { StyleSheet } from 'react-native';
import { typography } from 'styles';

export const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  noRecomText: {
    ...typography.headingMedium,
    textAlign: 'center',
    marginTop: 10,
  },
});
