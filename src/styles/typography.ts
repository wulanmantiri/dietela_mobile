import { StyleProp, TextStyle } from 'react-native';

export const fontConfig = {
  normal: 'Roboto-Regular',
  italic: 'Roboto-Italic',
  bold: 'Roboto-Medium',
  bold_montserrat: 'Montserrat-Bold',
};

export const typography = {
  displayLarge: {
    fontFamily: fontConfig.bold,
    fontSize: 40,
  },
  displayMedium: {
    fontFamily: fontConfig.bold,
    fontSize: 32,
  },
  displayMediumMontserrat: {
    fontFamily: fontConfig.bold_montserrat,
    fontSize: 30,
  },
  headingLarge: {
    fontFamily: fontConfig.bold,
    fontSize: 24,
  },
  headingMedium: {
    fontFamily: fontConfig.bold,
    fontSize: 20,
  },
  bodyLarge: {
    fontFamily: fontConfig.normal,
    fontSize: 18,
  },
  bodyMedium: {
    fontFamily: fontConfig.normal,
    fontSize: 16,
  },
  bodySmall: {
    fontFamily: fontConfig.normal,
    fontSize: 14,
  },
  caption: {
    fontFamily: fontConfig.normal,
    fontSize: 12,
  },
  overlineBig: {
    fontFamily: fontConfig.bold,
    fontSize: 14,
    textTransform: 'uppercase',
    letterSpacing: 1.25,
  },
  overlineSmall: {
    fontFamily: fontConfig.bold,
    fontSize: 10,
    textTransform: 'uppercase',
    letterSpacing: 1.5,
  },
};

export const typographyStyles: {
  [_ in keyof typeof typography]: StyleProp<TextStyle>;
} = {
  displayLarge: {
    fontFamily: fontConfig.bold,
    fontSize: 40,
  },
  displayMedium: {
    fontFamily: fontConfig.bold,
    fontSize: 32,
  },
  displayMediumMontserrat: {
    fontFamily: fontConfig.bold_montserrat,
    fontSize: 32,
  },
  headingLarge: {
    fontFamily: fontConfig.bold,
    fontSize: 24,
  },
  headingMedium: {
    fontFamily: fontConfig.bold,
    fontSize: 20,
  },
  bodyLarge: {
    fontFamily: fontConfig.normal,
    fontSize: 18,
  },
  bodyMedium: {
    fontFamily: fontConfig.normal,
    fontSize: 16,
  },
  bodySmall: {
    fontFamily: fontConfig.normal,
    fontSize: 14,
  },
  caption: {
    fontFamily: fontConfig.normal,
    fontSize: 12,
  },
  overlineBig: {
    fontFamily: fontConfig.bold,
    fontSize: 14,
    textTransform: 'uppercase',
    letterSpacing: 1.25,
  },
  overlineSmall: {
    fontFamily: fontConfig.bold,
    fontSize: 10,
    textTransform: 'uppercase',
    letterSpacing: 1.5,
  },
};
