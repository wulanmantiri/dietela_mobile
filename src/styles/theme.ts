import { Theme } from 'react-native-elements/dist/config/theme';
import { TouchableOpacity } from 'react-native';

import { colors } from './colors';
import { fontConfig, typography } from './typography';

export const theme: Theme = {
  Text: {
    style: {
      fontFamily: fontConfig.normal,
    },
  },
  Button: {
    buttonStyle: {
      padding: 12,
      paddingHorizontal: 20,
    },
    TouchableComponent: TouchableOpacity,
  },
  CheckBox: {
    containerStyle: {
      marginLeft: 0,
      marginRight: 0,
      borderColor: 'transparent',
      backgroundColor: colors.neutralLight,
      padding: 14,
    },
    textStyle: {
      fontWeight: '400',
      ...typography.bodyMedium,
    },
    size: 18,
    checkedColor: colors.primary,
    uncheckedColor: colors.neutral,
  },
  Input: {
    containerStyle: {
      paddingHorizontal: 0,
    },
    inputStyle: {
      ...typography.bodyMedium,
    },
  },
};
