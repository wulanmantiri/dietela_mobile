import { User } from 'services/auth/models';

export interface Client {
  user: User;
  diet_profile_id: number;
  diet_questionnaire_id: number;
  diet_recommendation_id: number;
}
