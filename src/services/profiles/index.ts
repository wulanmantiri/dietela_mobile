import { api, RequestMethod, ApiResponse } from '../api';

import * as apiUrls from './urls';
import { Client } from './models';

export const retrieveClientListApi = (): ApiResponse<Client[]> => {
  return api(RequestMethod.GET, apiUrls.profiles);
};
