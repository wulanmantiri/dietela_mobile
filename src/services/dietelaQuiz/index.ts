import { api, RequestMethod, ApiResponse } from '../api';

import * as apiUrls from './urls';
import { DietProfileRequest, DietProfileResponse } from './models';

export const createDietProfileApi = (
  body: DietProfileRequest,
): ApiResponse<DietProfileResponse> => {
  return api(RequestMethod.POST, apiUrls.dietProfile, body);
};
