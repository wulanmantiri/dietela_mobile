export enum Status {
  HEALTHY = 'healthy',
  WARNING = 'warning',
  DANGER = 'danger',
  CATASTROPHE = 'catastrophe',
}

export enum DietelaProgram {
  TRIAL = 'TRIAL',
  BABY_1 = 'BABY_1',
  BABY_3 = 'BABY_3',
  GOALS_1 = 'GOALS_1',
  GOALS_3 = 'GOALS_3',
  GOALS_6 = 'GOALS_6',
  BALANCED_1 = 'BALANCED_1',
  BALANCED_3 = 'BALANCED_3',
  BALANCED_6 = 'BALANCED_6',
}

export enum BreakfastReponse {
  NO_BREAKFAST = 'Perut kosong di pagi hari bisa mengarahkan kepada 2 keadaan, antara membuat makan berlebihan di siang hari atau kurang asupan zat gizi per hari. Makan pagi bisa disesuaikan dengan pola bangun tidur kamu lho, cari deh solusi terbaik agar kamu bisa beraktifitas tanpa perut kosong.',
  MED_BREAKFAST = 'Sudah bagus nih, bisa mengisi perut di pagi hari, eittts, tapi artinya kamu perlu melengkapi asupan energi di siang sampai malam hari karena makan paginya cuma sedikit, kamu perlu pengaturan lebih spesifik lagi agak asupan energi kamu seimbang.',
  HI_BREAKFAST = 'Juara! Permulaan yang bagus untuk memenuhi kebutuhan energi seharian kamu. Jangan lupa jenis makanan yang kamu pilih saat sarapan harus seimbang juga komposisinya. Sudah ada buah dan sayur belum di sarapanmu? Masih banyak gorengan gak di sarapanmu?',
}

export enum PhysicalActivityResponse {
  LEVEL1_ACTIVITY = 'Hayooo kok duduk-duduk aja? Yuk ah mulai lakukan aktivitas fisik, dimulai dari aktivitas yang ringan seperti jalan kaki, jogging, senam, atau naik turun tangga 15 menit per hari. Tingkatkan perlahan hingga mencapai 30 menit per hari. Konsultasikan kebutuhan gizi harianmu kepada Dietela agar asupan makan kamu seimbang dengan energi yang kamu keluarkan setiap harinya',
  LEVEL2_ACTIVITY = 'Lumayan sudah banyak jalan, tapi boleh dong diselingi olahraga ringan seperti jogging, senam, atau naik turun tangga 15 menit per hari. Tingkatkan perlahan hingga mencapai 30 menit per hari. Konsultasikan kebutuhan gizi harianmu kepada dietela agar asupan makan kamu seimbang dengan energi yang kamu keluarkan setiap harinya',
  LEVEL3_ACTIVITY = 'Keren! Pertahankan olahraga rutin selama 150 menit per minggu atau minimal 30 menit per hari, tambahkan aktivitas penguatan otot sebanyak 2 kali per minggu selama 10-15 menit per hari. Konsultasikan kebutuhan gizi harianmu kepada dietela untuk pengaturan pola makan yang sesuai dengan aktivitas fisikmu.',
  LEVEL4_ACTIVITY = 'Keren! Pertahankan durasi aktivitas fisik, tingkatkan intensitas gerakan secara bertahap, jangan lupa tambahkan aktivitas penguatan otot sebanyak 2 kali per minggu selama 10-15 menit per hari. Konsultasikan kebutuhan gizi harianmu kepada dietela untuk pengaturan pola makan yang sesuai dengan aktivitas fisikmu.',
  LEVEL5_ACTIVITY = 'Wow! Sudah bagus banget nih, jangan lupa lakukan monitoring rutin terhadap komposisi tubuhmu. Jangan lupa juga untuk mencukupi kebutuhan cairan sebelum, saat, dan setelah olahraga. Konsultasikan kebutuhan gizi harianmu kepada dietela untuk pengaturan pola makan yang sesuai dengan aktivitas fisikmu.',
}

export enum BodyMassConstants {
  UNDERWEIGHT = 'Underweight',
  NORMAL = 'Normal',
  PREOBESITY = 'Preobesitas',
  OBESITY1 = 'Obesitas 1',
  OBESITY2 = 'Obesitas 2',
}

export enum VegetableAndFruitSufficiencyResponse {
  LACKING = 'Kurang makan sayur dan buah',
  ENOUGH = 'Asupan sayur dan buah sudah cukup',
}

export enum SugarSaltFatProblemResponse {
  CONTROLLED = 'Asupan gula, garam, dan lemak terkontrol',
  EXCESSIVE = 'Kelebihan makan gula, garam, dan lemak',
}

export enum LargeMealDietRecommendation {
  ONCE_A_DAY = 'Hmmm.... Kenapa cuma makan besar 1 kali sehari? Makan besar cuma 1 kali dalam sehari berisiko membuat kamu makan berlebihan dan kekurangan zat gizi tertentu Lho! Yuk coba mulai diatur pola makannya, dengan merutinkan waktu makan agar metabolisme kamu lebih baik.',
  TWICE_A_DAY = 'Makan besar kamu sudah 2 kali, tapi masih kurang nih. Sebaiknya makan besar itu 3 kali yaah, karena ketika skip 1 jam makan, bisa menyebabkan berbagai risiko, seperti makan berlebihan di jam makan berikutnya, kekurangan zat gizi dan sampai ke masalah kesehatan.',
  THRICE_A_DAY = 'Keren! Apresiasi buat kamu yang sudah rutin makan besar 3 kali sehari. Tapi pastikan agar sesuai dengan kebutuhan kamu yah, jangan berlebih atau kekurangan.',
  MORE_THAN_THRICE_A_DAY = 'Eiiits, porsi makan besar kamu sulit untuk bisa seimbang kalau kamu makan besar lebih dari 3 kali dalam sehari. Cek porsi yang tepat untukmu yah kalau kamu memang sering makan besar.',
}

export enum SnacksDietRecommendation {
  NO_SNACK = 'Yah kok gak ngemil? Makan cemilan itu penting untuk menjaga kadar gula darah kamu seharian, dengan ngemil gula darah seharian kamu bisa terkontrol sehingga mencegah lemas dan berkurangnya konsentrasi.',
  ONCE_A_DAY = 'Tambah waktu nyemil ga dosa loh asal porsinya tetap sesuai dengan kebutuhan kamu. Sebaiknya sehari bisa 2 kali makan cemilan agar kamu tidak kekurangan energi di sela-sela aktivitas dan tidak berlebihan makan pada 1 jam makan tertentu. Atur waktu makan kamu dengan lebih baik yah.',
  TWICE_A_DAY = 'Baguuuus! Pertahankan ya makan cemilan 2x sehari. Perhatikan porsi dan kandungan zat gizi cemilanmu ya. Utamakan yang padat gizi sehingga kualitasnya baik untuk tubuh kamu.',
  THRICE_A_DAY = 'Boleh sih makan cemilan 3 kali sehari, artinya porsi dan jenis cemilan yang kamu konsumsi harus diatur sedemikian rupa agar tidak melebihi kebutuhan energi dan zat gizi dalam sehari.',
  MORE_THAN_THRICE_A_DAY = 'Hmmmm.... kalau makan cemilan lebih dari 3 kali dalam sehari, bisa-bisa asupan kalori kamu berlebih nih dalam sehari atau malah kurang karena seharian cuma makan cemilan. Yuk atur jam makan kamu agar lebih baik. Porsi dan jenis makanan yang kamu konsumsi juga menjadi kunci agar asupan energi dan zat gizi kamu sehat dan seimbang.',
}

export const bodyMassStatus = {
  [BodyMassConstants.UNDERWEIGHT]: {
    status: Status.WARNING,
  },
  [BodyMassConstants.NORMAL]: {
    status: Status.HEALTHY,
  },
  [BodyMassConstants.PREOBESITY]: {
    status: Status.WARNING,
  },
  [BodyMassConstants.OBESITY1]: {
    status: Status.DANGER,
  },
  [BodyMassConstants.OBESITY2]: {
    status: Status.CATASTROPHE,
  },
};

export const sayurBuahStatus = {
  [VegetableAndFruitSufficiencyResponse.ENOUGH]: {
    status: Status.HEALTHY,
  },
  [VegetableAndFruitSufficiencyResponse.LACKING]: {
    status: Status.WARNING,
  },
};

export const gglStatus = {
  [SugarSaltFatProblemResponse.CONTROLLED]: {
    status: Status.HEALTHY,
  },
  [SugarSaltFatProblemResponse.EXCESSIVE]: {
    status: Status.WARNING,
  },
};

export const makanBesarStatus = {
  [LargeMealDietRecommendation.ONCE_A_DAY]: {
    status: Status.DANGER,
    headline: 'Risiko tinggi kekurangan zat gizi yang penting',
  },
  [LargeMealDietRecommendation.TWICE_A_DAY]: {
    status: Status.WARNING,
    headline: 'Risiko sedang kekurangan zat gizi yang penting',
  },
  [LargeMealDietRecommendation.THRICE_A_DAY]: {
    status: Status.HEALTHY,
    headline: 'Zat gizi penting akan terpenuhi dengan baik',
  },
  [LargeMealDietRecommendation.MORE_THAN_THRICE_A_DAY]: {
    status: Status.DANGER,
    headline: 'Risiko tinggi kelebihan konsumsi makan',
  },
};

export const sarapanStatus = {
  [BreakfastReponse.NO_BREAKFAST]: {
    status: Status.DANGER,
    headline: 'Porsi sarapan kurang',
  },
  [BreakfastReponse.MED_BREAKFAST]: {
    status: Status.HEALTHY,
    headline: 'Porsi sarapan cukup',
  },
  [BreakfastReponse.HI_BREAKFAST]: {
    status: Status.HEALTHY,
    headline: 'Porsi sarapan cukup',
  },
};

export const cemilanStatus = {
  [SnacksDietRecommendation.NO_SNACK]: {
    status: Status.DANGER,
    headline: 'Risiko tinggi metabolisme energi berantakan',
  },
  [SnacksDietRecommendation.ONCE_A_DAY]: {
    status: Status.WARNING,
    headline: 'Risiko sedang metabolisme energi berantakan',
  },
  [SnacksDietRecommendation.TWICE_A_DAY]: {
    status: Status.HEALTHY,
    headline: 'Metabolisme energi akan terjaga',
  },
  [SnacksDietRecommendation.THRICE_A_DAY]: {
    status: Status.WARNING,
    headline: 'Risiko sedang metabolisme energi berantakan',
  },
  [SnacksDietRecommendation.MORE_THAN_THRICE_A_DAY]: {
    status: Status.DANGER,
    headline: 'Risiko tinggi metabolisme energi berantakan',
  },
};

export const aktivitasFisikStatus = {
  [PhysicalActivityResponse.LEVEL1_ACTIVITY]: {
    status: Status.DANGER,
    headline: 'Sangat kurang aktif (sedenter)',
  },
  [PhysicalActivityResponse.LEVEL2_ACTIVITY]: {
    status: Status.WARNING,
    headline: 'Kurang aktif',
  },
  [PhysicalActivityResponse.LEVEL3_ACTIVITY]: {
    status: Status.HEALTHY,
    headline: 'Cukup aktif',
  },
  [PhysicalActivityResponse.LEVEL4_ACTIVITY]: {
    status: Status.HEALTHY,
    headline: 'Aktif',
  },
  [PhysicalActivityResponse.LEVEL5_ACTIVITY]: {
    status: Status.HEALTHY,
    headline: 'Sangat aktif',
  },
};

export interface ProgramRecommendations {
  priority_1: DietelaProgram;
  priority_2: DietelaProgram | null;
}
