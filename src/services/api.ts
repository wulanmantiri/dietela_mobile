import axios, { AxiosRequestConfig } from 'axios';
import { API_BASE_URL } from 'env';

export enum RequestMethod {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
}

export interface Response<T> {
  success: boolean;
  data?: T;
  error?: any;
}

export type ApiResponse<T> = Promise<Response<T>>;

const apiInstance = axios.create({
  baseURL: API_BASE_URL + '/',
});

export async function api<T>(
  method: RequestMethod = RequestMethod.GET,
  url: string,
  body: object = {},
  headers: object = {},
  noStringify: boolean = false,
): ApiResponse<T> {
  const requestData: AxiosRequestConfig = {
    url,
    method,
    data: noStringify ? body : JSON.stringify(body),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json;charset=UTF-8',
      ...headers,
    },
  };

  return await apiInstance
    .request(requestData)
    .then((res) => ({
      success: true,
      data: res.data,
    }))
    .catch((err) => ({
      success: false,
      error: err.response.data,
    }));
}

export const setAuthHeader = (token: string): void => {
  apiInstance.defaults.headers.Authorization = `Bearer ${token}`;
};

export const resetAuthHeader = (): void => {
  apiInstance.defaults.headers.Authorization = '';
};

export const set401Callback = (callback: () => void): void => {
  apiInstance.interceptors.response.use(undefined, (err) => {
    if (err.response.status === 401) {
      callback();
    }
    return Promise.reject(err);
  });
};
