import { User } from 'services/auth/models';

export interface DietQuestionnaireRequest {
  agree_to_all_statements_consent?: number | null;
  personal_data_consent?: number | null;
  general_purpose?: number | null;
  date_of_birth?: Date | string | null;
  city_and_area_of_residence?: string | null;
  handphone_no?: string | null;
  whatsapp_no?: string | null;
  profession?: number | null;
  last_education?: number | null;
  meal_preference?: number | null;
  waist_size?: number | null;
  dietary_change?: number | null;
  has_weigher?: number | null;
  preferred_food_taste?: string | null;
  breakfast_frequency?: number | null;
  breakfast_meal_type?: number | null;
  sweet_tea_consumption_frequency?: number | null;
  coffee_consumption_frequency?: number | null;
  milk_consumption_frequency?: number | null;
  other_drink_consumption_frequency?: number | null;
  additional_sugar_in_a_day?: number | null;
  liquid_consumption_frequency?: number | null;
  meal_consumed_almost_every_day?: string | null;
  unliked_food?: string | null;
  expected_food_on_breakfast?: string | null;
  expected_food_on_lunch_dinner?: string | null;
  breakfast_meal_explanation?: string | null;
  morning_snack_explanation?: string | null;
  lunch_meal_explanation?: string | null;
  evening_snack_explanation?: string | null;
  dinner_meal_explanation?: string | null;
  night_snack_explanation?: string | null;
  food_alergies?: string | null;
  diet_drinks?: string | null;
  meal_provider?: number[];
  cigarette_alcohol_condition?: number[];
  multivitamin_tablet_suplement?: string | null;
  physical_activity?: number[] | null;
  other_physical_activity?: string | null;
  diet_and_life_style_story?: string | null;
  disease?: number[];
  complaint?: number[];
  regular_drug_consumption?: string | null;
  other_disease?: string | null;
  motivation_using_dietela?: string | null;
  dietela_nutritionist_expectation?: string | null;
  dietela_program_expectation?: string | null;
  step?: number;
}

export interface DietQuestionnaireResponse extends DietQuestionnaireRequest {
  id: number;
  user: User;
  finished_steps: number[];
}
