import { api, RequestMethod, ApiResponse } from '../api';

import * as apiUrls from './urls';
import { DietQuestionnaireRequest, DietQuestionnaireResponse } from './models';

export const updateDietQuestionnaireApi = (
  id: number,
  body: DietQuestionnaireRequest,
): ApiResponse<DietQuestionnaireResponse> => {
  return api(RequestMethod.PATCH, apiUrls.dietQuestionnaireById(id), body);
};

export const retrieveDietQuestionnaireApi = (): ApiResponse<
  DietQuestionnaireResponse[]
> => {
  return api(RequestMethod.GET, apiUrls.dietQuestionnaire);
};

export const retrieveDietQuestionnaireByIdApi = (id: number) => {
  return api(RequestMethod.GET, apiUrls.dietQuestionnaireById(id));
};
