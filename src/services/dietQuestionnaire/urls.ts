export const dietQuestionnaire = 'diet-questionnaire/';
export const dietQuestionnaireById = (id: number) =>
  `${dietQuestionnaire}${id}/`;
