export const dietRecommendation = 'diet-recommendation/';
export const dietRecommendationById = (id: number) =>
  `${dietRecommendation}${id}/`;
