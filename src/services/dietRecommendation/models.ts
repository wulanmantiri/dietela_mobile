export interface DietRecommendationResponse {
  id: number;
  client_plan_meal: string | null;
  nutritional_advice: string;
  lifestyle_advice: string;
  nutritionist: number;
  client: number;
}

export interface DietRecommendationRequest {
  client_plan_meal: string;
  nutritional_advice: string;
  lifestyle_advice: string;
}
