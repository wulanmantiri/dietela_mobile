import { api, RequestMethod, ApiResponse } from '../api';

import * as apiUrls from './urls';
import { DietRecommendationResponse } from './models';

export const retrieveDietRecommendationApi = (): ApiResponse<
  DietRecommendationResponse[]
> => {
  return api(RequestMethod.GET, apiUrls.dietRecommendation);
};

export const retrieveDietRecommendationByIdApi = (
  id: number,
): ApiResponse<DietRecommendationResponse> => {
  return api(RequestMethod.GET, apiUrls.dietRecommendationById(id));
};

export const submitDietRecommendationByIdApi = (id: number, body) => {
  return api(
    RequestMethod.PATCH,
    apiUrls.dietRecommendationById(id),
    body,
    {
      'Content-Type': 'multipart/form-data',
    },
    true,
  );
};
