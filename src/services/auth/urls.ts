const auth = 'auth/';

export const google = `${auth}google/`;
export const signup = `${auth}registration/`;
export const login = `${auth}user-login/`;

export const user = `${auth}user/`;
export const linkData = `${auth}link-data/`;
