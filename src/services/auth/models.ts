import { DietProfileResponse } from 'services/dietelaQuiz/models';
import { CartResponse, TransactionStatus } from 'services/payment/models';

export interface GoogleLoginRequest {
  access_token: string;
}

export interface RegistrationRequest {
  name: string;
  email: string;
  password1: string;
  password2: string;
}

export enum UserRole {
  CLIENT = 'client',
  NUTRITIONIST = 'nutritionist',
  ADMIN = 'admin',
}

export interface User {
  id: number | null;
  email: string;
  name: string;
  role: UserRole | null;
  phone_number: string | null;
  deadline: string | null;
}

export interface AuthUserResponse {
  id: number | null;
  email: string;
  name: string;
  role: UserRole | null;
  transaction_status: TransactionStatus | null;
  is_finished_onboarding: boolean;
  cart_id: number | null;
  all_carts_are_expired: boolean;
  nutritionist: {
    full_name_and_degree: string;
    phone_number: string | null;
  } | null;
}

export interface LoginRequest {
  email: string;
  password: string;
  role: UserRole;
}

export interface LoginResponse {
  access_token: string;
  refresh_token: string;
  user: AuthUserResponse;
}

export interface LinkUserDataRequest {
  email: string;
  diet_profile_id: number;
  cart_id: number;
}

export interface LinkUserDataResponse {
  user: User;
  diet_profile: DietProfileResponse;
  cart: CartResponse;
}
