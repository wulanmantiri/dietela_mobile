import { api, RequestMethod, ApiResponse } from '../api';

import * as apiUrls from './urls';
import {
  GoogleLoginRequest,
  LoginRequest,
  LoginResponse,
  RegistrationRequest,
  LinkUserDataRequest,
  LinkUserDataResponse,
  AuthUserResponse,
} from './models';

export const googleLoginApi = (
  body: GoogleLoginRequest,
): ApiResponse<LoginResponse> => {
  return api(RequestMethod.POST, apiUrls.google, body);
};

export const signupApi = (
  body: RegistrationRequest,
): ApiResponse<LoginResponse> => {
  return api(RequestMethod.POST, apiUrls.signup, body);
};

export const loginApi = (body: LoginRequest): ApiResponse<LoginResponse> => {
  return api(RequestMethod.POST, apiUrls.login, body);
};

export const retrieveUserApi = (): ApiResponse<AuthUserResponse> => {
  return api(RequestMethod.GET, apiUrls.user);
};

export const linkUserDataApi = (
  body: LinkUserDataRequest,
): ApiResponse<LinkUserDataResponse> => {
  return api(RequestMethod.POST, apiUrls.linkData, body);
};
