import { api, RequestMethod, ApiResponse } from '../api';

import * as apiUrls from './urls';
import {
  UserReportResponse,
  UserReportRequest,
  NutritionistCommentRequest,
  NutritionistCommentResponse,
  UserReportsResponse,
} from './models';

export const retrieveUserReportsByIdApi = (
  clientId: number | null,
): ApiResponse<UserReportsResponse> => {
  return api(RequestMethod.GET, apiUrls.userReportByClientId(clientId));
};

export const createUserReportApi = (
  payload: UserReportRequest,
): ApiResponse<UserReportResponse> => {
  return api(RequestMethod.POST, apiUrls.userReport, payload);
};

export const retrieveUserReportCommentByReportId = (reportId: number) => {
  return api(RequestMethod.GET, apiUrls.userReportCommentByReportId(reportId));
};

export const createNutritionistCommentApi = (
  payload: NutritionistCommentRequest,
): ApiResponse<NutritionistCommentResponse> => {
  return api(RequestMethod.POST, apiUrls.nutritionistComment, payload);
};
