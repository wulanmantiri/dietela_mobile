export const progress = 'progress/';

export const userReport = `${progress}user_report/`;
export const userReportByClientId = (id: number | null) =>
  `${userReport}?client_id=${id}`;
export const userReportCommentByReportId = (id: number) =>
  `${progress}nutritionist_comment/?weekly_report_id=${id}`;

export const nutritionistComment = `${progress}nutritionist_comment/`;
