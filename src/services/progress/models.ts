import { User } from 'services/auth/models';

export interface UserReportRequest {
  weight: number;
  height: number;
  waist_size: number;
  changes_felt: number;
  hunger_level: number;
  fullness_level: number;
  heavy_meal: number;
  snacks: number;
  sweet_beverages: number;
  sugary_ingredients: number;
  fried_snacks: number;
  umami_snacks: number;
  sweet_snacks: number;
  fruits_portion: number;
  vegetables_portion: number;
  water_consumption: number;
  physical_activity: number[];
  physical_activity_other: string;
  time_for_activity: number;
  feeling_rating: number;
  lesson_learned: string;
  problem_faced_and_feedbacks: string;
}

export interface UserReportResponse extends UserReportRequest {
  client: User;
  nutritionist: number;
  id: number;
  week_num: number;
}

export interface UserReportsResponse {
  has_submitted_this_week: boolean;
  today_week: number;
  data: UserReportResponse[];
}

export interface NutritionistComment {
  weight: string;
  height: string;
  waist_size: string;
  changes_felt: string;
  hunger_level: string;
  fullness_level: string;
  heavy_meal: string;
  snacks: string;
  average_consumption: string;
  water_consumption: string;
  physical_activity: string;
  time_for_activity: string;
  feeling_rating: string;
  lesson_learned: string;
  problem_faced_and_feedbacks: string;
}

export interface NutritionistCommentRequest extends NutritionistComment {
  weekly_report: number;
}

export interface NutritionistCommentResponse
  extends NutritionistCommentRequest {
  id: number;
}
