export interface Nutritionist {
  id: number;
  full_name_and_degree: string;
  registration_certificate_no: string;
  university: string;
  mastered_nutritional_problems: string;
  handled_age_group: string;
  another_practice_place: string;
  languages: string;
  profile_picture?: string;
  phone_number: string;
}
