import { DietelaProgram } from 'services/dietelaQuiz/quizResult';
import { Nutritionist } from 'services/nutritionists/models';

export interface CartRequest {
  program: DietelaProgram | null;
  nutritionist: number | null;
}

export enum TransactionStatus {
  UNPAID = 'unpaid',
  SUCCESS = 'success',
  PENDING = 'pending',
  ERROR = 'error',
}

export interface CartResponse {
  id: number;
  transaction_status: TransactionStatus;
  program: {
    id: number;
    name: string;
    price: number;
    unique_code: DietelaProgram;
  };
  nutritionist: Nutritionist;
  user: number;
}

export interface UpdateCartRequest {
  transaction_status: TransactionStatus;
}

export interface MidtransResponse {
  token: string;
  redirect_url: string;
}
