const payment = 'payment/';

export const cart = `${payment}cart/`;
export const cartById = (id: string | number) => `${payment}cart/${id}/`;

export const midtrans = `${payment}midtrans/`;
