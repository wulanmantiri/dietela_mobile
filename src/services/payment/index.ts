import { api, RequestMethod, ApiResponse } from '../api';

import * as apiUrls from './urls';
import {
  CartRequest,
  CartResponse,
  MidtransResponse,
  UpdateCartRequest,
} from './models';

export const createCartApi = (body: CartRequest): ApiResponse<CartResponse> => {
  return api(RequestMethod.POST, apiUrls.cart, body);
};

export const retrieveCartApi = (
  id: string | number | null,
): ApiResponse<CartResponse> => {
  if (id) {
    return api(RequestMethod.GET, apiUrls.cartById(id));
  }
  return Promise.reject('Anda belum memilih program diet.');
};

export const updateCartApi = (
  id: string | number | null,
  body: UpdateCartRequest,
): ApiResponse<CartResponse> => {
  if (id) {
    return api(RequestMethod.PATCH, apiUrls.cartById(id), body);
  }
  return Promise.reject('Anda belum memilih program diet.');
};

export const payWithMidtransApi = (
  cartId?: number | null,
): ApiResponse<MidtransResponse> => {
  if (cartId) {
    return api(RequestMethod.POST, apiUrls.midtrans, { cart_id: cartId });
  }
  return Promise.reject('Anda belum memilih program diet.');
};
