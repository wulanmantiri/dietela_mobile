export enum FileType {
  PDF = 'application/pdf',
  CSV = 'text/csv',
}
