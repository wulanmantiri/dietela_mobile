import { PermissionsAndroid } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import { FileType } from './schema';
import Toast from 'react-native-toast-message';

const useDownloadFiles = (
  url = '',
  title = '',
  fileType: FileType,
  fileTitle?: string,
) => {
  const fileName = fileTitle ? fileTitle : url?.split('/').pop();
  const extension = fileName?.split('.').pop()?.toUpperCase() || '-';

  const askWritePermission = async () =>
    await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    );

  const download = async (): Promise<any> => {
    const granted = await askWritePermission();
    if (!granted) {
      return;
    }

    Toast.show({
      type: 'success',
      text1: `Mengunduh ${fileName}...`,
      text2: 'Notifikasi akan muncul setelah unduh selesai. Mohon menunggu.',
    });

    const dirs = RNFetchBlob.fs.dirs;
    RNFetchBlob.config({
      addAndroidDownloads: {
        useDownloadManager: true,
        notification: true,
        mime: fileType,
        title: title || fileName,
        mediaScannable: true,
        path: dirs.DownloadDir + `/${fileName}`,
      },
    })
      .fetch('GET', url)
      .then((res) => res.path())
      .catch((err) => console.log(err));
  };

  return {
    fileName,
    extension,
    download,
  };
};

export default useDownloadFiles;
