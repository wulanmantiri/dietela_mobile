import { useState, useEffect, useCallback } from 'react';
import { ApiResponse, Response } from 'services/api';

const useApi = <T>(
  fetchApi: () => ApiResponse<T>,
): {
  isLoading: boolean;
} & Response<T> => {
  const [response, setResponse] = useState({
    isLoading: true,
    success: false,
  });

  const fetchApiWrapper = useCallback(fetchApi, []);

  useEffect(() => {
    const fetchData = async () => {
      const apiResponse = await fetchApiWrapper();
      setResponse({
        isLoading: false,
        ...apiResponse,
      });
    };

    fetchData();
  }, [fetchApiWrapper]);

  return response;
};

export default useApi;
