import { FormikConfig, useFormik } from 'formik';
import { useState } from 'react';

const useForm = <T>(
  formConfig: FormikConfig<T>,
  immediateValidate: boolean = true,
) => {
  const [shouldValidate, setShouldValidate] = useState(immediateValidate);

  const {
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    touched,
    values,
    errors,
    validateForm,
    ...methods
  } = useFormik({
    validateOnChange: shouldValidate,
    validateOnBlur: shouldValidate,
    ...formConfig,
  });

  const getTextInputProps = (fieldName: string) => {
    const name = fieldName as keyof T;
    return {
      onChangeText: handleChange(name),
      onBlur: handleBlur(name),
      value: `${values[name]}`,
      errorMessage: touched[name] ? errors[name] : null,
    };
  };

  const getFormFieldProps = (fieldName: string) => {
    const name = fieldName as keyof T;
    return {
      onChange: (value: any) => {
        setFieldTouched(fieldName, true);
        setFieldValue(fieldName, value);
      },
      value: values[name],
      errorMessage: touched[name] ? errors[name] : null,
    };
  };

  const getError = (fieldName: string) => {
    const name = fieldName as keyof T;
    return errors[name];
  };

  const isFieldError = (fieldName: string) => Boolean(getError(fieldName));

  const isFormUntouched = () => Object.keys(touched).length === 0;

  const validate = async () => {
    setShouldValidate(true);
    const error = await validateForm();
    return {
      isValid: Object.keys(error).length === 0,
      error,
    };
  };

  return {
    getTextInputProps,
    getFormFieldProps,
    getError,
    isFieldError,
    isFormUntouched,
    setFieldValue,
    touched,
    errors,
    values,
    validateForm: validate,
    ...methods,
  };
};

export default useForm;
