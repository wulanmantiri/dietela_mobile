import { useEffect, useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';
import * as ROUTES from 'constants/routes';
import CACHE_KEYS from 'constants/cacheKeys';
import { getCache } from 'utils/cache';

const useSignupEffect = () => {
  const navigation = useNavigation();

  const checkCart = useCallback(async () => {
    const dietProfileId = await getCache(CACHE_KEYS.dietProfileId);
    if (!dietProfileId) {
      navigation.reset({
        index: 0,
        routes: [
          { name: ROUTES.initial },
          { name: ROUTES.allAccessQuestionnaire },
        ],
      });
    }
  }, [navigation]);

  useEffect(() => {
    checkCart();
  }, [checkCart]);
};

export default useSignupEffect;
