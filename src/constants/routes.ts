export const initial = 'initial-page';
export const allCartsExpired = 'all-carts-expired';
export const comingSoon = '*';

const questionnaire = 'questionnaire';
export const allAccessQuestionnaire = `${questionnaire}/all-access`;
export const dietelaQuizResult = `${questionnaire}/dietela-quiz-result`;
export const consentForm = `${questionnaire}/consent`;
export const extendedQuestionnaire = `${questionnaire}/extended`;
export const extendedQuestionnaireById = (id: number) =>
  `${extendedQuestionnaire}/${id}`;

export const checkout = 'checkout';
export const choosePlan = `${checkout}/choose-plan`;
export const programDetail = `${checkout}/dietela-program`;
export const nutritionistDetail = `${checkout}/nutritionist`;

export const registration = 'registration';
export const login = 'login';
export const loginChoosePlan = 'login-choose-plan';
export const nutritionistAdminLogin = 'nutritionist-admin-login';

export const payment = 'payment';
export const paymentResult = `${payment}/result`;

const client = 'client';
export const clientTabNavigation = `${client}/tab-navigation`;
export const clientTabProfile = `${client}/profile`;
export const clientRecommendation = `${client}/recommendation`;
export const clientTabWeeklyReport = `${client}/tab-report`;
export const clientChat = `${client}/chat`;

const nutritionist = 'nutritionist';
export const clientListForNutritionist = `${nutritionist}/client-list`;
export const clientProfileNutritionist = `${nutritionist}/client-profile`;
export const clientDietReportNutritionist = `${nutritionist}/client-diet-report`;
export const clientChatNutritionist = `${nutritionist}/client-chat`;

export const profileDietRecommendation = `${clientProfileNutritionist}/recommendation`;

const admin = 'admin';
export const clientListForAdmin = `${admin}/client-list`;
export const clientProfileAdmin = `${admin}/client-profile`;
export const clientDietReportAdmin = `${admin}/client-diet-report`;
export const clientChatAdmin = `${admin}/client-chat`;
export const clientDietRecommendation = `${admin}/client-diet-recommendation`;

const weeklyReport = 'weekly-report';
export const weeklyReportForm = `${weeklyReport}/form`;
export const weeklyReportChooseWeek = `${weeklyReport}/choose-week`;
export const weeklyReportChooseWeekForNutritionist = `${weeklyReport}/choose-week-for-nutritionist`;
export const weeklyReportReadOnly = `${weeklyReport}/read-only`;
