export const yesOrNo = ['Ya', 'Tidak'];

export const drinkFrequency = [
  'Tidak pernah',
  'Sangat jarang',
  'Tidak lebih dari 3 gelas per bulan',
  '1 gelas per minggu',
  '2 gelas atau lebih per minggu',
  'Hampir setiap hari',
  '1 gelas (250 ml) per hari',
  '2 gelas (500 ml) per hari',
  'Lebih dari 2 gelas per hari',
];

export const physicalActivity = [
  'Hampir tidak pernah olahraga',
  'Jogging',
  'Senam aerobic, zumba, yoga, dan sejenisnya',
  'Sepak bola atau futsal',
  'Renang',
  'Basket',
  'Bulu tangkis',
  'Voli',
];

export const likertScale5 = ['1', '2', '3', '4', '5'];
export const likertScale10 = [
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '10',
];

export const averageConsumptionOptions = [
  '0',
  '1',
  '2',
  '3',
  '4',
  'Lebih\ndari 4',
];
