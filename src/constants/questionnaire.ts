import { yesOrNo, drinkFrequency, physicalActivity } from 'constants/options';
import { TextFieldSchema, SelectFieldSchema } from 'types/form';
import { FieldType } from 'utils/form';

export const pageHeaders: string[] = [
  'Persetujuan Program Diet',
  'Identitas Diri',
  'Pola Makan',
  'Konsumsi Makanan Harian',
  'Gaya Hidup dan Kebiasaan Diet',
  'Kondisi Pribadi',
];

export const dateField: { label: string; name: string } = {
  label: 'Tanggal Lahir',
  name: 'date_of_birth',
};

export const textFields: { [_: string]: TextFieldSchema[] } = {
  identity: [
    {
      label: 'Daerah Tempat Tinggal',
      placeholder: 'Tebet, Jakarta',
      name: 'city_and_area_of_residence',
      required: true,
    },
    {
      label: 'Nomor HP',
      placeholder: 'Ex: 081234567890',
      name: 'handphone_no',
      required: true,
      keyboardType: 'numeric',
    },
    {
      label: 'Nomor WhatsApp',
      placeholder: 'Kosongkan jika sama dengan nomor HP',
      name: 'whatsapp_no',
      keyboardType: 'numeric',
    },
    {
      label: 'Lingkar Pinggang (cm)',
      placeholder: 'Ex: 100',
      name: 'waist_size',
      required: true,
      fieldType: FieldType.NUMBER,
      max: 150,
      keyboardType: 'numeric',
    },
  ],
  eatingPattern: [
    {
      label: 'Makanan apa yang hampir setiap hari Anda konsumsi?',
      placeholder: '-',
      name: 'meal_consumed_almost_every_day',
      errorMessage: 'Makanan yang dikonsumsi',
    },
    {
      label: 'Makanan apa yang tidak Anda sukai?',
      placeholder: '-',
      name: 'unliked_food',
      errorMessage: 'Makanan yang tidak disukai',
    },
    {
      label:
        'Adakah cita rasa makanan atau jenis makanan tertentu yang Anda sukai?',
      placeholder: 'Chinese food',
      name: 'preferred_food_taste',
      errorMessage: 'Cita rasa yang disukai',
    },
    {
      label: 'Makanan apa saja yang Anda harapkan muncul di menu sarapan?',
      placeholder: 'Telur',
      name: 'expected_food_on_breakfast',
      errorMessage: 'Makanan untuk sarapan',
    },
    {
      label:
        'Makanan apa saja yang Anda harapkan muncul di menu makan siang dan makan malam?',
      placeholder: 'Ayam',
      name: 'expected_food_on_lunch_dinner',
      errorMessage: 'Makanan untuk makan siang dan malam',
    },
  ],
  foodConsumption: [
    {
      label: 'Sarapan (jam 00.00 - 10.00)',
      placeholder: 'Sesuai format jawaban',
      name: 'breakfast_meal_explanation',
    },
    {
      label: 'Cemilan pagi (jam 10.00 - 12.00)',
      placeholder: 'Sesuai format jawaban',
      name: 'morning_snack_explanation',
    },
    {
      label: 'Makan siang (jam 12.00 - 15.00)',
      placeholder: 'Sesuai format jawaban',
      name: 'lunch_meal_explanation',
    },
    {
      label: 'Cemilan sore (jam 15.00 - 18.00)',
      placeholder: 'Sesuai format jawaban',
      name: 'evening_snack_explanation',
    },
    {
      label: 'Makan malam (jam 18.00 - 21.00)',
      placeholder: 'Sesuai format jawaban',
      name: 'dinner_meal_explanation',
    },
    {
      label: 'Cemilan malam (jam 21.00 - 00.00)',
      placeholder: 'Sesuai format jawaban',
      name: 'night_snack_explanation',
    },
  ],
  lifestyle: [
    {
      label: 'Makanan yang menyebabkan Anda alergi (jika ada)',
      placeholder: 'Ayam',
      name: 'food_alergies',
    },
    {
      label:
        'Jenis obat atau ramuan herbal atau minuman khusus yang pernah atau sedang Anda gunakan untuk diet (jika ada)',
      placeholder: 'TCM',
      name: 'diet_drinks',
    },
    {
      label:
        'Jenis multivitamin atau tablet suplementasi yang Anda konsumsi (jika ada)',
      placeholder: 'Natur-E',
      name: 'multivitamin_tablet_suplement',
    },
    {
      label:
        'Tuliskan hal lain yang ingin Anda ceritakan tentang pola makan atau gaya hidup Anda yang dapat membantu kami dalam mempertimbangkan saran bagi Anda?',
      multiline: true,
      name: 'diet_and_life_style_story',
    },
  ],
  healthCondition: [
    {
      label:
        'Adakah konsumsi obat-obatan rutin yang perlu kami ketahui? Jika ada, mohon sebutkan obat untuk apa atau merk obatnya.',
      placeholder:
        'Contoh: obat diabetes, obat hipertensi, dexamethasone, metformin, captopril',
      name: 'regular_drug_consumption',
    },
    {
      label:
        'Adakah riwayat penyakit atau kondisi kesehatan lainnya yang perlu kami ketahui?',
      name: 'other_disease',
    },
    {
      label: 'Apa motivasi Anda menggunakan program Dietela?',
      name: 'motivation_using_dietela',
    },
    {
      label: 'Apa yang Anda harapkan dari Nutrisionis Dietela?',
      name: 'dietela_nutritionist_expectation',
    },
    {
      label: 'Apa yang Anda harapkan dari program yang Anda pilih?',
      name: 'dietela_program_expectation',
    },
  ],
};

export const selectFields: { [_: string]: SelectFieldSchema[] } = {
  identity: [
    {
      name: 'profession',
      label: 'Pekerjaan',
      picker: true,
      choiceList: [
        'Pegawai Swasta',
        'PNS',
        'Wirausaha',
        'Self-Employed',
        'Freelancer',
        'Ibu Rumah Tangga',
        'Mahasiswa',
        'Pelajar',
        'Lainnya',
      ],
    },
    {
      name: 'last_education',
      label: 'Pendidikan Terakhir',
      picker: true,
      choiceList: ['S2 atau S3', 'S1', 'SMA', 'SMP', 'SD', 'Lainnya'],
    },
    {
      name: 'meal_preference',
      label: 'Preferensi Makanan',
      picker: true,
      choiceList: [
        'Tidak ada batasan',
        'Halal',
        'Vegetarian',
        'Lacto-Ovo Vegetarian',
        'Ovo Vegetarian',
        'Vegan',
      ],
    },
    {
      name: 'general_purpose',
      label: 'Tujuan umum yang ingin dicapai',
      choiceList: [
        'Mencapai Body Goals',
        'Mencapai Hidup Sehat Jangka Panjang',
      ],
    },
    {
      name: 'dietary_change',
      label:
        'Tipe diet atau perubahan pola makan seperti apa yang dirasa mampu diikuti oleh Anda?',
      choiceList: ['Diet ketat', 'Dimulai dari perubahan bertahap saja dulu'],
    },
    {
      name: 'has_weigher',
      label: 'Apakah Anda memiliki timbangan berat badan di rumah?',
      choiceList: yesOrNo,
    },
  ],
  eatingPattern: [
    {
      name: 'breakfast_frequency',
      label: 'Bagaimana kebiasaan sarapan Anda?',
      choiceList: ['Selalu', 'Kadang-Kadang', 'Tidak Pernah'],
    },
    {
      name: 'breakfast_meal_type',
      label: 'Jenis makanan yang Anda konsumsi saat sarapan?',
      choiceList: [
        'Makanan yang ringan saja, hanya untuk mengganjal perut sebelum beraktivitas (misalnya: roti isi atau cemilan)',
        'Makanan yang mengenyangkan perut (karbohidrat dengan lauk-pauk)',
      ],
    },
    {
      name: 'sweet_tea_consumption_frequency',
      label: 'Seberapa sering Anda mengonsumsi teh manis?',
      picker: true,
      placeholder: 'Pilih frekuensi konsumsi teh manis',
      choiceList: drinkFrequency,
    },
    {
      name: 'coffee_consumption_frequency',
      label: 'Seberapa sering Anda mengonsumsi kopi?',
      picker: true,
      placeholder: 'Pilih frekuensi konsumsi kopi',
      choiceList: drinkFrequency,
    },
    {
      name: 'milk_consumption_frequency',
      label: 'Seberapa sering Anda mengonsumsi susu?',
      picker: true,
      placeholder: 'Pilih frekuensi konsumsi susu',
      choiceList: drinkFrequency,
    },
    {
      name: 'other_drink_consumption_frequency',
      label:
        'Seberapa sering Anda mengonsumsi minuman selain yang disebutkan diatas (contoh: soft drink atau teh kemasan)?',
      picker: true,
      placeholder: 'Pilih frekuensi konsumsi minuman lainnya',
      choiceList: drinkFrequency,
    },
    {
      name: 'additional_sugar_in_a_day',
      label:
        'Berapa rata-rata banyak sendok gula yang Anda gunakan dalam sehari (seperti dalam kopi, teh, dll)?',
      picker: true,
      placeholder: 'Pilih rata-rata sendok gula',
      choiceList: [
        'Tidak pernah menggunakan gula tambahan',
        '1 sendok teh',
        '2 sendok teh',
        'Lebih dari 2 sendok teh',
        '1 sendok makan',
        '2 sendok makan',
        'Lebih dari 2 sendok makan',
      ],
    },
    {
      name: 'liquid_consumption_frequency',
      label:
        'Berapa rata-rata jumlah konsumsi cairan Anda setiap hari (selain air mineral)?',
      picker: true,
      placeholder: 'Pilih rata-rata jumlah konsumsi cairan',
      choiceList: [
        '8 gelas atau 2 liter per hari',
        'Kurang dari 8 gelas atau 2 liter per hari',
        'Lebih dari 8 gelas atau 2 liter per hari',
      ],
    },
  ],
  lifestyle: [
    {
      name: 'meal_provider',
      label: 'Siapakah yang menyediakan makanan bagi Anda?',
      choiceList: [
        'Anggota keluarga',
        'Asisten rumah tangga',
        'Beli makanan di luar (beli langsung maupun delivery)',
        'Dapat makanan di kantor',
        'Saya sendiri',
      ],
    },
    {
      name: 'cigarette_alcohol_condition',
      label: 'Apakah Anda merokok/minum minuman beralkohol?',
      choiceList: [
        'Saat ini merokok',
        'Pernah merokok tetapi sudah berhenti',
        'Terkadang mengonsumsi minuman beralkohol',
      ],
    },
    {
      name: 'physical_activity',
      label:
        'Apakah Anda melakukan salah satu dari aktivitas fisik atau olahraga berikut?',
      hasOtherChoice: true,
      otherChoiceValue: 9,
      choiceList: physicalActivity,
    },
  ],
  healthCondition: [
    {
      name: 'disease',
      label:
        'Adakah saat ini Anda dinyatakan mengidap salah satu kondisi di bawah ini oleh dokter?',
      choiceList: [
        'Diabetes Melitus Tipe II (Kencing Manis)',
        'Diabetes Melitus Tipe I (Bawaan/Keturunan)',
        'Gagal Ginjal Kronis Tingkat Akhir',
        'Gagal Ginjal Kronis Tingkat 1-4',
        'Penyakit Liver',
        'Jantung Koroner',
        'Kelainan Jantung Bawaan',
        'Kelainan Ginjal Bawaan',
        'Kanker',
        'Maag',
        'Tinggi kolesterol',
        'Tinggi asam urat',
        'Konstipasi',
        'Anemia',
        'Autoimun',
        'Lupus',
        'HIV/AIDS',
      ],
    },
    {
      name: 'complaint',
      label: 'Adakah Anda mengalami keluhan dibawah ini dalam 3 hari terakhir?',
      choiceList: [
        'Mual',
        'Muntah',
        'Susah buang air besar dalam 3 hari terakhir',
        'Kesulitan mengunyah',
        'Kesulitan menelan',
      ],
    },
  ],
};
