import { TextFieldSchema } from 'types/form';
import {
  averageConsumptionOptions,
  likertScale10,
  likertScale5,
  physicalActivity,
} from './options';
import { FieldType } from 'utils/form';

export const dietReportTextFields: { [_: string]: TextFieldSchema[] } = {
  dietReportPage1: [
    {
      label: 'Berat Badan (kg)',
      placeholder: 'Masukkan berat badan yang terakhir diukur',
      name: 'weight',
      required: true,
      fieldType: FieldType.NUMBER,
      keyboardType: 'numeric',
    },
    {
      label: 'Tinggi Badan (cm)',
      placeholder: 'Masukkan tinggi badan yang terakhir diukur',
      name: 'height',
      required: true,
      fieldType: FieldType.NUMBER,
      keyboardType: 'numeric',
    },
    {
      label: 'Lingkar Pinggang (cm)',
      placeholder: 'Masukkan lingkar pinggang yang terakhir diukur',
      name: 'waist_size',
      required: true,
      fieldType: FieldType.NUMBER,
      max: 150,
      keyboardType: 'numeric',
    },
  ],
  dietReportPage2: [
    {
      label:
        'Selama 1 minggu terakhir, berapa rata-rata total gelas air putih yang Anda minum?',
      name: 'water_consumption',
      required: true,
      placeholder: 'ex: 8',
      keyboardType: 'numeric',
      errorMessage: 'Konsumsi minuman',
    },
  ],
  dietReportPage4: [
    {
      label:
        'Dalam 1 minggu terakhir, apa saja yang sudah bisa Anda pelajari dari program ini?',
      placeholder: 'Masukkan pelajaran minggu ini',
      name: 'lesson_learned',
      required: true,
      multiline: true,
      errorMessage: 'Pelajaran minggu ini',
    },
    {
      label: `Silahkan sampaikan disini, jika Anda mempunyai:
(1) kendala, keluhan atau kesulitan dalam mengikuti program, atau
(2) masukan, saran, dan komplain terkait layanan sejauh ini.`,
      placeholder: 'Masukkan keluh kesah atau saran bagi Dietela',
      name: 'problem_faced_and_feedbacks',
      multiline: true,
    },
  ],
};

export const dietReportSelectFields: { [_: string]: any[] } = {
  dietReportPage2: [
    {
      label: 'Apakah sudah mulai terasa ada perubahan ukuran baju atau celana?',
      name: 'changes_felt',
      choiceList: likertScale5,
      scaleDescription: {
        lowest: 'Belum terasa sama sekali',
        middle: '3: Tidak ada perubahan sama sekali',
        highest: 'Sudah sangat berubah',
      },
    },
    {
      label:
        'Secara rata-rata, sebelum waktu makan selama 1 minggu terakhir ini, dimana level rasa lapar yang Anda rasakan?',
      helperText: 'Cek indikator di program book kamu ya',
      name: 'hunger_level',
      choiceList: likertScale10,
      scaleDescription: {
        lowest: 'Sangat kelaparan',
        highest: 'Sangat begah (kenyang berlebihan)',
      },
    },
    {
      label:
        'Secara rata-rata, saat berhenti makan selama 1 minggu terakhir ini, dimana level rasa kenyang yang Anda rasakan?',
      helperText: 'Cek indikator di program book kamu ya',
      name: 'fullness_level',
      choiceList: likertScale10,
      scaleDescription: {
        lowest: 'Sangat kelaparan',
        highest: 'Sangat begah (kenyang berlebihan)',
      },
    },
    {
      label:
        'Selama 1 minggu terakhir, secara rata-rata, berapa kali Anda makan berat atau makan utama dalam 1 hari?',
      name: 'heavy_meal',
      choiceList: ['1x/hari', '2x/hari', '3x/hari', 'Lebih dari 3x/hari'],
    },
    {
      label:
        'Selama 1 minggu terakhir, secara rata-rata, berapa kali Anda makan cemilan dalam 1 hari?',
      name: 'snacks',
      choiceList: [
        '1x/hari',
        '2x/hari',
        '3x/hari',
        'Lebih dari 3x/hari',
        'Hampir tidak ada',
      ],
    },
  ],
  dietReportPage3: [
    {
      label: 'Minuman manis',
      helperText: 'dalam satuan gelas',
      name: 'sweet_beverages',
      choiceList: averageConsumptionOptions,
    },
    {
      label: 'Gula pasir, gula aren, sirup, selai, atau madu',
      helperText: 'dalam satuan sendok makan',
      name: 'sugary_ingredients',
      choiceList: averageConsumptionOptions,
    },
    {
      label: 'Cemilan digoreng',
      helperText: 'dalam satuan potong',
      name: 'fried_snacks',
      choiceList: averageConsumptionOptions,
    },
    {
      label:
        'Makanan ringan asin atau gurih (seperti makanan ringan kemasan, ciki-cikian, keripik)',
      helperText: 'dalam satuan bungkus',
      name: 'umami_snacks',
      choiceList: averageConsumptionOptions,
    },
    {
      label:
        'Cemilan manis (seperti kue-kue manis, brownis, cake, biskuit, cokelat, wafer)',
      helperText: 'dalam satuan potong',
      name: 'sweet_snacks',
      choiceList: averageConsumptionOptions,
    },
    {
      label: 'Porsi buah',
      name: 'fruits_portion',
      choiceList: averageConsumptionOptions,
    },
    {
      label: 'Porsi sayur',
      name: 'vegetables_portion',
      choiceList: averageConsumptionOptions,
    },
  ],
  dietReportPage4: [
    {
      label:
        'Selama 1 minggu terakhir, pilih semua jenis aktivitas atau olahraga yang sudah Anda lakukan',
      name: 'physical_activity',
      checkbox: true,
      hasOtherChoice: true,
      otherChoiceValue: 9,
      choiceList: physicalActivity,
    },
    {
      label:
        'Selama 1 minggu terakhir, berapa total menit yang Anda habiskan untuk melakukan bergerak aktif dan olahraga diatas dalam seminggu?',
      name: 'time_for_activity',
      choiceList: [
        '0 - 60 menit',
        '60 - 100 menit',
        '100 - 120 menit',
        '120 - 150 menit',
        '150 - 175 menit',
        '175 - 200 menit',
        '200 - 250 menit',
        'Lebih dari 250 menit',
      ],
    },
    {
      label: 'Sejauh ini, bagaimana perasaan Anda dalam mengikuti program?',
      name: 'feeling_rating',
      choiceList: [
        '⭐️: Rasanya mau menyerah saja',
        '⭐️⭐️: Capek, susah, bosen, males, repot, sibuk',
        '⭐️⭐️⭐️: Biasa aja, meski ada kendala tapi semua bisa diatur',
        '⭐️⭐️⭐️⭐️: Lancar terus, semangat cukup stabil, gak ada masalah',
        '⭐️⭐️⭐️⭐️⭐️: Super seneng, semangat banget, worry-free lah',
      ],
    },
  ],
};
