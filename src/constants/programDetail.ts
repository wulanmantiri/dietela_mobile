interface ProgramDetails {
  description: string;
  details: {
    title: string;
    content: string[];
  }[];
}

export const balancedDietProgram: ProgramDetails = {
  description:
    'Dalam Program yang didesain khusus secara personalised untuk kamu. Sehat bukan hanya diukur dari angka di timbangan. Namun seberapa banyak perubahan kebiasaan sehat yang kamu capai, yang akan merubah pola makan kamu dan hidup lebih sehat untuk jangka panjang! #DietGayaKuSendiri',
  details: [
    {
      title: 'Di akhir program, yang akan kamu rasakan:',
      content: [
        'Badan terasa lebih fit, bonus turun berat badan atau hasil lab mulai terkontrol',
        'Mindset positif terhadap tubuh dan makanan',
        'Lebih tau cara memilih jenis makanan yang cocok untuk kamu',
        'Bisa mengukur porsi makan sendiri',
        'Bisa mengenali sinyal lapar dan kenyang (mindful eating)',
        'Lebih bisa menghargai tubuh dan kesehatan',
      ],
    },
    {
      title: 'Untuk mencapai goal sehat kamu, program ini menyediakan:',
      content: [
        'Konseling One-on-One melalui video call dengan nutrisionis pribadimu',
        'Penilaian Kondisi Gizi',
        'Personalized Meal Plan (sesuai kondisi, kebutuhan dan kesukaan kamu)',
        'Konsultasi harian (Online Chat) dengan nutrisionis pribadimu',
        'Materi coaching selama 1, 3, atau 6 bulan',
        'Personalized Menu 10 Hari (di update setiap bulan, selama berlangganan)',
        'Evaluasi',
      ],
    },
  ],
};

export const bodyGoalsProgram: ProgramDetails = {
  description:
    'Untuk mendapatkan berat badan yang ideal, pola makan yang sehat adalah kunci utama. Tapi bukan berarti kamu harus ninggalin makanan enak yang kamu suka! Kuncinya adalah makan sesuai dengan kebutuhanmu #DietGayaKuSendiri. Pilih tipe program yang sesuai dengan target kamu.',
  details: [
    {
      title: 'Di akhir program, yang akan kamu rasakan:',
      content: [
        'Berat badan sesuai goal realistis kamu',
        'Tetap enjoy makanan yang kamu suka dengan lebih mindful',
        'Menghargai tubuh kamu kamu sendiri',
        'Berat badan yang bertahan untuk jangka panjang, anti yo-yo diet',
      ],
    },
    {
      title:
        'Dalam Body Goals Program, kamu akan dipandu untuk mendapatkan Berat badan ideal dengan cara yang seimbang, aman dan terbukti secara ilmiah. Program ini menyediakan:',
      content: [
        'Konseling One-on-One melalui video call dengan nutrisionis pribadimu',
        'Penilaian Kondisi Gizi',
        'Personalized Meal Plan (sesuai kondisi, kebutuhan dan kesukaan kamu)',
        'Konsultasi harian (Online Chat) dengan nutrisionis pribadimu',
        'Materi coaching selama 1, 3, atau 6 bulan',
        'Personalized Menu 10 Hari (di update setiap bulan, selama berlangganan)',
        'Evaluasi',
      ],
    },
  ],
};

export const babyProgram: ProgramDetails = {
  description:
    'Program Body for Baby memastikan calon ibu terpenuhi kebutuhan gizinya, lewat program yang mudah diterapkan. Mulai perjalanan kamu menjadi seorang ibu, bersama Dietela!',
  details: [
    {
      title: 'Di akhir program, yang akan kamu rasakan:',
      content: [
        'Berat badan semakin ideal',
        'Gula darah normal',
        'Badan yang lebih bugar',
        'Perbaikan pola makan dan gaya hidup yang dapat mendukung kesuburan',
      ],
    },
    {
      title:
        'Program Dietela ini akan mendampingi perjalanan kamu mejadi ibu lewat:',
      content: [
        'Konseling One-on-One melalui video call dengan nutrisionis pribadimu',
        'Penilaian Kondisi Gizi',
        'Personalized Meal Plan (sesuai kondisi, kebutuhan dan kesukaan kamu)',
        'Konsultasi harian (Online Chat) dengan nutrisionis pribadimu',
        'Materi coaching selama 1, 3, atau 6 bulan',
        'Personalized Menu 10 Hari (di update setiap bulan, selama berlangganan)',
        'Evaluasi',
      ],
    },
  ],
};

export const oneTimeConsultation: ProgramDetails = {
  description:
    'Kamu belum siap berkomitmen jangka panjang? Program 1 minggu Dietela ini layaknya sneak peak agar kamu bisa lebih kenal pola makan dan gaya hidup sehat yang cocok untukmu.',
  details: [
    {
      title: 'Yang akan kamu dapatkan:',
      content: [
        'Konseling One-on-One melalui video call dengan nutrisionis pribadimu',
        'Penilaian Kondisi Gizi',
        'Personalized Meal Plan (sesuai kondisi, kebutuhan dan kesukaan kamu)',
        'Konsultasi harian (Online Chat) dengan nutrisionis pribadimu',
        'Materi coaching 7 hari',
        'Personalized Menu 10 Hari',
      ],
    },
  ],
};
