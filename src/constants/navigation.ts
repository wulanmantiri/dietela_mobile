import * as ROUTES from 'constants/routes';
import {
  // Public
  AllAccessQuestionnaire,
  ChoosePlan,
  DietelaQuizResult,
  InitialPage,
  ManualRegistrationPage,
  LoginPage,
  NutritionistAdminLogin,
  ProgramDetail,
  NutritionistDetail,

  // Private
  Checkout,
  PaymentResult,
  ReadOnlyDietProfile,
  ClientListNutritionist,
  ComingSoonPage,
  ClientDietRecommendationForAdmin,
  PaymentWebView,
  ProfileDietRecommendation,
  ClientListAdmin,
  LoginChoosePlan,
  ClientNavigation,
  ChooseWeekForNutritionist,
  AllCartsExpiredPage,
} from 'scenes';
import { FC } from 'react';
import DietReportForNutritionist from 'scenes/nutritionist/DietReportForNutritionist';

export interface NavRoute<T = any> {
  name: string;
  component: FC<T>;
  header?: string;
}

const navigation: NavRoute[] = [
  {
    name: ROUTES.allAccessQuestionnaire,
    component: AllAccessQuestionnaire,
    header: 'Dietela Quiz',
  },
  {
    name: ROUTES.dietelaQuizResult,
    component: DietelaQuizResult,
    header: 'Dietela Quiz Result',
  },
  {
    name: ROUTES.choosePlan,
    component: ChoosePlan,
    header: 'Choose Plan',
  },
  {
    name: ROUTES.programDetail,
    component: ProgramDetail,
    header: 'Program Dietela',
  },
  {
    name: ROUTES.nutritionistDetail,
    component: NutritionistDetail,
    header: 'Nutrisionis',
  },
];

export const publicNavigation: NavRoute[] = [
  {
    name: ROUTES.initial,
    component: InitialPage,
  },
  ...navigation,
  {
    name: ROUTES.registration,
    component: ManualRegistrationPage,
    header: 'Registrasi',
  },
  {
    name: ROUTES.login,
    component: LoginPage,
    header: 'Login',
  },
  {
    name: ROUTES.loginChoosePlan,
    component: LoginChoosePlan,
    header: 'Login',
  },
  {
    name: ROUTES.nutritionistAdminLogin,
    component: NutritionistAdminLogin,
    header: 'Login Tim Dietela',
  },
];

export const takeQuizClientNavigation: NavRoute[] = [
  ...navigation,
  {
    name: ROUTES.allCartsExpired,
    component: AllCartsExpiredPage,
    header: 'Dietela',
  },
  {
    name: ROUTES.checkout,
    component: Checkout,
    header: 'Checkout',
  },
  {
    name: ROUTES.payment,
    component: PaymentWebView,
    header: 'Pembayaran',
  },
  {
    name: ROUTES.paymentResult,
    component: PaymentResult,
  },
  {
    name: ROUTES.clientTabNavigation,
    component: ClientNavigation,
    header: 'Dietela',
  },
];

export const cartsExpireClientNavigation: NavRoute[] = [
  {
    name: ROUTES.allCartsExpired,
    component: AllCartsExpiredPage,
    header: 'Dietela',
  },
  {
    name: ROUTES.checkout,
    component: Checkout,
    header: 'Checkout',
  },
  {
    name: ROUTES.payment,
    component: PaymentWebView,
    header: 'Pembayaran',
  },
  {
    name: ROUTES.paymentResult,
    component: PaymentResult,
  },
  ...navigation,
  {
    name: ROUTES.clientTabNavigation,
    component: ClientNavigation,
    header: 'Dietela',
  },
];

export const unpaidClientNavigation: NavRoute[] = [
  {
    name: ROUTES.checkout,
    component: Checkout,
    header: 'Checkout',
  },
  {
    name: ROUTES.payment,
    component: PaymentWebView,
    header: 'Pembayaran',
  },
  {
    name: ROUTES.paymentResult,
    component: PaymentResult,
  },
  ...navigation,
  {
    name: ROUTES.clientTabNavigation,
    component: ClientNavigation,
    header: 'Dietela',
  },
];

export const paidClientNavigation: NavRoute[] = [
  {
    name: ROUTES.clientTabNavigation,
    component: ClientNavigation,
    header: 'Dietela',
  },
];

export const nutritionistNavigation: NavRoute[] = [
  {
    name: ROUTES.clientListForNutritionist,
    component: ClientListNutritionist,
    header: 'Daftar Klien',
  },
  {
    name: ROUTES.clientChatNutritionist,
    component: ComingSoonPage,
    header: 'Chat Klien',
  },
  {
    name: ROUTES.weeklyReportChooseWeekForNutritionist,
    component: ChooseWeekForNutritionist,
    header: 'Riwayat Laporan Diet Klien',
  },
  {
    name: ROUTES.clientDietReportNutritionist,
    component: DietReportForNutritionist,
    header: 'Laporan Klien',
  },
  {
    name: ROUTES.clientProfileNutritionist,
    component: ReadOnlyDietProfile,
    header: 'Profil Klien',
  },
  {
    name: ROUTES.profileDietRecommendation,
    component: ProfileDietRecommendation,
    header: 'Rekomendasi Profil Diet',
  },
];

export const adminNavigation: NavRoute[] = [
  {
    name: ROUTES.clientListForAdmin,
    component: ClientListAdmin,
    header: 'Daftar Klien',
  },
  {
    name: ROUTES.clientProfileAdmin,
    component: ReadOnlyDietProfile,
    header: 'Profil Klien',
  },
  {
    name: ROUTES.clientDietReportAdmin,
    component: ComingSoonPage,
    header: 'Profil Klien',
  },
  {
    name: ROUTES.clientChatAdmin,
    component: ComingSoonPage,
    header: 'Profil Klien',
  },
  {
    name: ROUTES.clientDietRecommendation,
    component: ClientDietRecommendationForAdmin,
    header: 'Rekomendasi Profil Diet',
  },
];

// FOR TESTING PURPOSES
export const testNavigation: NavRoute[] = [
  ...unpaidClientNavigation,
  ...nutritionistNavigation,
  ...adminNavigation,
  {
    name: ROUTES.initial,
    component: InitialPage,
  },
  {
    name: ROUTES.registration,
    component: ManualRegistrationPage,
    header: 'Registrasi',
  },
  {
    name: ROUTES.login,
    component: LoginPage,
    header: 'Login',
  },
  {
    name: ROUTES.loginChoosePlan,
    component: LoginChoosePlan,
    header: 'Login',
  },
  {
    name: ROUTES.nutritionistAdminLogin,
    component: NutritionistAdminLogin,
    header: 'Login Tim Dietela',
  },
];
