import {
  ProgramRecommendations,
  DietelaProgram,
} from 'services/dietelaQuiz/quizResult';
import {
  babyProgram,
  balancedDietProgram,
  bodyGoalsProgram,
  oneTimeConsultation,
} from './programDetail';

const prices = {
  oneWeek: '239.900',
  oneMonth: '609.900',
  threeMonths: '1.659.900',
  sixMonths: '3.449.000',
};

export const dietPrograms = {
  [DietelaProgram.TRIAL]: {
    title: 'One Week Trial (7 Hari)',
    price: prices.oneWeek,
    details: oneTimeConsultation,
  },
  [DietelaProgram.BALANCED_1]: {
    title: 'Balanced Diet (1 Bulan)',
    price: prices.oneMonth,
    details: balancedDietProgram,
  },
  [DietelaProgram.BALANCED_3]: {
    title: 'Balanced Diet (3 Bulan)',
    price: prices.threeMonths,
    details: balancedDietProgram,
  },
  [DietelaProgram.BALANCED_6]: {
    title: 'Balanced Diet (6 Bulan)',
    price: prices.sixMonths,
    details: balancedDietProgram,
  },
  [DietelaProgram.GOALS_1]: {
    title: 'Body Goals (1 Bulan)',
    price: prices.oneMonth,
    details: bodyGoalsProgram,
  },
  [DietelaProgram.GOALS_3]: {
    title: 'Body Goals (3 Bulan)',
    price: prices.threeMonths,
    details: bodyGoalsProgram,
  },
  [DietelaProgram.GOALS_6]: {
    title: 'Body Goals (6 Bulan)',
    price: prices.sixMonths,
    details: bodyGoalsProgram,
  },
  [DietelaProgram.BABY_1]: {
    title: 'Body for Baby (1 Bulan)',
    price: prices.oneMonth,
    details: babyProgram,
  },
  [DietelaProgram.BABY_3]: {
    title: 'Body for Baby (3 Bulan)',
    price: prices.threeMonths,
    details: babyProgram,
  },
};

export const defaultProgramRecommendations: ProgramRecommendations = {
  priority_1: DietelaProgram.TRIAL,
  priority_2: null,
};
