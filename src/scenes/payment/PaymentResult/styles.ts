import { StyleSheet } from 'react-native';
import { layoutStyles, colors, typography } from 'styles';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    ...layoutStyles,
  },
  success: {
    backgroundColor: colors.primaryVariant,
  },
  pending: {
    backgroundColor: colors.warning,
  },
  error: {
    backgroundColor: colors.danger,
  },
  priceLabel: {
    borderTopColor: colors.border,
    borderTopWidth: 1,
    paddingTop: 20,
    paddingBottom: 10,
    color: 'white',
  },
  price: {
    color: 'white',
    paddingBottom: 80,
    fontSize: 24,
  },
  text: {
    ...typography.headingLarge,
    color: 'white',
    marginVertical: 30,
    textAlign: 'center',
  },
  buttonStyles: {
    borderColor: 'white',
    borderWidth: 2,
  },
  buttonText: {
    ...typography.headingMedium,
    color: 'white',
  },
});
