import React from 'react';
import { fireEvent, waitFor, render } from 'utils/testing';
import axios from 'axios';

import * as ROUTES from 'constants/routes';

import PaymentResult from '.';
import { TransactionStatus } from 'services/payment/models';
import { DietelaProgram } from 'services/dietelaQuiz/quizResult';

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('PaymentResult', () => {
  const retrieveCartApi = (transaction_status: TransactionStatus) =>
    Promise.resolve({
      status: 200,
      data: {
        id: 1,
        program: {
          id: 1,
          unique_code: DietelaProgram.TRIAL,
        },
        transaction_status,
      },
    });
  const userContext = {
    user: { cart_id: 1 },
  };

  it('shows success message with successful payment', async () => {
    mockAxios.request.mockImplementationOnce(() =>
      retrieveCartApi(TransactionStatus.SUCCESS),
    );

    const { queryByText, getByText } = render(
      <PaymentResult />,
      ROUTES.paymentResult,
      { userContext },
    );
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    expect(queryByText(/pembayaran berhasil/i)).toBeTruthy();

    const button = getByText(/Mulai Perjalanan Diet Anda/i);
    expect(button).toBeTruthy();
    fireEvent.press(button);
  });

  it('shows please pay message with pending payment', async () => {
    mockAxios.request.mockImplementationOnce(() =>
      retrieveCartApi(TransactionStatus.PENDING),
    );
    const { queryByText, getByText } = render(
      <PaymentResult />,
      ROUTES.paymentResult,
      { userContext },
    );
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    expect(
      queryByText(/Mohon segera menyelesaikan pembayaran Anda/i),
    ).toBeTruthy();

    const button = getByText(/Batalkan Pembayaran/i);
    expect(button).toBeTruthy();
    fireEvent.press(button);
  });

  it('shows error message with failed payment', async () => {
    mockAxios.request.mockImplementationOnce(() =>
      retrieveCartApi(TransactionStatus.ERROR),
    );
    const { queryByText } = render(<PaymentResult />, ROUTES.paymentResult, {
      userContext,
    });
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    expect(queryByText(/pembayaran gagal/i)).toBeTruthy();
  });
});
