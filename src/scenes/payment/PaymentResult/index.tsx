import React, { FC, useContext } from 'react';
import { View } from 'react-native';
import { Text, Icon, Button } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

import * as ROUTES from 'constants/routes';
import { TransactionStatus } from 'services/payment/models';
import { dietPrograms } from 'constants/dietelaProgram';
import { typographyStyles } from 'styles';
import { useApi } from 'hooks';
import { retrieveCartApi } from 'services/payment';

import { styles } from './styles';
import { UserContext } from 'provider';

const PaymentResult: FC = () => {
  const navigation = useNavigation();
  const { user } = useContext(UserContext);

  const { data } = useApi(() => retrieveCartApi(`${user.cart_id}`));

  const redirectToChoosePlan = () => {
    navigation.reset({
      index: 0,
      routes: [{ name: ROUTES.choosePlan }],
    });
  };

  const redirectToQuestionnaire = () => {
    navigation.reset({
      index: 0,
      routes: [{ name: ROUTES.clientTabNavigation }],
    });
  };

  if (!data) {
    return null;
  }
  if (data.transaction_status === TransactionStatus.SUCCESS) {
    return (
      <View style={[styles.container, styles.success]}>
        <Icon
          name="check-circle"
          color="white"
          type="font-awesome-5"
          size={100}
        />
        <Text style={styles.text}>Transaksi pembayaran berhasil!</Text>
        <Text style={[styles.priceLabel, typographyStyles.overlineBig]}>
          Jumlah Tagihan
        </Text>
        <Text style={[typographyStyles.displayMediumMontserrat, styles.price]}>
          Rp{dietPrograms[data.program.unique_code].price}
        </Text>
        <Button
          title="Mulai Perjalanan Diet Anda"
          type="outline"
          buttonStyle={styles.buttonStyles}
          titleStyle={styles.buttonText}
          onPress={redirectToQuestionnaire}
        />
      </View>
    );
  } else if (data.transaction_status === TransactionStatus.PENDING) {
    return (
      <View style={[styles.container, styles.pending]}>
        <Icon
          name="credit-card-clock-outline"
          color="white"
          type="material-community"
          size={100}
        />
        <Text style={styles.text}>
          Mohon segera menyelesaikan pembayaran Anda.
        </Text>
        <Button
          title="Batalkan Pembayaran"
          type="outline"
          buttonStyle={styles.buttonStyles}
          titleStyle={styles.buttonText}
          onPress={redirectToChoosePlan}
        />
      </View>
    );
  } else {
    return (
      <View style={[styles.container, styles.error]}>
        <Icon name="error" color="white" size={100} />
        <Text style={styles.text}>Transaksi pembayaran gagal.</Text>
        <Button
          title="Kembali"
          type="outline"
          buttonStyle={styles.buttonStyles}
          titleStyle={styles.buttonText}
          onPress={redirectToChoosePlan}
        />
      </View>
    );
  }
};

export default PaymentResult;
