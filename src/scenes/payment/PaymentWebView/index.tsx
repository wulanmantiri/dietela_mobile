import React, { FC, useContext } from 'react';
import { WebView } from 'react-native-webview';
import { useRoute, useNavigation } from '@react-navigation/native';

import * as ROUTES from 'constants/routes';
import { Loader } from 'components/core';
import { updateCartApi } from 'services/payment';
import { TransactionStatus } from 'services/payment/models';
import { API_BASE_URL } from 'env';
import { UserContext } from 'provider';

const PaymentWebView: FC = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const { url } = route.params as { url: string };
  const { user, setUser } = useContext(UserContext);

  const postPay = async (state: any) => {
    if (state.url.startsWith(`${API_BASE_URL}/payment/midtrans-redirection`)) {
      const response = await updateCartApi(user.cart_id, {
        transaction_status: TransactionStatus.SUCCESS,
      });
      setUser({
        ...user,
        nutritionist: response.data?.nutritionist || null,
      });
      navigation.navigate(ROUTES.paymentResult);
    }
  };

  return (
    <WebView
      source={{ uri: url }}
      originWhitelist={['*']}
      renderLoading={Loader}
      onNavigationStateChange={postPay}
    />
  );
};

export default PaymentWebView;
