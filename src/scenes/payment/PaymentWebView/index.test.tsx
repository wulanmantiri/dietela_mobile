import React from 'react';
import { render } from '@testing-library/react-native';

import PaymentWebView from '.';

const mockedNavigate = jest.fn();

jest.mock('axios');
jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
    useRoute: () => ({
      params: {
        url: 'url',
      },
    }),
  };
});

describe('PaymentWebView', () => {
  it('renders correctly', () => {
    render(<PaymentWebView />);
  });
});
