import React from 'react';
import { render } from '@testing-library/react-native';

import ClientNavigation, {
  WeeklyReportStackScreen,
  ExtQuestionnaireStackScreen,
} from '.';
import { NavigationContainer } from '@react-navigation/native';
import { mockUserContext } from 'mocks/userContext';
import { UserContext } from 'provider';

describe('ClientNavigation', () => {
  it('provides tab if user has finished onboarding', () => {
    const userProviderValues = {
      ...mockUserContext,
      user: {
        ...mockUserContext.user,
        is_finished_onboarding: true,
      },
    };

    render(
      <UserContext.Provider value={userProviderValues}>
        <NavigationContainer>
          <ClientNavigation />
        </NavigationContainer>
      </UserContext.Provider>,
    );
  });

  it('does not provide tab if user has not finished onboarding', () => {
    const userProviderValues = {
      ...mockUserContext,
      user: {
        ...mockUserContext.user,
        is_finished_onboarding: false,
      },
    };

    render(
      <UserContext.Provider value={userProviderValues}>
        <NavigationContainer>
          <ClientNavigation />
        </NavigationContainer>
      </UserContext.Provider>,
    );
  });

  test('weekly report renders correctly', () => {
    render(
      <NavigationContainer>
        <WeeklyReportStackScreen />
      </NavigationContainer>,
    );
  });

  test('profile renders correctly', () => {
    render(
      <NavigationContainer>
        <ExtQuestionnaireStackScreen />
      </NavigationContainer>,
    );
  });
});
