import { StyleSheet } from 'react-native';
import { colors, typography } from 'styles';

export const styles = StyleSheet.create({
  sceneStyle: {
    backgroundColor: 'transparent',
  },
});

export const tabBarOptions = {
  activeTintColor: colors.primaryVariant,
  labelStyle: {
    paddingVertical: 0,
    marginVertical: 0,
    ...typography.headingMedium,
    fontSize: 14,
  },
  style: {
    height: 66,
    paddingTop: 8,
    paddingBottom: 8,
  },
};
