import React, { FC, useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Icon } from 'react-native-elements';

import * as ROUTES from 'constants/routes';
import { screenOptions } from 'app/styles';
import { styles, tabBarOptions } from './styles';

import ClientRecommendation from 'scenes/recommendation/ClientRecommendation';
import WeeklyReport from 'scenes/report/WeeklyReport';
import ExtendedQuestionnaire from 'scenes/questionnaire/ExtendedQuestionnaire';
import {
  ConsentForm,
  Questionnaire1,
  Questionnaire2,
  Questionnaire3,
  Questionnaire4,
  Questionnaire5,
} from 'scenes/questionnaire/ExtendedQuestionnaire/components';
import ReadOnlyWeeklyReport from 'scenes/report/ReadOnlyWeeklyReport';
import ChooseWeekForClient from 'scenes/report/ChooseWeekForClient';
import ChatForClient from 'scenes/chat/ChatForClient';
import { UserContext } from 'provider';

interface NavRoute<T = any> {
  name: string;
  component: FC<T>;
  header?: string;
}

const profileClientNavigation: NavRoute[] = [
  {
    name: ROUTES.extendedQuestionnaire,
    component: ExtendedQuestionnaire,
  },
  ...[
    {
      component: ConsentForm,
    },
    {
      component: Questionnaire1,
    },
    {
      component: Questionnaire2,
    },
    {
      component: Questionnaire3,
    },
    {
      component: Questionnaire4,
    },
    {
      component: Questionnaire5,
    },
  ].map((nav, id) => ({
    ...nav,
    name: ROUTES.extendedQuestionnaireById(id),
  })),
];

const ExtQuestionnaireStack = createStackNavigator();
export const ExtQuestionnaireStackScreen: FC = () => (
  <ExtQuestionnaireStack.Navigator screenOptions={screenOptions}>
    {profileClientNavigation.map((nav, i) => (
      <ExtQuestionnaireStack.Screen
        key={`ext-questionnaire-nav-${i}`}
        name={nav.name}
        component={nav.component}
        options={{
          headerShown: false,
        }}
      />
    ))}
  </ExtQuestionnaireStack.Navigator>
);

const reportClientNavigation: NavRoute[] = [
  {
    name: ROUTES.weeklyReportChooseWeek,
    component: ChooseWeekForClient,
  },
  {
    name: ROUTES.weeklyReportForm,
    component: WeeklyReport,
  },
  {
    name: ROUTES.weeklyReportReadOnly,
    component: ReadOnlyWeeklyReport,
  },
];

const WeeklyReportStack = createStackNavigator();
export const WeeklyReportStackScreen: FC = () => (
  <WeeklyReportStack.Navigator screenOptions={screenOptions}>
    {reportClientNavigation.map((nav, i) => (
      <WeeklyReportStack.Screen
        key={`report-client-nav-${i}`}
        name={nav.name}
        component={nav.component}
        options={{
          headerShown: false,
        }}
      />
    ))}
  </WeeklyReportStack.Navigator>
);

const ClientTab = createBottomTabNavigator();
const ClientNavigation: FC = () => {
  const { user } = useContext(UserContext);

  if (!user.is_finished_onboarding) {
    return <ExtQuestionnaireStackScreen />;
  }
  return (
    <ClientTab.Navigator
      initialRouteName={ROUTES.clientTabProfile}
      sceneContainerStyle={styles.sceneStyle}
      tabBarOptions={tabBarOptions}>
      <ClientTab.Screen
        name={ROUTES.clientTabProfile}
        component={ExtQuestionnaireStackScreen}
        options={{
          tabBarLabel: 'Profil',
          tabBarIcon: ({ color, size }) => (
            <Icon name="home" color={color} size={size} />
          ),
        }}
      />
      <ClientTab.Screen
        name={ROUTES.clientRecommendation}
        component={ClientRecommendation}
        options={{
          tabBarLabel: 'Rekomendasi',
          tabBarIcon: ({ color, size }) => (
            <Icon
              name="nutrition"
              type="material-community"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <ClientTab.Screen
        name={ROUTES.clientTabWeeklyReport}
        component={WeeklyReportStackScreen}
        options={{
          tabBarLabel: 'Laporan Diet',
          tabBarIcon: ({ color, size }) => (
            <Icon
              name="file-document-edit-outline"
              type="material-community"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <ClientTab.Screen
        name={ROUTES.clientChat}
        component={ChatForClient}
        options={{
          tabBarLabel: 'Chat',
          tabBarIcon: ({ color, size }) => (
            <Icon name="chatbox" type="ionicon" color={color} size={size} />
          ),
        }}
      />
    </ClientTab.Navigator>
  );
};

export default ClientNavigation;
