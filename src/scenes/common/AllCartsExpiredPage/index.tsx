import React, { FC } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import * as ROUTES from 'constants/routes';
import { EmptyDataPage, BigButton } from 'components/core';
import { styles } from './styles';

const AllCartsExpiredPage: FC = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.center}>
      <EmptyDataPage text="Program diet Anda telah selesai!" />
      <BigButton
        title="konsultasi lagi"
        onPress={() => navigation.navigate(ROUTES.allAccessQuestionnaire)}
      />
    </View>
  );
};

export default AllCartsExpiredPage;
