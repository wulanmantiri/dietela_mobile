import { StyleSheet } from 'react-native';
import { typography, layoutStyles } from 'styles';

export const styles = StyleSheet.create({
  center: {
    flex: 1,
    ...layoutStyles,
  },
  noRecomText: {
    ...typography.headingMedium,
    textAlign: 'center',
    marginTop: 10,
  },
});
