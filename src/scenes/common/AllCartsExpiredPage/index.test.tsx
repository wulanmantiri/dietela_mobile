import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import AllCartsExpiredPage from '.';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
  };
});

describe('AllCartsExpiredPage', () => {
  it('has call-to-action button that navigates to Dietela Quiz', () => {
    const { getByText, queryByText } = render(<AllCartsExpiredPage />);
    expect(queryByText(/konsultasi lagi/i)).toBeTruthy();
    fireEvent.press(getByText(/konsultasi lagi/i));

    expect(mockedNavigate).toHaveBeenCalled();
  });
});
