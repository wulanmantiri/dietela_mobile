import React, { FC } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, Text, ImageBackground, Image } from 'react-native';

import { BigButton, Link } from 'components/core';
import { banner_girl_eating, logo_white_small } from 'assets/images';
import * as ROUTES from 'constants/routes';
import { layoutStyles, typographyStyles } from 'styles';

import { styles } from './styles';

const InitialPage: FC = () => {
  const navigation = useNavigation();

  return (
    <ImageBackground
      source={banner_girl_eating}
      style={styles.bgImage}
      testID="background">
      <View style={[layoutStyles, styles.view]}>
        <Image source={logo_white_small} style={styles.logo} testID="logo" />
        <View style={styles.headingContainer}>
          <Text
            style={[typographyStyles.displayMediumMontserrat, styles.heading]}>
            Online Nutritionist Pertama di Indonesia
          </Text>
          <Text style={[typographyStyles.bodyMedium, styles.heading]}>
            Hadir untuk mendefinisikan ulang kata “Diet” untuk Anda!
          </Text>
          <Text style={[typographyStyles.bodyMedium, styles.heading]}>
            Apapun masalah diet Anda, konsultasikan bersama kami!
          </Text>
        </View>
        <View style={styles.ctaContainer}>
          <BigButton
            title="konsultasi sekarang"
            onPress={() => navigation.navigate(ROUTES.allAccessQuestionnaire)}
          />
          <Link
            title="Sudah punya akun? Login disini"
            onPress={() => navigation.navigate(ROUTES.login)}
          />
        </View>
      </View>
    </ImageBackground>
  );
};

export default InitialPage;
