import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import InitialPage from '.';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: mockedNavigate,
      reset: mockedNavigate,
    }),
  };
});

describe('InitialPage', () => {
  test('has background image', () => {
    const { queryByTestId } = render(<InitialPage />);

    expect(queryByTestId('background')).toBeTruthy();
  });

  test('has Dietela logo', () => {
    const { queryByTestId } = render(<InitialPage />);

    expect(queryByTestId('logo')).toBeTruthy();
  });

  test('has call-to-action button that navigates to Dietela Quiz', () => {
    const { getByText, queryByText } = render(<InitialPage />);
    expect(queryByText(/konsultasi sekarang/i)).toBeTruthy();
    fireEvent.press(getByText(/konsultasi sekarang/i));

    expect(mockedNavigate).toHaveBeenCalled();
  });

  test('has link button that navigates to Login Page', () => {
    const { getByText, queryByText } = render(<InitialPage />);
    expect(queryByText(/Login disini/i)).toBeTruthy();
    fireEvent.press(getByText(/Login disini/i));

    expect(mockedNavigate).toHaveBeenCalled();
  });
});
