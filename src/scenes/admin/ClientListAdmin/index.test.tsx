import React from 'react';
import { render, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';
import axios from 'axios';

import ClientListAdmin from '.';
import { mockClientList } from 'mocks/clientList';

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('ClientListAdmin', () => {
  it('renders correctly', async () => {
    mockAxios.request.mockImplementationOnce(() =>
      Promise.resolve({
        status: 200,
        data: [],
      }),
    );
    render(<ClientListAdmin />, ROUTES.clientListForAdmin);
    await waitFor(() => expect(mockAxios.request).toBeCalled());
  });

  it('shows correct client list', async () => {
    mockAxios.request.mockImplementationOnce(() =>
      Promise.resolve({
        status: 200,
        data: mockClientList,
      }),
    );

    const { queryByText } = render(
      <ClientListAdmin />,
      ROUTES.clientListForAdmin,
    );
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    expect(queryByText(mockClientList[0].user.name)).toBeTruthy();
    expect(queryByText(/Download Csv/i)).toBeTruthy();
  });
});
