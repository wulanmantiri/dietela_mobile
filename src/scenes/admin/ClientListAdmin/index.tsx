import React, { FC } from 'react';
import * as ROUTES from 'constants/routes';
import { UserRole } from 'services/auth/models';
import { ClientList } from 'components/core';

const ClientListAdmin: FC = () => {
  return (
    <ClientList
      role={UserRole.ADMIN}
      clientProfileRoute={ROUTES.clientProfileAdmin}
      clientDietReportRoute={ROUTES.clientDietReportAdmin}
    />
  );
};

export default ClientListAdmin;
