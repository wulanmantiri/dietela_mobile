import React, { FC } from 'react';
import { View } from 'react-native';

import { Text, Button, ListItem, Icon } from 'react-native-elements';
import { colors } from 'styles';

import { Props } from './types';
import { styles } from './styles';

const PricingCard: FC<Props> = ({
  title,
  price,
  info = [],
  onButtonPress,
  onReadMore,
  isSelected,
}) => {
  const buttonProps = isSelected
    ? {
        buttonStyle: styles.selectedButton,
        icon: {
          name: 'check-circle',
          size: 20,
          color: 'white',
        },
        title: 'Terpilih',
      }
    : {
        buttonStyle: styles.buttonStyle,
        titleStyle: styles.titleStyle,
        title: `Pilih ${title}`,
      };

  return (
    <View style={[styles.container, isSelected ? styles.selected : null]}>
      <Text style={styles.title}>{title}</Text>
      {price ? (
        <View style={styles.basePriceContainer}>
          <Text style={styles.currency}>Rp</Text>
          <Text style={styles.basePrice}>{price}</Text>
        </View>
      ) : null}
      <View style={styles.info}>
        {info.map((item, i) => (
          <ListItem key={`info${i}`} containerStyle={styles.info}>
            <Icon name="check" color={colors.primary} />
            <ListItem.Content>
              <ListItem.Title>{item.trim()}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        ))}
      </View>
      <Button
        onPress={onButtonPress}
        containerStyle={styles.buttonContainer}
        {...buttonProps}
      />
      <Button
        title="Baca selengkapnya"
        type="clear"
        icon={{
          name: 'arrow-forward',
          size: 25,
          color: colors.primaryVariant,
        }}
        onPress={onReadMore}
        iconRight
        titleStyle={styles.readMore}
      />
    </View>
  );
};

export default PricingCard;
