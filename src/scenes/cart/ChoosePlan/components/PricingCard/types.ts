import { PricingCardProps } from 'react-native-elements';

export interface Props extends PricingCardProps {
  onReadMore: () => void;
  isSelected?: boolean;
}
