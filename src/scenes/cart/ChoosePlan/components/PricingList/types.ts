export interface Props {
  headerText: string;
  items: {
    title: string;
    price?: string;
    value: string | number;
    info?: string[];
    onReadMore: () => void;
  }[];
  value: any;
  onChange: (_: any) => void;
  onButtonPress?: () => void;
}
