import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';
import axios from 'axios';

import ChoosePlan from '.';

import * as ROUTES from 'constants/routes';
import CACHE_KEYS from 'constants/cacheKeys';
import { mockProgramRecommendations } from 'mocks/quizResult';
import { dietPrograms } from 'constants/dietelaProgram';
import { setCache } from 'utils/cache';
import { nutritionistDummy } from 'mocks/cart';

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('ChoosePlan', () => {
  const nutritionists = [nutritionistDummy];
  const retrieveNutritionistsApi = () =>
    Promise.resolve({
      status: 200,
      data: nutritionists,
    });

  it('provides default program recommendations if not in cache', async () => {
    mockAxios.request.mockImplementationOnce(retrieveNutritionistsApi);

    const { getAllByText } = render(<ChoosePlan />, ROUTES.choosePlan);
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const defaultProgram = getAllByText(/One Week Trial/i);
    expect(defaultProgram).toBeTruthy();
  });

  it('redirects to checkout page when authenticated user clicks Bayar button', async () => {
    await setCache(
      CACHE_KEYS.programRecommendations,
      JSON.stringify(mockProgramRecommendations),
    );
    const createCartApi = () =>
      Promise.resolve({
        status: 200,
        data: {
          id: 1,
          program: { id: 1 },
          nutritionist: nutritionists[0],
        },
      });

    mockAxios.request.mockImplementationOnce(retrieveNutritionistsApi);

    const { getByText, queryAllByText } = render(
      <ChoosePlan />,
      ROUTES.choosePlan,
      { userContext: { user: { id: 1 }, isAuthenticated: true } },
    );
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const selectProgramButton = getByText(/Pilih One Week Trial/i);
    expect(selectProgramButton).toBeTruthy();
    fireEvent.press(selectProgramButton);

    const nextButton = getByText(/Lanjut/i);
    expect(nextButton).toBeTruthy();
    fireEvent.press(nextButton);

    const selectNutritionistButton = getByText(/Pilih Wendy/i);
    expect(selectNutritionistButton).toBeTruthy();
    fireEvent.press(selectNutritionistButton);

    mockAxios.request.mockImplementationOnce(createCartApi);

    const payButton = getByText(/Bayar/i);
    expect(payButton).toBeTruthy();
    await waitFor(() => fireEvent.press(payButton));

    const checkoutPage = queryAllByText(/Checkout/i);
    expect(checkoutPage).toBeTruthy();
  });

  it('redirects to registration page when non-authenticated user clicks Bayar button', async () => {
    const createCartApi = () =>
      Promise.resolve({
        status: 200,
        data: {
          id: 1,
          program: { id: 1 },
          nutritionist: nutritionists[0],
        },
      });

    mockAxios.request.mockImplementationOnce(retrieveNutritionistsApi);

    const { getByText } = render(<ChoosePlan />, ROUTES.choosePlan, {
      userContext: { isAuthenticated: false },
    });
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const selectProgramButton = getByText(/Pilih One Week Trial/i);
    expect(selectProgramButton).toBeTruthy();
    fireEvent.press(selectProgramButton);

    const nextButton = getByText(/Lanjut/i);
    expect(nextButton).toBeTruthy();
    fireEvent.press(nextButton);

    const selectNutritionistButton = getByText(/Pilih Wendy/i);
    expect(selectNutritionistButton).toBeTruthy();
    fireEvent.press(selectNutritionistButton);

    mockAxios.request.mockImplementationOnce(createCartApi);

    const payButton = getByText(/Bayar/i);
    expect(payButton).toBeTruthy();
    await waitFor(() => fireEvent.press(payButton));
  });

  it('does not redirect to cart page when submit fails', async () => {
    const createCartApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      });

    mockAxios.request.mockImplementationOnce(retrieveNutritionistsApi);

    const { getByText, queryByText } = render(
      <ChoosePlan />,
      ROUTES.choosePlan,
    );
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const selectProgramButton = getByText(/Pilih One Week Trial/i);
    expect(selectProgramButton).toBeTruthy();
    fireEvent.press(selectProgramButton);

    const nextButton = getByText(/Lanjut/i);
    expect(nextButton).toBeTruthy();
    fireEvent.press(nextButton);

    const selectNutritionistButton = getByText(/Pilih Wendy/i);
    expect(selectNutritionistButton).toBeTruthy();
    fireEvent.press(selectNutritionistButton);

    mockAxios.request.mockImplementationOnce(createCartApi);

    const payButton = getByText(/Bayar/i);
    expect(payButton).toBeTruthy();
    await waitFor(() => fireEvent.press(payButton));

    const checkoutPage = queryByText(/Checkout/i);
    expect(checkoutPage).toBeFalsy();
  });

  it('redirects to program detail page when user clicks Baca selengkapnya button for program', async () => {
    mockAxios.request.mockImplementationOnce(retrieveNutritionistsApi);

    const { getAllByText, getByText } = render(
      <ChoosePlan />,
      ROUTES.choosePlan,
    );
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const readMoreButton = getAllByText(/Baca selengkapnya/i)[0];
    expect(readMoreButton).toBeTruthy();
    fireEvent.press(readMoreButton);

    const program = mockProgramRecommendations.priority_1;
    const programDetails = dietPrograms[program].details;

    const programDetailPage = getByText(programDetails.description);
    expect(programDetailPage).toBeTruthy();
  });

  it('redirects to nutritionist detail page when user clicks Baca selengkapnya button for nutritionist', async () => {
    mockAxios.request.mockImplementationOnce(retrieveNutritionistsApi);

    const { getAllByText, getByText, queryAllByText } = render(
      <ChoosePlan />,
      ROUTES.choosePlan,
    );
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const selectProgramButton = getByText(/Pilih One Week Trial/i);
    expect(selectProgramButton).toBeTruthy();
    fireEvent.press(selectProgramButton);

    const nextButton = getByText(/Lanjut/i);
    expect(nextButton).toBeTruthy();
    fireEvent.press(nextButton);

    const nutritionistPage = getByText(/Nutrisionis/i);
    expect(nutritionistPage).toBeTruthy();

    const readMoreButton = getAllByText(/Baca selengkapnya/i)[0];
    expect(readMoreButton).toBeTruthy();
    fireEvent.press(readMoreButton);

    const nutritionistDetailPage = queryAllByText(
      nutritionistDummy.full_name_and_degree,
    );
    expect(nutritionistDetailPage).toBeTruthy();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
});
