import React from 'react';
import { render } from '@testing-library/react-native';

import NutritionistDetail from '.';

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: jest.fn(),
    }),
    useRoute: () => ({
      params: {
        ntr: {
          id: 1,
          full_name_and_degree: 'Wendy',
          registration_certificate_no: '432491859',
          university: 'UGM',
          mastered_nutritional_problems: 'Manajemen berat badan, hipertensi',
          handled_age_group: '12 - 17 tahun (Remaja)',
          another_practice_place: 'RSIU',
          languages: 'Bahasa Indonesia, Jepang',
        },
      },
    }),
  };
});

describe('NutritionistDetail', () => {
  it('renders correctly', () => {
    render(<NutritionistDetail />);
  });
});
