import { Nutritionist } from 'services/nutritionists/models';

const splitMastery = (mastery: string) => {
  return splitStatistics(mastery, 'ahli menangani', '☑️', ';');
};

const splitAgeRange = (ageRange: string) => {
  return splitStatistics(ageRange, 'umur klien ditangani', '🎂', ',');
};

const splitLanguage = (language: string) => {
  return splitStatistics(language, 'bahasa dikuasai', '💬', ',');
};

const splitStatistics = (
  stats: string,
  label: string,
  emoji: string,
  splitBy: string,
) => {
  const statsArr = stats.split(splitBy);
  return statsArr.map((statsStr, i) => {
    if (i === 0) {
      return {
        label: label,
        emote: emoji,
        content: statsStr,
      };
    } else {
      return {
        emote: emoji,
        content: statsStr.trim(),
      };
    }
  });
};

export const toStatisticList = (nutritionist: Nutritionist) => {
  return [
    {
      label: 'nama lengkap',
      emote: '👨‍💼',
      content: nutritionist.full_name_and_degree,
    },
    {
      label: 'no. sertifikat',
      emote: '📃',
      content: nutritionist.registration_certificate_no,
    },
    {
      label: 'universitas',
      emote: '🎓',
      content: nutritionist.university,
    },
    ...splitMastery(nutritionist.mastered_nutritional_problems),
    ...splitAgeRange(nutritionist.handled_age_group),
    {
      label: 'tempat praktek lainnya',
      emote: '🏥',
      content: nutritionist.another_practice_place,
    },
    ...splitLanguage(nutritionist.languages),
  ];
};
