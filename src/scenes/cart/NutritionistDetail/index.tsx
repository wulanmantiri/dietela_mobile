import React, { FC } from 'react';
import { useRoute } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';
import { AutoImage, Statistic } from 'components/core';
import { Row, Section } from 'components/layout';

import { Nutritionist } from 'services/nutritionists/models';
import { default_nutritionist } from 'assets/images';

import { layoutStyles } from 'styles';
import { HeadingLarge } from 'components/typography';
import { toStatisticList } from './utils';

const NutritionistDetail: FC = () => {
  const route = useRoute();
  const { ntr: nutritionist } = route.params as { ntr: Nutritionist };
  const ntrStats = toStatisticList(nutritionist);

  return (
    <ScrollView contentContainerStyle={layoutStyles}>
      <HeadingLarge text={nutritionist.full_name_and_degree} />

      <Section>
        <AutoImage source={default_nutritionist} />
      </Section>

      {ntrStats.map((stat, ii) =>
        stat.label ? (
          <Section key={`stat${ii}`}>
            <Row>
              <Statistic
                title={stat.label}
                emote={stat.emote}
                content={stat.content}
              />
            </Row>
          </Section>
        ) : (
          <Row key={`stat${ii}`}>
            <Statistic
              title={stat.label}
              emote={stat.emote}
              content={stat.content}
            />
          </Row>
        ),
      )}
      <Section />
    </ScrollView>
  );
};

export default NutritionistDetail;
