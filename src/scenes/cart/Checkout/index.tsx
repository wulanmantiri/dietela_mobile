import React, { FC, useContext } from 'react';
import { View } from 'react-native';
import { Text, Button } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

import { Loader, BigButton, Toast, EmptyDataPage } from 'components/core';
import { Section } from 'components/layout';
import * as ROUTES from 'constants/routes';
import { dietPrograms } from 'constants/dietelaProgram';
import { useApi } from 'hooks';
import { retrieveCartApi, payWithMidtransApi } from 'services/payment';
import { typographyStyles } from 'styles';

import { styles } from './styles';
import { CheckoutCard } from './components';
import { UserContext } from 'provider';

const Checkout: FC = () => {
  const navigation = useNavigation();
  const { user } = useContext(UserContext);

  const { isLoading, data } = useApi(() => retrieveCartApi(user.cart_id));

  const pay = async () => {
    const response = await payWithMidtransApi(user.cart_id);
    if (response.success && response.data) {
      navigation.navigate(ROUTES.payment, { url: response.data.redirect_url });
    } else {
      Toast.show({
        type: 'error',
        text1: 'Anda sudah membayar tagihan ini.',
        text2:
          'Mohon restart aplikasi Dietela untuk memulai perjalanan diet Anda.',
      });
    }
  };

  if (isLoading) {
    return <Loader />;
  }
  if (!data) {
    return <EmptyDataPage text="Anda belum memilih program diet" />;
  }
  return (
    <View style={styles.container}>
      <View>
        <CheckoutCard
          content={dietPrograms[data.program.unique_code].title}
          type="program"
          onReadMore={() =>
            navigation.navigate(ROUTES.programDetail, {
              id: data.program.unique_code,
            })
          }
        />
        <CheckoutCard
          content={data.nutritionist.full_name_and_degree}
          type="nutritionist"
          onReadMore={() =>
            navigation.navigate(ROUTES.nutritionistDetail, {
              ntr: data.nutritionist,
            })
          }
        />
      </View>
      <View style={styles.priceContainer}>
        <Text style={typographyStyles.headingMedium}>Harga:</Text>
        <View style={styles.currencyContainer}>
          <Text style={styles.currency}>Rp</Text>
          <Text style={styles.basePrice}>
            {dietPrograms[data.program.unique_code].price}
          </Text>
        </View>
      </View>
      <View>
        <Button
          title="ganti pilihan"
          type="outline"
          onPress={() =>
            navigation.reset({
              index: 0,
              routes: [{ name: ROUTES.choosePlan }],
            })
          }
          buttonStyle={styles.buttonStyle}
          titleStyle={[typographyStyles.overlineBig, styles.titleStyle]}
        />
        <Section>
          <BigButton
            title="bayar dengan midtrans"
            onPress={pay}
            disabled={!data.id}
          />
        </Section>
      </View>
    </View>
  );
};

export default Checkout;
