export interface Props {
  type: 'program' | 'nutritionist';
  content: string;
  onReadMore: () => void;
}
