import React from 'react';
import { render } from '@testing-library/react-native';

import CheckoutCard from '.';

describe('CheckoutCard component', () => {
  it('for program renders correctly', () => {
    render(
      <CheckoutCard type="program" content="yes" onReadMore={jest.fn()} />,
    );
  });

  it('for nutritionist renders correctly', () => {
    render(
      <CheckoutCard type="nutritionist" content="yes" onReadMore={jest.fn()} />,
    );
  });
});
