import { StyleSheet } from 'react-native';
import { colors, typography } from 'styles';

export const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    borderRadius: 6,
    padding: 20,
    paddingBottom: 15,
    marginTop: 20,
    backgroundColor: colors.primaryYellow,
    borderColor: colors.primaryVariant,
  },
  title: {
    ...typography.headingMedium,
    marginVertical: 15,
  },
  readMore: {
    color: colors.primary,
  },
  buttonStyle: {
    margin: 0,
    padding: 0,
  },
});
