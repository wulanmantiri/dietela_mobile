import { StyleSheet } from 'react-native';
import { layoutStyles, typography, colors } from 'styles';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    flexGrow: 1,
    ...layoutStyles,
    marginBottom: 10,
  },
  topContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  priceContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  currencyContainer: {
    display: 'flex',
    flexDirection: 'row',
  },
  currency: {
    ...typography.bodyMedium,
    color: colors.formLabel,
    marginRight: 8,
    marginTop: 8,
  },
  basePrice: {
    ...typography.displayMediumMontserrat,
    color: colors.primary,
  },
  buttonStyle: {
    borderColor: colors.buttonYellow,
    borderWidth: 1,
  },
  titleStyle: {
    color: colors.textBlack,
  },
});
