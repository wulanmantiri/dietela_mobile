import React from 'react';
import { waitFor, fireEvent, render } from 'utils/testing';
import axios from 'axios';

import Checkout from '.';

import * as ROUTES from 'constants/routes';
import { DietelaProgram } from 'services/dietelaQuiz/quizResult';

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('Checkout', () => {
  const nutritionist = {
    id: 1,
    full_name_and_degree: 'Wendy',
    registration_certificate_no: '123',
    university: 'UI',
    mastered_nutritional_problems: 'diet',
    handled_age_group: '18',
    another_practice_place: '-',
    languages: 'English',
  };
  const retrieveCartApi = () =>
    Promise.resolve({
      status: 200,
      data: {
        id: 1,
        program: {
          id: 1,
          unique_code: DietelaProgram.TRIAL,
        },
        nutritionist,
      },
    });
  const userContext = {
    user: { cart_id: 1 },
  };

  it('redirects to program detail page when user clicks Baca selengkapnya button for program', async () => {
    mockAxios.request.mockImplementationOnce(retrieveCartApi);

    const { getAllByText, getByText } = render(<Checkout />, ROUTES.checkout, {
      userContext,
    });
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const chosenProgram = getByText(/One Week Trial/i);
    expect(chosenProgram).toBeTruthy();

    const readMoreButton = getAllByText(/Baca selengkapnya/i)[0];
    expect(readMoreButton).toBeTruthy();
    fireEvent.press(readMoreButton);
  });

  it('redirects to nutritionist detail page when user clicks Baca selengkapnya button for nutritionist', async () => {
    mockAxios.request.mockImplementationOnce(retrieveCartApi);

    const { getAllByText, getByText } = render(<Checkout />, ROUTES.checkout, {
      userContext,
    });
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const chosenNutritionist = getByText(/Wendy/i);
    expect(chosenNutritionist).toBeTruthy();

    const readMoreButton = getAllByText(/Baca selengkapnya/i)[1];
    expect(readMoreButton).toBeTruthy();
    fireEvent.press(readMoreButton);
  });

  it('redirects to choose plan page when user clicks Ganti Pilihan button', async () => {
    mockAxios.request.mockImplementationOnce(retrieveCartApi);

    const { getByText } = render(<Checkout />, ROUTES.checkout, {
      userContext,
    });
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const changePlanButton = getByText(/ganti pilihan/i);
    expect(changePlanButton).toBeTruthy();
    await waitFor(() => fireEvent.press(changePlanButton));
  });

  it('redirect to payment webview when user clicks Bayar button and submit successful', async () => {
    mockAxios.request.mockImplementationOnce(retrieveCartApi);

    const { getByText } = render(<Checkout />, ROUTES.checkout, {
      userContext,
    });
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const payWithMidtransApi = () =>
      Promise.resolve({
        status: 200,
        data: {
          redirect_url: 'url',
        },
      });
    mockAxios.request.mockImplementationOnce(payWithMidtransApi);

    const payButton = getByText(/bayar dengan midtrans/i);
    expect(payButton).toBeTruthy();
    await waitFor(() => fireEvent.press(payButton));
  });

  it('does not redirect to payment when user clicks Bayar button but submit fails', async () => {
    mockAxios.request.mockImplementationOnce(retrieveCartApi);

    const { getByText } = render(<Checkout />, ROUTES.checkout, {
      userContext,
    });
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const payWithMidtransApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      });
    mockAxios.request.mockImplementationOnce(payWithMidtransApi);

    const payButton = getByText(/bayar dengan midtrans/i);
    expect(payButton).toBeTruthy();
    await waitFor(() => fireEvent.press(payButton));
  });

  it('shows empty data page when fetch cart fails', async () => {
    mockAxios.request.mockImplementationOnce(() =>
      Promise.resolve({
        status: 400,
        response: {
          data: undefined,
        },
      }),
    );

    const { getByText } = render(<Checkout />, ROUTES.checkout, {
      userContext,
    });
    await waitFor(() => expect(mockAxios.request).toBeCalled());

    const emptyDataPage = getByText(/Anda belum memilih program diet/i);
    expect(emptyDataPage).toBeTruthy();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
});
