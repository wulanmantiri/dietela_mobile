import React from 'react';
import { render } from '@testing-library/react-native';

import ProgramDetail from '.';
import { dietPrograms } from 'constants/dietelaProgram';
import { DietelaProgram } from 'services/dietelaQuiz/quizResult';

jest.mock('@react-navigation/native', () => {
  return {
    useRoute: () => ({
      params: { id: 'GOALS_1' },
    }),
  };
});

describe('ProgramDetail', () => {
  it('shows program details content correctly', () => {
    const { getByText } = render(<ProgramDetail />);
    const programDetail = dietPrograms[DietelaProgram.GOALS_1].details;

    const description = getByText(programDetail.description);
    expect(description).toBeTruthy();

    const firstDetail = programDetail.details[0];
    expect(getByText(firstDetail.title)).toBeTruthy();

    const secondDetail = programDetail.details[1];
    expect(getByText(secondDetail.title)).toBeTruthy();
  });
});
