import { useRoute } from '@react-navigation/native';
import { Section } from 'components/layout';
import { BodyMedium, HeadingLarge } from 'components/typography';
import { dietPrograms } from 'constants/dietelaProgram';
import React, { FC } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Icon, ListItem } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { DietelaProgram } from 'services/dietelaQuiz/quizResult';
import { colors, layoutStyles } from 'styles';

interface IdProgram {
  id: DietelaProgram;
}

const styles = StyleSheet.create({
  padding: { padding: 10 },
});

const ProgramDetail: FC = () => {
  const route = useRoute();
  const { id } = route.params as IdProgram;
  const programTitle = dietPrograms[id].title;
  const programDetail = dietPrograms[id].details;

  return (
    <ScrollView contentContainerStyle={layoutStyles}>
      <HeadingLarge text={programTitle} />

      <Section>
        <BodyMedium text={programDetail.description} />
      </Section>

      {programDetail.details.map((detail, i) => (
        <View key={`details${i}`}>
          <Section>
            <Text>{detail.title}</Text>
          </Section>

          {detail.content.map((item, j) => (
            <ListItem key={`info${j}`} containerStyle={styles.padding}>
              <Icon name="check" color={colors.primary} />
              <ListItem.Content>
                <ListItem.Title>{item}</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          ))}
        </View>
      ))}
    </ScrollView>
  );
};

export default ProgramDetail;
