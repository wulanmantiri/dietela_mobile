import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import ChatForClient from '.';
import { UserContext } from 'provider/UserContext';
import { Linking } from 'react-native';
import { mockUserContext } from 'mocks/userContext';

describe('ChatForClient', () => {
  it('call Linking open url when user clicks WhatsApp button and phone number exists', () => {
    const userProviderValues = {
      ...mockUserContext,
      user: {
        ...mockUserContext.user,
        nutritionist: {
          full_name_and_degree: 'tes',
          phone_number: '09',
        },
      },
    };

    const { getByText } = render(
      <UserContext.Provider value={userProviderValues}>
        <ChatForClient />
      </UserContext.Provider>,
    );

    const spy = jest.spyOn(Linking, 'openURL');

    const wabutton = getByText('Hubungi via WhatsApp');
    expect(wabutton).toBeTruthy();
    fireEvent.press(wabutton);

    expect(spy).toHaveBeenCalled();
    spy.mockReset();
  });

  it('provides placeholder if nutritionist does not exist', () => {
    const { getByText } = render(<ChatForClient />);
    const placeholder = getByText(
      /Anda tidak didaftarkan dengan sebuah nutrisionis/i,
    );
    expect(placeholder).toBeTruthy();
  });
});
