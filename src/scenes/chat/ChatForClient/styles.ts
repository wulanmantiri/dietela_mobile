import { StyleSheet } from 'react-native';
import { colors, typography, layoutStyles } from 'styles';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    ...layoutStyles,
    paddingHorizontal: 30,
  },
  nutritionist: {
    textAlign: 'center',
    ...typography.headingMedium,
    color: colors.formLabel,
    marginBottom: 16,
  },
  name: {
    textAlign: 'center',
    ...typography.headingLarge,
    color: colors.textBlack,
    marginBottom: 40,
  },
  buttonStyle: {
    backgroundColor: colors.primaryVariant,
  },
  titleStyle: {
    color: 'white',
    ...typography.headingMedium,
    marginLeft: 10,
  },
});
