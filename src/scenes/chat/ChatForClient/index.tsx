import React, { FC, useContext } from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';

import { UserContext } from 'provider';
import { redirectToWA } from 'utils/chat';
import { styles } from './styles';
import { EmptyDataPage } from 'components/core';

const ChatForClient: FC = () => {
  const { user } = useContext(UserContext);

  return (
    <View style={styles.container}>
      {user.nutritionist ? (
        <View>
          <Text style={styles.nutritionist}>Nutrisionis Anda:</Text>
          <Text style={styles.name}>
            👩🏻‍⚕️ {user.nutritionist.full_name_and_degree}
          </Text>
          <Button
            title="Hubungi via WhatsApp"
            icon={{
              name: 'whatsapp',
              type: 'material-community',
              color: 'white',
              size: 28,
            }}
            onPress={() => redirectToWA(user.nutritionist?.phone_number)}
            buttonStyle={styles.buttonStyle}
            titleStyle={styles.titleStyle}
          />
        </View>
      ) : (
        <EmptyDataPage text="Anda tidak didaftarkan dengan sebuah nutrisionis." />
      )}
    </View>
  );
};

export default ChatForClient;
