import { DietProfileResponse } from 'services/dietelaQuiz/models';
import { sayurBuahStatus } from 'services/dietelaQuiz/quizResult';
import { ResultPageContent } from '../types';
import { genericResult, getAnswerString } from '../utils';

const kecukupanSayurBuah = (
  response: DietProfileResponse,
): ResultPageContent => {
  const result = response.quiz_result;
  const infosContent = result.vegetable_and_fruit_sufficiency;
  const infoStatus = sayurBuahStatus[infosContent].status;
  const recommendation = result.vegetable_and_fruit_diet_recommendation;

  const vegetables = getAnswerString(
    'vegetables_in_one_day',
    response.vegetables_in_one_day,
  );
  const fruits = getAnswerString(
    'fruits_in_one_day',
    response.fruits_in_one_day,
  );

  const questions = [vegetables, fruits];

  return genericResult(
    'Kecukupan Sayur dan Buah',
    'status kecukupan',
    infosContent,
    infoStatus,
    recommendation,
    questions,
  );
};

export default kecukupanSayurBuah;
