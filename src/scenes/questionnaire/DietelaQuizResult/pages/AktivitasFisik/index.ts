import { DietProfileResponse } from 'services/dietelaQuiz/models';
import { aktivitasFisikStatus } from 'services/dietelaQuiz/quizResult';
import { ResultPageContent } from '../types';
import { genericResult, getAnswerString, getInfos } from '../utils';

const aktivitasFisik = (response: DietProfileResponse): ResultPageContent => {
  const result = response.quiz_result;
  const recommendation = result.physical_activity_recommendation;
  const { infosContent, infoStatus } = getInfos(
    aktivitasFisikStatus[recommendation],
  );

  // body_activity
  const activity = getAnswerString('body_activity', response.body_activity);

  return genericResult(
    'Aktivitas fisik',
    'status aktivitas fisik',
    infosContent,
    infoStatus,
    recommendation,
    [activity],
  );
};

export default aktivitasFisik;
