import { gglStatus } from 'services/dietelaQuiz/quizResult';
import { DietProfileResponse } from 'services/dietelaQuiz/models';
import { ResultPageContent } from '../types';
import { genericResult, getAnswerString } from '../utils';

const asupanGGL = (response: DietProfileResponse): ResultPageContent => {
  const result = response.quiz_result;
  const infosContent = result.sugar_salt_fat_problem;
  const infoStatus = gglStatus[infosContent].status;
  const recommendation = result.sugar_salt_fat_diet_recommendation;

  const fried = getAnswerString(
    'fried_food_in_one_day',
    response.fried_food_in_one_day,
  );
  const snack = getAnswerString(
    'sweet_snacks_in_one_day',
    response.sweet_snacks_in_one_day,
  );
  const drink = getAnswerString(
    'sweet_drinks_in_one_day',
    response.sweet_drinks_in_one_day,
  );
  const packaged = getAnswerString(
    'packaged_food_in_one_day',
    response.packaged_food_in_one_day,
  );

  const questions = [fried, snack, drink, packaged];

  return genericResult(
    'Asupan Gula, Garam, dan Lemak (GGL)',
    'status asupan ggl',
    infosContent,
    infoStatus,
    recommendation,
    questions,
  );
};

export default asupanGGL;
