import React, { FC } from 'react';
import { ScrollView, View, Text } from 'react-native';
import { layoutStyles, typographyStyles } from 'styles';
import { BigButton, InfoCard, ResultCard, Statistic } from 'components/core';
import { Column, Row, Section } from 'components/layout';
import * as ROUTES from 'constants/routes';

import { ResultPageContent } from '../types';
import { styles } from './style';
import { useNavigation } from '@react-navigation/native';

const ResultPage: FC<{
  content: ResultPageContent;
  cta?: boolean;
}> = ({ content, cta }) => {
  const navigation = useNavigation();

  return (
    <View style={layoutStyles}>
      <ScrollView>
        <Text style={typographyStyles.headingLarge}>{content.title}</Text>
        <Section>
          <ResultCard
            status={content.mainInfo.status}
            infos={content.mainInfo.infos}
          />
        </Section>

        {content.sections.map((section, i) => (
          <Section key={i}>
            <Text style={[typographyStyles.bodySmall]}>{section.header}</Text>

            {section.content.statistics?.map((statRow, ii) => (
              <View style={styles.marginTop} key={`stat${ii}`}>
                <Row>
                  {statRow.map((stat, iii) => (
                    <Column key={`statrow${iii}`}>
                      <Statistic
                        title={stat.label}
                        emote={stat.emote}
                        content={stat.content}
                      />
                    </Column>
                  ))}
                </Row>
              </View>
            ))}

            {section.content.textCard?.map((text) => (
              <View style={styles.marginTop} key={text}>
                <InfoCard content={text} />
              </View>
            ))}
            {cta && (
              <View style={styles.marginTop}>
                <BigButton
                  title="mulai konsultasi"
                  onPress={() => navigation.navigate(ROUTES.choosePlan)}
                />
              </View>
            )}
          </Section>
        ))}
      </ScrollView>
    </View>
  );
};

export default ResultPage;
