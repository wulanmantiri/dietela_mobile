import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';

import ResultPage from '.';
import { Status } from 'services/dietelaQuiz/quizResult';
import { ResultPageContent } from '../types';

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: jest.fn(),
    }),
  };
});

describe('ResultPage', () => {
  const infos = [
    {
      label: 'Indeks massa tubuh',
      content: '31.6',
    },
    {
      label: 'Status',
      content: 'Obesitas 2',
    },
  ];

  const heightWeightData = {
    header: 'Dihitung berdasarkan data:',
    content: {
      statistics: [
        [
          {
            label: 'berat badan',
            emote: '⚖️',
            content: '77 kg',
          },
          {
            label: 'tinggi badan',
            emote: '📐',
            content: '156 cm',
          },
        ],
      ],
    },
  };

  const weightRecommendation = {
    header:
      'Untuk orang dengan tinggi 156 cm, berikut rentang berat badan yang ideal:',
    content: {
      statistics: [
        [
          {
            label: 'minimal',
            emote: '◀️',
            content: '46.2 kg',
          },
          {
            label: 'maksimal',
            emote: '▶️',
            content: '55.9 kg',
          },
        ],
      ],
      textCard: ['Usahakan berat badan kamu masuk ke rentang ideal, ya!'],
    },
  };

  const content: ResultPageContent = {
    title: 'Status Berat Badan',
    mainInfo: {
      infos: infos,
      status: Status.HEALTHY,
    },
    sections: [heightWeightData, weightRecommendation],
  };

  it('renders correctly when there is CTA button', () => {
    const { getAllByText } = render(<ResultPage content={content} cta />);
    const button = getAllByText(/mulai konsultasi/i).pop();
    if (button) {
      fireEvent.press(button);
    }
  });

  it('shows the correct content', () => {
    const { getByText } = render(<ResultPage content={content} />);

    // Check for result page title
    expect(getByText(content.title)).toBeTruthy();

    // Check for each info
    infos.forEach((info) => {
      expect(getByText(info.label)).toBeTruthy();
      expect(getByText(info.content)).toBeTruthy();
    });

    // Check for height & weight data shows correct statistics
    expect(getByText(heightWeightData.header)).toBeTruthy();
    heightWeightData.content.statistics[0].forEach((stats) => {
      expect(getByText(stats.label)).toBeTruthy();
      expect(getByText(stats.emote)).toBeTruthy();
      expect(getByText(stats.content)).toBeTruthy();
    });

    // Check for weight recommendation shows correct statistics
    expect(getByText(weightRecommendation.header)).toBeTruthy();
    weightRecommendation.content.statistics[0].forEach((stats) => {
      expect(getByText(stats.label)).toBeTruthy();
      expect(getByText(stats.emote)).toBeTruthy();
      expect(getByText(stats.content)).toBeTruthy();
    });
  });
});
