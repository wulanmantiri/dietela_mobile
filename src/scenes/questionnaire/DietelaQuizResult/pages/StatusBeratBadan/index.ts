import { DietProfileResponse } from 'services/dietelaQuiz/models';
import { bodyMassStatus } from 'services/dietelaQuiz/quizResult';
import { ResultPageContent } from '../types';

const statusBeratBadan = (response: DietProfileResponse): ResultPageContent => {
  const result = response.quiz_result;
  const bmi = result.body_mass_index.toString();
  const nutritionStatus = result.nutrition_status;
  const infoStatus = bodyMassStatus[nutritionStatus].status;
  const weight = result.weight.toFixed(0).toString();
  const height = result.height.toString();
  const minimum = result.ideal_weight_range.min.toFixed(1).toString();
  const maximum = result.ideal_weight_range.max.toFixed(1).toString();

  return {
    title: 'Status Berat Badan',
    mainInfo: {
      infos: [
        {
          label: 'Indeks massa tubuh',
          content: bmi,
        },
        {
          label: 'Status',
          content: nutritionStatus,
        },
      ],
      status: infoStatus,
    },
    sections: [
      {
        header: 'Dihitung berdasarkan data:',
        content: {
          statistics: [
            [
              {
                label: 'berat badan',
                emote: '⚖️',
                content: `${weight} kg`,
              },
              {
                label: 'tinggi badan',
                emote: '📐',
                content: `${height} cm`,
              },
            ],
          ],
        },
      },
      {
        header:
          'Untuk orang dengan tinggi 156 cm, berikut rentang berat badan yang ideal:',
        content: {
          statistics: [
            [
              {
                label: 'minimal',
                emote: '◀️',
                content: `${minimum} kg`,
              },
              {
                label: 'maksimal',
                emote: '▶️',
                content: `${maximum} kg`,
              },
            ],
          ],
          textCard: ['Usahakan berat badan kamu masuk ke rentang ideal, ya!'],
        },
      },
    ],
  };
};

export default statusBeratBadan;
