import aktivitasFisik from './AktivitasFisik';
import asupanGGL from './AsupanGGL';
import detailEnergiSehari from './DetailEnergiSehari';
import kebutuhanEnergiZatGizi from './KebutuhanEnergiZatGizi';
import kecukupanSayurBuah from './KecukupanSayurBuah';
import makanBesar from './MakanBesar';
import makanCemilan from './MakanCemilan';
import makanPagi from './MakanPagi';
import statusBeratBadan from './StatusBeratBadan';

export const pages = [
  statusBeratBadan,
  kebutuhanEnergiZatGizi,
  kecukupanSayurBuah,
  asupanGGL,
  makanBesar,
  makanCemilan,
  makanPagi,
  aktivitasFisik,
  detailEnergiSehari,
];

export { default as ResultPage } from './ResultPage';
