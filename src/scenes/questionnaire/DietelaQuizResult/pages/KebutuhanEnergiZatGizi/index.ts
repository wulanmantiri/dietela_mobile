import { DietProfileResponse } from 'services/dietelaQuiz/models';
import { ResultPageContent } from '../types';
import { getAnswerString } from '../utils';

const kebutuhanEnergiZatGizi = (
  response: DietProfileResponse,
): ResultPageContent => {
  const result = response.quiz_result;
  const energi = result.daily_energy_needs.toFixed(1).toString();
  const protein = result.daily_nutrition_needs.protein_needs
    .toFixed(1)
    .toString();
  const karbohidrat = result.daily_nutrition_needs.carbohydrate_needs
    .toFixed(1)
    .toString();
  const lemak = result.daily_nutrition_needs.fat_needs.toFixed(1).toString();
  const serat = result.daily_nutrition_needs.fiber_needs.toFixed(1).toString();
  const genderType = ['Pria', 'Wanita'];
  const age = result.age.toString();
  const weight = result.weight.toFixed(0).toString();
  const height = result.height.toString();
  const gender = genderType[result.gender - 1];

  const body = getAnswerString('body_activity', response.body_activity)!;
  const special = getAnswerString(
    'special_condition',
    response.special_condition,
  )!;

  return {
    title: 'Kebutuhan Energi dan Zat Gizi dalam Sehari',
    mainInfo: {
      infos: [
        {
          label: 'energi per hari',
          content: `${energi} kkal`,
        },
        {
          label: 'protein per hari',
          content: `${protein} g`,
        },
        {
          label: 'karbohidrat per hari',
          content: `${karbohidrat} g`,
        },
        {
          label: 'lemak per hari',
          content: `${lemak} g`,
        },
        {
          label: 'serat per hari',
          content: `${serat} g`,
        },
      ],
      status: 'healthy',
    },
    sections: [
      {
        header: 'Dihitung berdasarkan data:',
        content: {
          statistics: [
            [
              {
                label: 'jenis kelamin',
                emote: '🧑',
                content: gender,
              },
              {
                label: 'Usia',
                emote: '🎂',
                content: `${age} tahun`,
              },
            ],
            [
              {
                label: 'berat badan',
                emote: '⚖️',
                content: `${weight} kg`,
              },
              {
                label: 'tinggi badan',
                emote: '📐',
                content: `${height} cm`,
              },
            ],
          ],
        },
      },
      {
        header: 'Dan juga berdasarkan jawaban kuis kamu:',
        content: {
          textCard: [body, special],
        },
      },
    ],
  };
};

export default kebutuhanEnergiZatGizi;
