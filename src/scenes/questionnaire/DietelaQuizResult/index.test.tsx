import React from 'react';
import { render } from '@testing-library/react-native';

import DietelaQuizResult from '.';

import {
  DietProfileResponse,
  DietProfileRequest,
} from 'services/dietelaQuiz/models';
import {
  BodyMassConstants,
  VegetableAndFruitSufficiencyResponse,
  SugarSaltFatProblemResponse,
  LargeMealDietRecommendation,
  SnacksDietRecommendation,
  BreakfastReponse,
  PhysicalActivityResponse,
} from 'services/dietelaQuiz/quizResult';
import { defaultProgramRecommendations } from 'constants/dietelaProgram';

const validFormValues: DietProfileRequest = {
  name: 'Dietela',
  email: 'dietela@gmail.com',
  age: 20,
  weight: 60,
  height: 172,
  gender: 2,
  special_condition: 1,
  body_activity: 1,
  vegetables_in_one_day: 1,
  fruits_in_one_day: 1,
  fried_food_in_one_day: 1,
  sweet_snacks_in_one_day: 1,
  sweet_drinks_in_one_day: 1,
  packaged_food_in_one_day: 1,
  large_meal_in_one_day: 1,
  snacks_in_one_day: 1,
  breakfast_type: 1,
  current_condition: 1,
  problem_to_solve: 1,
  health_problem: [2, 3],
};

const mockQuizResult: DietProfileResponse = {
  id: 1,
  ...validFormValues,
  quiz_result: {
    diet_profile: 1,
    age: 20,
    weight: 60,
    height: 172,
    gender: 2,
    body_mass_index: 20,
    nutrition_status: BodyMassConstants.NORMAL,
    ideal_weight_range: {
      max: 68.0432,
      min: 56.209599999999995,
    },
    daily_energy_needs: 1696.8,
    daily_nutrition_needs: {
      fat_needs: 56.559999999999995,
      fiber_needs: 25,
      protein_needs: 55.146,
      carbohydrate_needs: 241.79399999999998,
    },
    vegetable_and_fruit_sufficiency:
      VegetableAndFruitSufficiencyResponse.LACKING,
    vegetable_and_fruit_diet_recommendation:
      'Yah.. asupan buah & sayur kamu masih kurang nih. Yuk, cari solusi agar kamu bisa konsumsi setidaknya 2 porsi sayur & 3 porsi buah  atau sebaliknya per hari. Pokoknya kalau di total kamu perlu 5 porsi buah & sayur per hari.',
    sugar_salt_fat_problem: SugarSaltFatProblemResponse.CONTROLLED,
    sugar_salt_fat_diet_recommendation:
      'Selamat! Kamu sudah memenuhi salah satu langkah untuk menuju hidup sehat. Pertahankan pemilihan jenis makanan yang seperti ini ya. Upayakan agar jumlah gorengan, cemilan manis, makan manis, makanan kemasan, dan makanan cepat saji yang kamu makan bisa terkontrol jumlahnya.',
    large_meal_diet_recommendation: LargeMealDietRecommendation.ONCE_A_DAY,
    snacks_diet_recommendation: SnacksDietRecommendation.NO_SNACK,
    breakfast_recommendation: BreakfastReponse.NO_BREAKFAST,
    energy_needed_per_dine: {
      lunch: 509.03999999999996,
      dinner: 509.03999999999996,
      breakfast: 169.68,
      morning_snack: 254.51999999999998,
      afternoon_snack: 254.51999999999998,
    },
    physical_activity_recommendation: PhysicalActivityResponse.LEVEL1_ACTIVITY,
    program_recommendation: defaultProgramRecommendations,
  },
};

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: jest.fn(),
    }),
    useRoute: () => ({
      params: mockQuizResult,
    }),
  };
});

describe('DietelaQuizResult', () => {
  it('renders correctly', () => {
    render(<DietelaQuizResult />);
  });
});
