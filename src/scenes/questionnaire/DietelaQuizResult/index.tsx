import React, { FC, useState } from 'react';
import { View, Dimensions } from 'react-native';
import Carousel from 'react-native-snap-carousel';

import { CarouselPagination } from 'components/core';
import { ResultPage, pages } from './pages';
import { styles } from './styles';
import { useRoute } from '@react-navigation/native';
import { DietProfileResponse } from 'services/dietelaQuiz/models';

const DietelaQuizResult: FC = () => {
  const [activeSlide, setActiveSlide] = useState(0);
  const route = useRoute();
  const resultData = route.params as DietProfileResponse;

  return (
    <View style={styles.view}>
      <Carousel
        data={pages.map((page, idx) => (
          <ResultPage key={idx} content={page(resultData)} cta={idx === 8} />
        ))}
        renderItem={({ item }: any) => item}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={Dimensions.get('window').width}
        onSnapToItem={setActiveSlide}
      />
      <CarouselPagination index={activeSlide} length={9} />
    </View>
  );
};

export default DietelaQuizResult;
