import React, { FC } from 'react';
import { ScrollView, View } from 'react-native';
import { Text, Button } from 'react-native-elements';
import Pdf from 'react-native-pdf';

import { InfoCard, EmptyDataPage } from 'components/core';
import { layoutStyles } from 'styles';
import { useDownloadFiles } from 'hooks';
import { DietRecommendationResponse } from 'services/dietRecommendation/models';
import { FileType } from 'hooks/useDownloadFiles/schema';
import { API_BASE_URL } from 'env';

import { styles } from './styles';
import { Props } from './types';

const ReadOnlyDietRecommendation: FC<Props> = ({ children, data }) => {
  const url = API_BASE_URL + data?.client_plan_meal;
  const { download, fileName } = useDownloadFiles(url, '', FileType.PDF);

  const hasValues = (obj: DietRecommendationResponse) =>
    [obj.client_plan_meal, obj.nutritional_advice, obj.lifestyle_advice].reduce(
      (acc, item) => acc || Boolean(item),
      false,
    );

  if (!data || !hasValues(data)) {
    return (
      <View style={styles.noRecom}>
        <EmptyDataPage text="Silahkan tunggu rekomendasi dari nutrisionis dalam 24 jam" />
        {children}
      </View>
    );
  }
  return (
    <ScrollView contentContainerStyle={layoutStyles}>
      <Text style={styles.header}>Saran Gizi</Text>
      <InfoCard content={data.nutritional_advice || '-'} />
      <Text style={[styles.header, styles.spacing]}>Saran Gaya Hidup</Text>
      <InfoCard content={data.lifestyle_advice || '-'} />
      <View style={styles.spacing}>
        <Text style={styles.header}>Rencana Menu dan Porsi Makan</Text>
      </View>
      {data?.client_plan_meal ? (
        <View>
          <Pdf source={{ uri: url, cache: true }} style={styles.pdfView} />
          <Button
            title={`Unduh ${fileName}`}
            type="outline"
            icon={{
              name: 'file-download',
            }}
            iconRight
            buttonStyle={styles.buttonStyle}
            titleStyle={styles.titleStyle}
            onPress={download}
          />
        </View>
      ) : (
        <InfoCard content="Belum ada nih, ditunggu yaa" />
      )}
      {children}
    </ScrollView>
  );
};

export default ReadOnlyDietRecommendation;
