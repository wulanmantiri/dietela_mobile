import { DietRecommendationResponse } from 'services/dietRecommendation/models';

export interface Props {
  data?: DietRecommendationResponse;
}
