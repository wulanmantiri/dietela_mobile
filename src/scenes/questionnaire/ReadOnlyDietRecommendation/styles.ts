import { StyleSheet, Dimensions } from 'react-native';
import { typography, colors, layoutStyles } from 'styles';

export const styles = StyleSheet.create({
  header: {
    ...typography.headingMedium,
    fontSize: 18,
    marginBottom: 4,
  },
  spacing: {
    marginTop: 20,
  },
  pdfView: {
    height: Dimensions.get('window').height * 0.75,
    marginBottom: 8,
  },
  buttonStyle: {
    backgroundColor: colors.primaryYellow,
  },
  titleStyle: {
    color: 'black',
    paddingRight: 6,
  },
  noRecom: {
    flex: 1,
    ...layoutStyles,
  },
});
