import React from 'react';
import { render } from '@testing-library/react-native';

import ReadOnlyDietRecommendation from '.';
import { mockDietRecommendation } from 'mocks/dietRecommendation';

describe('ReadOnlyDietRecommendation', () => {
  it('shows "Silahkan tunggu rekomendasi dari nutrisionis dalam 24 jam" text if no recommendation is provided yet', () => {
    const { getByText } = render(<ReadOnlyDietRecommendation />);
    expect(
      getByText(/Silahkan tunggu rekomendasi dari nutrisionis dalam 24 jam/i),
    ).toBeTruthy();
  });

  it('shows default no input text if recommendation is not fully provided yet', () => {
    const { getByText } = render(
      <ReadOnlyDietRecommendation
        data={{
          id: 1,
          nutritionist: 1,
          client: 1,
          client_plan_meal: null,
          nutritional_advice: '',
          lifestyle_advice: 'hai',
        }}
      />,
    );
    expect(getByText(/Belum ada nih, ditunggu yaa/i)).toBeTruthy();
  });

  it('shows default no input text if recommendation is not fully provided yet (2)', () => {
    const { getByText } = render(
      <ReadOnlyDietRecommendation
        data={{
          id: 1,
          nutritionist: 1,
          client: 1,
          client_plan_meal: null,
          nutritional_advice: 'bye',
          lifestyle_advice: '',
        }}
      />,
    );
    expect(getByText(/-/i)).toBeTruthy();
  });

  it('shows recommendation when provided', () => {
    const { getByText } = render(
      <ReadOnlyDietRecommendation data={mockDietRecommendation} />,
    );
    expect(getByText(/haiya/i)).toBeTruthy();
  });
});
