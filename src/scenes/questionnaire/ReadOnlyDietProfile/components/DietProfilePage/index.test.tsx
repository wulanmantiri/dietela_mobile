import React from 'react';
import { render } from 'utils/testing';
import * as ROUTES from 'constants/routes';
import { pages } from '../../pages';

import DietProfilePage from '.';
import { mockDietQuestionnaire } from 'mocks/dietQuestionnaire';

describe('DietProfilePage', () => {
  it('identity page renders correctly', () => {
    render(
      <DietProfilePage content={pages[0](mockDietQuestionnaire)} />,
      ROUTES.clientProfileNutritionist,
    );
  });
  it('eating pattern page renders correctly', () => {
    render(
      <DietProfilePage content={pages[1](mockDietQuestionnaire)} />,
      ROUTES.clientProfileNutritionist,
    );
  });
  it('daily food consumption page renders correctly', () => {
    render(
      <DietProfilePage content={pages[2](mockDietQuestionnaire)} />,
      ROUTES.clientProfileNutritionist,
    );
  });
  it('lifestyle page renders correctly', () => {
    render(
      <DietProfilePage content={pages[3](mockDietQuestionnaire)} />,
      ROUTES.clientProfileNutritionist,
    );
  });
  it('health condition page renders correctly', () => {
    render(
      <DietProfilePage content={pages[4](mockDietQuestionnaire)} />,
      ROUTES.clientProfileNutritionist,
    );
  });
});
