import { FC } from 'react';
import { Text, ScrollView } from 'react-native';

import { typography } from 'styles';
import { styles } from './styles';
import React from 'react';
import QuestionAnswerCard from '../QuestionAnswerCard';
import { DietProfilePageContent } from '../../pages/types';

const DietProfilePage: FC<{
  content: DietProfilePageContent;
}> = ({ content }) => {
  return (
    <ScrollView style={styles.container}>
      <Text style={[typography.headingLarge, styles.nameMargin]}>
        {content.title}
      </Text>
      {content.questions.map((qst, idx) => (
        <QuestionAnswerCard
          key={idx}
          question={qst.label}
          answer={qst.answer}
        />
      ))}
    </ScrollView>
  );
};

export default DietProfilePage;
