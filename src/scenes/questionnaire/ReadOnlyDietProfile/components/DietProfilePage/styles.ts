import { StyleSheet, Dimensions } from 'react-native';

export const styles = StyleSheet.create({
  nameMargin: {
    marginBottom: 15,
  },
  container: {
    width: Dimensions.get('window').width - 40,
  },
});
