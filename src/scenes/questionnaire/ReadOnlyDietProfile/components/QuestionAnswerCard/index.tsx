import { FC } from 'react';
import { View, Text } from 'react-native';

import React from 'react';
import { InfoCard } from 'components/core';
import { typographyStyles } from 'styles';
import { styles } from './styles';

interface Props {
  question: string;
  answer: string;
  testID?: string;
}

const QuestionAnswerCard: FC<Props> = ({ question, answer, testID }) => {
  return (
    <View style={styles.labelContainer} testID={testID}>
      <Text style={[typographyStyles.bodyLarge, styles.label]}>{question}</Text>
      <InfoCard content={answer} />
    </View>
  );
};

export default QuestionAnswerCard;
