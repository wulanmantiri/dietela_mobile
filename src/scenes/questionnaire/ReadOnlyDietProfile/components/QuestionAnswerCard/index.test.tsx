import React from 'react';
import { render } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import QuestionAnswerCard from '.';

jest.useFakeTimers();

describe('QuestionAnswerCard', () => {
  it('renders correctly', () => {
    render(
      <QuestionAnswerCard question="apa?" answer="yes" />,
      ROUTES.clientProfileNutritionist,
    );
  });
  it('renders correctly with testID', () => {
    render(
      <QuestionAnswerCard
        testID="question answer"
        question="apa?"
        answer="yes"
      />,
      ROUTES.clientProfileNutritionist,
    );
  });
});
