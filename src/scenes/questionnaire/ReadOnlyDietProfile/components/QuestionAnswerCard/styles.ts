import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  label: {
    marginBottom: 5,
    marginTop: 15,
  },
  labelContainer: {
    alignSelf: 'stretch',
  },
  answer: {
    flexDirection: 'row',
  },
});
