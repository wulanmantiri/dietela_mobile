export interface DietProfilePageContent {
  title: string;
  questions: {
    label: string;
    answer: string;
  }[];
}

export const answerTypes: { SELECT: string; TEXT: string } = {
  SELECT: 'select',
  TEXT: 'text',
};
