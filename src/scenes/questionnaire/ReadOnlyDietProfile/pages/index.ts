import { identity } from './Identity';
import { eatingPattern } from './EatingPattern';
import { dailyFoodConsumption } from './DailyFoodConsumption';
import { lifestyle } from './Lifestyle';
import { healthCondition } from './HealthCondition';

export const pages = [
  identity,
  eatingPattern,
  dailyFoodConsumption,
  lifestyle,
  healthCondition,
];
