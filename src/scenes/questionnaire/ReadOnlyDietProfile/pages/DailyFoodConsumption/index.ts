import { DietProfilePageContent, answerTypes } from '../types';
import { getQuestionAnswerString } from '../utils';
import { DietQuestionnaireResponse } from 'services/dietQuestionnaire/models';
import { pageHeaders } from 'constants/questionnaire';

export const dailyFoodConsumption = ({
  breakfast_meal_explanation,
  morning_snack_explanation,
  lunch_meal_explanation,
  evening_snack_explanation,
  dinner_meal_explanation,
  night_snack_explanation,
}: DietQuestionnaireResponse): DietProfilePageContent => {
  const page = 'foodConsumption';
  return {
    title: pageHeaders[3],
    questions: [
      getQuestionAnswerString(
        page,
        'breakfast_meal_explanation',
        breakfast_meal_explanation,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'morning_snack_explanation',
        morning_snack_explanation,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'lunch_meal_explanation',
        lunch_meal_explanation,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'evening_snack_explanation',
        evening_snack_explanation,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'dinner_meal_explanation',
        dinner_meal_explanation,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'night_snack_explanation',
        night_snack_explanation,
        answerTypes.TEXT,
      ),
    ],
  };
};
