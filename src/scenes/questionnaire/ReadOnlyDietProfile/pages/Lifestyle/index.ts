import { DietProfilePageContent, answerTypes } from '../types';
import { getQuestionAnswerString } from '../utils';
import { DietQuestionnaireResponse } from 'services/dietQuestionnaire/models';
import { pageHeaders } from 'constants/questionnaire';

export const lifestyle = ({
  meal_provider,
  cigarette_alcohol_condition,
  physical_activity,
  food_alergies,
  diet_drinks,
  multivitamin_tablet_suplement,
  diet_and_life_style_story,
}: DietQuestionnaireResponse): DietProfilePageContent => {
  const page = 'lifestyle';
  return {
    title: pageHeaders[4],
    questions: [
      getQuestionAnswerString(
        page,
        'meal_provider',
        meal_provider,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'cigarette_alcohol_condition',
        cigarette_alcohol_condition,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'physical_activity',
        physical_activity,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'food_alergies',
        food_alergies,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'diet_drinks',
        diet_drinks,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'multivitamin_tablet_suplement',
        multivitamin_tablet_suplement,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'diet_and_life_style_story',
        diet_and_life_style_story,
        answerTypes.TEXT,
      ),
    ],
  };
};
