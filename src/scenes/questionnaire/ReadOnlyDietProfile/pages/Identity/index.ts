import { DietProfilePageContent, answerTypes } from '../types';
import { getQuestionAnswerString } from '../utils';
import { DietQuestionnaireResponse } from 'services/dietQuestionnaire/models';
import { convertDate } from 'utils/format';
import { pageHeaders } from 'constants/questionnaire';

export const identity = ({
  user,
  date_of_birth,
  city_and_area_of_residence,
  handphone_no,
  whatsapp_no,
  waist_size,
  profession,
  last_education,
  meal_preference,
  general_purpose,
  dietary_change,
  has_weigher,
}: DietQuestionnaireResponse): DietProfilePageContent => {
  const page = 'identity';
  return {
    title: pageHeaders[1],
    questions: [
      {
        label: 'Nama',
        answer: user.name,
      },
      getQuestionAnswerString(
        page,
        'date_of_birth',
        convertDate(date_of_birth as string),
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'city_and_area_of_residence',
        city_and_area_of_residence,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'handphone_no',
        handphone_no,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'whatsapp_no',
        whatsapp_no,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(page, 'waist_size', waist_size, answerTypes.TEXT),
      getQuestionAnswerString(
        page,
        'profession',
        profession,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'last_education',
        last_education,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'meal_preference',
        meal_preference,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'general_purpose',
        general_purpose,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'dietary_change',
        dietary_change,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'has_weigher',
        has_weigher,
        answerTypes.SELECT,
      ),
    ],
  };
};
