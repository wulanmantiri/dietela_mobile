import { DietProfilePageContent, answerTypes } from '../types';
import { getQuestionAnswerString } from '../utils';
import { DietQuestionnaireResponse } from 'services/dietQuestionnaire/models';
import { pageHeaders } from 'constants/questionnaire';

export const eatingPattern = ({
  breakfast_frequency,
  breakfast_meal_type,
  sweet_tea_consumption_frequency,
  coffee_consumption_frequency,
  milk_consumption_frequency,
  other_drink_consumption_frequency,
  additional_sugar_in_a_day,
  liquid_consumption_frequency,
  meal_consumed_almost_every_day,
  unliked_food,
  preferred_food_taste,
  expected_food_on_breakfast,
  expected_food_on_lunch_dinner,
}: DietQuestionnaireResponse): DietProfilePageContent => {
  const page = 'eatingPattern';
  return {
    title: pageHeaders[2],
    questions: [
      getQuestionAnswerString(
        page,
        'breakfast_frequency',
        breakfast_frequency,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'breakfast_meal_type',
        breakfast_meal_type,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'sweet_tea_consumption_frequency',
        sweet_tea_consumption_frequency,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'coffee_consumption_frequency',
        coffee_consumption_frequency,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'milk_consumption_frequency',
        milk_consumption_frequency,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'other_drink_consumption_frequency',
        other_drink_consumption_frequency,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'additional_sugar_in_a_day',
        additional_sugar_in_a_day,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'liquid_consumption_frequency',
        liquid_consumption_frequency,
        answerTypes.SELECT,
      ),
      getQuestionAnswerString(
        page,
        'meal_consumed_almost_every_day',
        meal_consumed_almost_every_day,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'unliked_food',
        unliked_food,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'preferred_food_taste',
        preferred_food_taste,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'expected_food_on_breakfast',
        expected_food_on_breakfast,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'expected_food_on_lunch_dinner',
        expected_food_on_lunch_dinner,
        answerTypes.TEXT,
      ),
    ],
  };
};
