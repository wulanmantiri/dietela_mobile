import { selectFields, textFields, dateField } from 'constants/questionnaire';
import { answerTypes } from './types';

export const getQuestionAnswerString = (
  page: string,
  question: string,
  answer: any,
  answerType: string,
): { label: string; answer: string } => {
  if (question === dateField.name) {
    return {
      label: dateField.label,
      answer: answer,
    };
  }
  const combinedFields =
    page === 'foodConsumption'
      ? textFields[page]
      : selectFields[page].concat(textFields[page]);

  let finalAnswer = answer;
  if (answerType === answerTypes.SELECT) {
    finalAnswer = getSelectedAnswerString(
      page,
      combinedFields,
      question,
      answer,
    );
  }
  if (finalAnswer === '') {
    finalAnswer = '-';
  }

  return {
    label: combinedFields.find((qst) => qst.name === question)!.label,
    answer: finalAnswer,
  };
};

export const getSelectedAnswerString = (
  page: string,
  selectOptionFields: any[],
  question: string,
  choice: any,
) => {
  if (Array.isArray(choice)) {
    return getAnswerListString(page, selectOptionFields, question, choice);
  }
  return getSelectFieldsChoice(page, selectOptionFields, question, choice);
};

const getSelectFieldsChoice = (
  page: string,
  selectOptionFields: any[],
  question: string,
  choice: any,
) =>
  selectOptionFields.find((qst) => qst.name === question)?.choiceList[
    choice - 1
  ];

const getAnswerListString = (
  page: string,
  selectOptionFields: any[],
  question: string,
  answers: any[],
) => {
  let answer = '';
  if (answers.length > 0) {
    if (typeof answers[0] === 'number') {
      answers.map(
        (ans) =>
          (answer += `- ${getSelectFieldsChoice(
            page,
            selectOptionFields,
            question,
            ans,
          )}\n`),
      );
    } else {
      answers.map((ans) => (answer += `- ${ans}\n`));
    }
    answer = answer.slice(0, answer.length - 1);
  } else {
    answer = '-';
  }
  return answer;
};
