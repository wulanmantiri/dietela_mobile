import { DietProfilePageContent, answerTypes } from '../types';
import { getQuestionAnswerString } from '../utils';
import { DietQuestionnaireResponse } from 'services/dietQuestionnaire/models';
import { pageHeaders } from 'constants/questionnaire';

export const healthCondition = ({
  disease,
  complaint,
  regular_drug_consumption,
  other_disease,
  motivation_using_dietela,
  dietela_nutritionist_expectation,
  dietela_program_expectation,
}: DietQuestionnaireResponse): DietProfilePageContent => {
  const page = 'healthCondition';
  return {
    title: pageHeaders[5],
    questions: [
      getQuestionAnswerString(page, 'disease', disease, answerTypes.SELECT),
      getQuestionAnswerString(page, 'complaint', complaint, answerTypes.SELECT),
      getQuestionAnswerString(
        page,
        'regular_drug_consumption',
        regular_drug_consumption,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'other_disease',
        other_disease,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'motivation_using_dietela',
        motivation_using_dietela,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'dietela_nutritionist_expectation',
        dietela_nutritionist_expectation,
        answerTypes.TEXT,
      ),
      getQuestionAnswerString(
        page,
        'dietela_program_expectation',
        dietela_program_expectation,
        answerTypes.TEXT,
      ),
    ],
  };
};
