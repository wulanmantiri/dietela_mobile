import { FC, useState } from 'react';
import { Dimensions, ScrollView } from 'react-native';
import { styles } from './styles';
import { layoutStyles } from 'styles';
import { useNavigation, useRoute } from '@react-navigation/native';
import React from 'react';
import { DietProfilePage } from './components';
import {
  CarouselPagination,
  BigButton,
  Loader,
  EmptyDataPage,
} from 'components/core';
import Carousel from 'react-native-snap-carousel';
import { pages } from './pages';
import { useApi } from 'hooks';
import { retrieveDietQuestionnaireByIdApi } from 'services/dietQuestionnaire';
import { DietQuestionnaireResponse } from 'services/dietQuestionnaire/models';
import * as ROUTES from 'constants/routes';
import { UserRole } from 'services/auth/models';

interface ParamsDietProfile {
  id: number;
  id_recommendation: number;
  role: string;
}

const ReadOnlyDietProfile: FC = () => {
  const navigation = useNavigation();
  const [activeSlide, setActiveSlide] = useState(0);
  const route = useRoute();
  const { id, id_recommendation, role } = route.params as ParamsDietProfile;
  const { isLoading, data } = useApi(() =>
    retrieveDietQuestionnaireByIdApi(id),
  );

  const dataDQR = data as DietQuestionnaireResponse;

  if (!id) {
    return <EmptyDataPage text="Klien belum mengisi diet questionnaire" />;
  }

  if (isLoading) {
    return <Loader />;
  }

  return (
    <ScrollView contentContainerStyle={[layoutStyles, styles.container]}>
      <Carousel
        data={pages.map((page, idx) => (
          <DietProfilePage
            key={idx}
            content={page(data as DietQuestionnaireResponse)}
          />
        ))}
        renderItem={({ item }: any) => item}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={Dimensions.get('window').width}
        onSnapToItem={setActiveSlide}
      />
      <CarouselPagination index={activeSlide} length={5} />
      {role === UserRole.NUTRITIONIST ? (
        <BigButton
          title="Berikan Rekomendasi"
          onPress={() =>
            navigation.navigate(ROUTES.profileDietRecommendation, {
              name: dataDQR.user.name,
              id: id_recommendation,
            })
          }
        />
      ) : (
        <BigButton
          title="Lihat Rekomendasi"
          onPress={() =>
            navigation.navigate(ROUTES.clientDietRecommendation, {
              name: dataDQR.user.name,
              id: id_recommendation,
            })
          }
        />
      )}
    </ScrollView>
  );
};

export default ReadOnlyDietProfile;
