import React from 'react';
import { render } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import ReadOnlyDietProfile from '.';
import { UserRole } from 'services/auth/models';

jest.useFakeTimers();

describe('ReadOnlyDietProfile', () => {
  it('renders correctly when role is nutritionist', () => {
    render(<ReadOnlyDietProfile />, ROUTES.clientProfileNutritionist, {
      routeParams: { id: 1, role: UserRole.NUTRITIONIST },
    });
  });

  it('renders correctly when role is admin', () => {
    render(<ReadOnlyDietProfile />, ROUTES.clientProfileNutritionist, {
      routeParams: { id: 1, role: UserRole.ADMIN },
    });
  });

  it('shows "Klien belum mengisi diet questionnaire" text if no questionnaire answer is provided yet', () => {
    const { getByText } = render(
      <ReadOnlyDietProfile />,
      ROUTES.clientProfileNutritionist,
      {
        routeParams: { id: null, role: UserRole.NUTRITIONIST },
      },
    );
    expect(getByText(/Klien belum mengisi diet questionnaire/i)).toBeTruthy();
  });
});
