import { DietRecommendationRequest } from 'services/dietRecommendation/models';
import { TextFieldSchema } from 'types/form';
import { FieldType, FieldValidation } from 'utils/form';

export const textField: TextFieldSchema[] = [
  {
    label: 'Saran Gizi',
    placeholder: 'Ex: Perbanyak makanan yang berserat...',
    required: true,
    name: 'nutritional_advice',
  },
  {
    label: 'Saran Gaya Hidup',
    placeholder: 'Ex: Tubuh Anda membutuhkan istirahat lebih...',
    required: true,
    name: 'lifestyle_advice',
  },
];

export const initialValues: DietRecommendationRequest = {
  client_plan_meal: '',
  nutritional_advice: '',
  lifestyle_advice: '',
};

export const fieldValidation: FieldValidation[] = [
  {
    name: 'nutritional_advice',
    required: true,
    label: 'Saran Gizi',
    type: FieldType.TEXT,
  },
  {
    name: 'lifestyle_advice',
    required: true,
    label: 'Saran Gaya Hidup',
    type: FieldType.TEXT,
  },
];
