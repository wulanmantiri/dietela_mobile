import React, { FC, useState } from 'react';
import { Dimensions, ScrollView, StyleSheet, Text } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import DocumentPicker, {
  DocumentPickerResponse,
} from 'react-native-document-picker';

import { fieldValidation, initialValues, textField } from './schema';
import { Section } from 'components/layout';
import { BodyMedium, HeadingLarge } from 'components/typography';

import Pdf from 'react-native-pdf';
import { BigButton, Loader, Toast } from 'components/core';
import { layoutStyles } from 'styles';
import { useApi, useForm } from 'hooks';
import { generateValidationSchema } from 'utils/form';
import { TextField } from 'components/form';
import { retrieveClientListApi } from 'services/profiles';
import { Client } from 'services/profiles/models';
import { submitDietRecommendationByIdApi } from 'services/dietRecommendation';
import * as ROUTES from 'constants/routes';

interface Params {
  name: string;
  id: number;
}

const ProfileDietRecommendation: FC = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const [fileError, setFileError] = useState(false);
  const [file, setFile] = useState<DocumentPickerResponse>();
  const { name: userName, id } = route.params as Params;
  const { isLoading, data: clients = [] } = useApi(retrieveClientListApi);

  const {
    getTextInputProps,
    setFieldError,
    handleSubmit,
    isSubmitting,
  } = useForm({
    initialValues,
    validationSchema: generateValidationSchema(fieldValidation),
    onSubmit: async (values) => {
      const client = getClient();
      setFileError(!file);

      const payload = new FormData();
      payload.append('client_plan_meal', {
        uri: file?.uri,
        type: file?.type,
        name: file?.name,
      });
      payload.append('nutritional_advice', values.nutritional_advice);
      payload.append('lifestyle_advice', values.lifestyle_advice);

      const response = await submitDietRecommendationByIdApi(
        client.diet_recommendation_id,
        payload,
      );

      if (!response.success) {
        const error = response.error;

        setFieldError('nutritional_advice', error.nutritional_advice);
        setFieldError('lifestyle_advice', error.lifestyle_advice);

        Toast.show({
          type: 'error',
          text1: 'Gagal mengirim rekomendasi',
          text2: 'Terjadi kesalahan pada sisi kami. Silakan coba lagi',
        });
      } else {
        Toast.show({
          type: 'success',
          text1: 'Rekomendasi berhasil dikirim',
          text2: `File rencana menu serta saran gizi & gaya hidup berhasil dikirim ke client ${userName}.`,
        });
        navigation.navigate(ROUTES.clientProfileNutritionist, {
          id: client.diet_questionnaire_id,
        });
      }
    },
  });

  const handlePickFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });
      setFile(res);
      setFileError(false);
    } catch (err) {
      if (!DocumentPicker.isCancel(err)) {
        throw err;
      }
    }
  };

  const submitHandler = () => {
    setFileError(!file);
    handleSubmit();
  };

  const getClient = (): Client => {
    const client = clients.filter((c) => c.diet_questionnaire_id === id)[0];
    return client;
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <ScrollView style={layoutStyles}>
      <HeadingLarge text={`Rekomendasi untuk ${userName}`} />

      <Section>
        <BodyMedium text="Rencana porsi makan dan menu *" />
      </Section>

      <Section>
        {file && (
          <Pdf source={{ uri: file.uri, cache: true }} style={styles.pdfView} />
        )}
        <BigButton title="Pilih PDF" onPress={handlePickFile} />
        {fileError && (
          <Text style={styles.error}>
            Rencana porsi makan dan menu perlu diisi
          </Text>
        )}
        <Section />
      </Section>

      {textField.map(({ name, label, required, placeholder }, i) => (
        <TextField
          key={`field${i}`}
          label={label}
          required={required}
          placeholder={placeholder}
          {...getTextInputProps(name)}
          numberOfLines={6}
          multiline={true}
        />
      ))}
      <BigButton
        title="Submit"
        onPress={submitHandler}
        loading={isSubmitting}
        testID="submitButton"
      />
      <Section />
      <Section />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  pdfView: {
    height: Dimensions.get('window').height * 0.6,
    marginBottom: 10,
  },
  error: { color: 'red' },
});

export default ProfileDietRecommendation;
