import React from 'react';
import { render, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import ProfileDietRecommendation from '.';

jest.mock('react-native-toast-message');
jest.mock('axios');
jest.mock('react-native-document-picker');

describe('ProfileDietRecommendation', () => {
  it('renders correctly', () => {
    render(<ProfileDietRecommendation />, ROUTES.profileDietRecommendation, {
      routeParams: {
        name: 'Doan',
        id: 1,
      },
    });
  });

  it('shows correct name', async () => {
    const { queryByText } = render(
      <ProfileDietRecommendation />,
      ROUTES.profileDietRecommendation,
      {
        routeParams: {
          name: 'Doan',
          id: 1,
        },
      },
    );

    await waitFor(() => expect(queryByText(/Rekomendasi/i)).toBeTruthy());
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
});
