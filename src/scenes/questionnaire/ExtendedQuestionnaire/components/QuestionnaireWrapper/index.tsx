import React, { FC, useContext } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { BigButton, Toast } from 'components/core';
import * as ROUTES from 'constants/routes';
import { useForm } from 'hooks';
import { layoutStyles } from 'styles';

import { generateValidationSchema } from 'utils/form';
import { Props } from './types';
import { updateDietQuestionnaireApi } from 'services/dietQuestionnaire';
import { UserContext } from 'provider';

const QuestionnaireWrapper: FC<Props> = ({
  Questionnaire,
  initialValues,
  fieldValidations = [],
  convertPayload,
  route,
}) => {
  const navigation = useNavigation();
  const dietQuestionnaireResponse = route.params;
  const { user, setUser } = useContext(UserContext);

  const getInitialValues = () => {
    let defaultValues: typeof initialValues = {};
    Object.entries(initialValues).forEach(([k, value]) => {
      const key = k as keyof typeof initialValues;
      defaultValues[key] = dietQuestionnaireResponse[key] || value;
    });
    return defaultValues;
  };

  const {
    getTextInputProps,
    getFormFieldProps,
    handleSubmit,
    isSubmitting,
    values: formValues,
  } = useForm({
    initialValues: getInitialValues(),
    validationSchema: generateValidationSchema(fieldValidations),
    onSubmit: async (values) => {
      const payload = convertPayload ? convertPayload(values) : values;
      const response = await updateDietQuestionnaireApi(
        dietQuestionnaireResponse.id,
        payload,
      );
      if (response.success && response.data) {
        if (response.data.finished_steps.length === 6) {
          setUser({
            ...user,
            is_finished_onboarding: true,
          });
        }
        navigation.reset({
          index: 0,
          routes: [{ name: ROUTES.extendedQuestionnaire }],
        });
      } else {
        Toast.show({
          type: 'error',
          text1: 'Gagal menyimpan data',
          text2: 'Terjadi kesalahan pada sisi kami. Silakan coba lagi',
        });
      }
    },
  });

  const isValid =
    formValues.step === 1
      ? formValues.agree_to_all_statements_consent === 1 &&
        formValues.personal_data_consent === 1
      : true;

  return (
    <ScrollView contentContainerStyle={layoutStyles}>
      <Questionnaire
        getTextInputProps={getTextInputProps}
        getFormFieldProps={getFormFieldProps}
      />
      <BigButton
        title="simpan"
        onPress={handleSubmit}
        loading={isSubmitting}
        disabled={!isValid}
      />
    </ScrollView>
  );
};

export const styles = StyleSheet.create({
  spacing: {
    marginBottom: 24,
  },
});

export default QuestionnaireWrapper;
