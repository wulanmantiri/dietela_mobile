import { FC } from 'react';
import { FieldValidation } from 'utils/form';
import {
  DietQuestionnaireRequest,
  DietQuestionnaireResponse,
} from 'services/dietQuestionnaire/models';

export interface QuestionnaireProps {
  getTextInputProps: (_: string) => any;
  getFormFieldProps: (_: string) => any;
}

export interface NavProps {
  route: {
    params: DietQuestionnaireResponse;
  };
}

export interface Props extends NavProps {
  Questionnaire: FC<QuestionnaireProps>;
  initialValues: { [key in keyof DietQuestionnaireRequest]: any };
  fieldValidations?: FieldValidation[];
  convertPayload?: (_: any) => DietQuestionnaireRequest;
}
