export const answerFormat = `Nasi goreng 1 centong
Ayam goreng 1 potong
Tempe orek kurang lebih 2 sendok makan
Sambal goreng kurang lebih 1 sendok makan
Roti isi coklat 1 potong
Jus alpukat 1 gelas`;

export const initialValues = {
  breakfast_meal_explanation: '',
  morning_snack_explanation: '',
  lunch_meal_explanation: '',
  evening_snack_explanation: '',
  dinner_meal_explanation: '',
  night_snack_explanation: '',
  step: 4,
};
