import React, { FC } from 'react';
import { Text } from 'react-native-elements';
import { View } from 'react-native';

import { InfoCard } from 'components/core';
import { typographyStyles } from 'styles';
import { TextField } from 'components/form';

import { initialValues, answerFormat } from './schema';
import { textFields } from 'constants/questionnaire';
import QuestionnaireWrapper, { styles } from '../QuestionnaireWrapper';
import { NavProps } from '../QuestionnaireWrapper/types';

const Questionnaire3: FC<NavProps> = (navProps) => (
  <QuestionnaireWrapper
    {...navProps}
    Questionnaire={({ getTextInputProps }) => (
      <View>
        <Text style={[typographyStyles.headingMedium, styles.spacing]}>
          Tuliskan makanan dan minuman (selain air mineral) yang Anda konsumsi
          kemarin sesuai jam yang ditanyakan.
        </Text>
        <Text style={[typographyStyles.bodyMedium, styles.spacing]}>
          Sertakan jenis makanan, pengolahan makanan (digoreng, disemur,
          ditumis, direbus), banyaknya jumlah yang dikonsumsi baik dengan
          estimasi menggunakan takaran alat makan (1 centong, 1 gelas) atau
          dengan estimasi jumlah persis (misalnya: 200 g, 500 ml, 3 buah).
        </Text>
        <Text style={[typographyStyles.overlineBig]}>
          Contoh format jawaban:
        </Text>
        <InfoCard content={answerFormat} />
        <View style={styles.spacing} />
        {textFields.foodConsumption.map((props, i) => (
          <TextField
            {...props}
            {...getTextInputProps(props.name)}
            key={`textfield${i}`}
            multiline
          />
        ))}
      </View>
    )}
    initialValues={initialValues}
  />
);

export default Questionnaire3;
