import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import axios from 'axios';

import Questionnaire3 from '.';
import { mockDietQuestionnaire } from 'mocks/dietQuestionnaire';
import { mockUserContext } from 'mocks/userContext';
import { UserContext } from 'provider';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      reset: mockedNavigate,
    }),
  };
});

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('Questionnaire3', () => {
  const userProviderValues = {
    ...mockUserContext,
  };

  it('renders correctly', () => {
    render(
      <UserContext.Provider value={userProviderValues}>
        <Questionnaire3 route={{ params: mockDietQuestionnaire }} />
      </UserContext.Provider>,
    );
  });

  it('does not redirect to extended questionnaire if submit fails', async () => {
    const updateDietQuestionnaireApi = () =>
      Promise.resolve({
        status: 400,
        response: {
          data: 'error',
        },
      });
    mockAxios.request.mockImplementationOnce(updateDietQuestionnaireApi);

    const { getAllByPlaceholderText, getByText } = render(
      <UserContext.Provider value={userProviderValues}>
        <Questionnaire3 route={{ params: mockDietQuestionnaire }} />
      </UserContext.Provider>,
    );

    const formFields = getAllByPlaceholderText('Sesuai format jawaban');
    formFields.forEach((formField) => {
      fireEvent.changeText(formField, 'tempe');
    });

    const submitButton = getByText(/Simpan/i);
    await waitFor(() => fireEvent.press(submitButton));

    expect(mockedNavigate).not.toHaveBeenCalled();
  });

  it('redirects to extended questionnaire if all form values are valid and submit success', async () => {
    const updateDietQuestionnaireApi = () =>
      Promise.resolve({
        status: 200,
        data: mockDietQuestionnaire,
      });
    mockAxios.request.mockImplementationOnce(updateDietQuestionnaireApi);

    const { getAllByPlaceholderText, getByText } = render(
      <UserContext.Provider value={userProviderValues}>
        <Questionnaire3 route={{ params: mockDietQuestionnaire }} />
      </UserContext.Provider>,
    );

    const formFields = getAllByPlaceholderText('Sesuai format jawaban');
    formFields.forEach((formField) => {
      fireEvent.changeText(formField, 'tempe');
    });

    const submitButton = getByText(/Simpan/i);
    await waitFor(() => fireEvent.press(submitButton));

    expect(mockedNavigate).toHaveBeenCalled();
  });
});
