import { filterArr } from 'utils/form';

export const initialValues = {
  disease: [],
  complaint: [],
  regular_drug_consumption: '',
  other_disease: '',
  motivation_using_dietela: '',
  dietela_nutritionist_expectation: '',
  dietela_program_expectation: '',
  step: 6,
};

export const convertPayload = (values: typeof initialValues) => ({
  ...values,
  disease: values.disease.length === 0 ? [1] : filterArr(values.disease, 1),
  complaint:
    values.complaint.length === 0 ? [1] : filterArr(values.complaint, 1),
});
