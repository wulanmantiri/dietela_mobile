import React from 'react';
import { render } from '@testing-library/react-native';

import Questionnaire5 from '.';
import { mockDietQuestionnaire } from 'mocks/dietQuestionnaire';
import { mockUserContext } from 'mocks/userContext';
import { UserContext } from 'provider';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      reset: mockedNavigate,
    }),
  };
});

describe('Questionnaire5', () => {
  it('renders correctly', () => {
    const userProviderValues = {
      ...mockUserContext,
    };

    render(
      <UserContext.Provider value={userProviderValues}>
        <Questionnaire5 route={{ params: mockDietQuestionnaire }} />
      </UserContext.Provider>,
    );
  });
});
