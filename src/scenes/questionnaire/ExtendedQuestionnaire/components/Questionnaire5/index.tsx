import React, { FC } from 'react';
import { View } from 'react-native';

import { TextField, CheckboxGroup } from 'components/form';
import { textFields, selectFields } from 'constants/questionnaire';

import { initialValues, convertPayload } from './schema';
import QuestionnaireWrapper, { styles } from '../QuestionnaireWrapper';
import { NavProps } from '../QuestionnaireWrapper/types';

const Questionnaire5: FC<NavProps> = (navProps) => (
  <QuestionnaireWrapper
    {...navProps}
    Questionnaire={({ getTextInputProps, getFormFieldProps }) => (
      <View>
        {selectFields.healthCondition.map((props, i) => (
          <View key={`checkbox${i}`} style={styles.spacing}>
            <CheckboxGroup
              {...props}
              choices={props.choiceList.map((label, id) => ({
                label,
                value: id + 2,
              }))}
              {...getFormFieldProps(props.name)}
            />
          </View>
        ))}
        {textFields.healthCondition.map((props, i) => (
          <TextField
            {...props}
            {...getTextInputProps(props.name)}
            multiline
            key={`textfield${i}`}
          />
        ))}
      </View>
    )}
    initialValues={initialValues}
    convertPayload={convertPayload}
  />
);

export default Questionnaire5;
