import { FieldValidation, FieldType } from 'utils/form';
import { textFields, selectFields } from 'constants/questionnaire';
import { dateToString } from 'utils/format';

export const initialValues = {
  date_of_birth: new Date(),
  city_and_area_of_residence: '',
  handphone_no: '',
  whatsapp_no: '',
  waist_size: '',
  profession: 0,
  last_education: 0,
  meal_preference: 0,
  general_purpose: 0,
  dietary_change: 0,
  has_weigher: 0,
  step: 2,
};

export const fieldValidations: FieldValidation[] = [
  ...textFields.identity.map((field) => ({
    name: field.name,
    required: field.required,
    label: field.label,
    type: field.fieldType || FieldType.TEXT,
    max: field.max,
  })),
  ...selectFields.identity.map((field) => ({
    name: field.name,
    required: true,
    label: field.label,
    type: FieldType.RADIO_BUTTON,
  })),
];

export const convertPayload = (values: typeof initialValues) => ({
  ...values,
  date_of_birth: dateToString(values.date_of_birth),
  waist_size: parseInt(values.waist_size, 10),
});
