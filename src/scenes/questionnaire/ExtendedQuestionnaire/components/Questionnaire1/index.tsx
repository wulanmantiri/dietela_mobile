import React, { FC } from 'react';
import { View } from 'react-native';
import {
  RadioButtonGroup,
  TextField,
  Picker,
  Datepicker,
} from 'components/form';

import { initialValues, fieldValidations, convertPayload } from './schema';
import { textFields, selectFields, dateField } from 'constants/questionnaire';
import QuestionnaireWrapper, { styles } from '../QuestionnaireWrapper';
import { NavProps } from '../QuestionnaireWrapper/types';

const Questionnaire1: FC<NavProps> = (navProps) => (
  <QuestionnaireWrapper
    {...navProps}
    Questionnaire={({ getTextInputProps, getFormFieldProps }) => (
      <View>
        <Datepicker
          {...getFormFieldProps(dateField.name)}
          label={dateField.label}
          required
        />
        {textFields.identity.map((props, i) => (
          <TextField
            {...props}
            {...getTextInputProps(props.name)}
            required={props.required}
            key={`textfield${i}`}
          />
        ))}
        {selectFields.identity.map((props, i) => {
          const FormField = props.picker ? Picker : RadioButtonGroup;
          return (
            <View key={`select${i}`} style={styles.spacing}>
              <FormField
                {...props}
                choices={props.choiceList.map((label, id) => ({
                  label,
                  value: id + 1,
                }))}
                {...getFormFieldProps(props.name)}
                required
              />
            </View>
          );
        })}
      </View>
    )}
    initialValues={initialValues}
    fieldValidations={fieldValidations}
    convertPayload={convertPayload}
  />
);

export default Questionnaire1;
