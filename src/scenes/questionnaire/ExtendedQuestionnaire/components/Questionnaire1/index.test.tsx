import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import axios from 'axios';

import Questionnaire1 from '.';
import { mockDietQuestionnaire } from 'mocks/dietQuestionnaire';
import { textFields } from 'constants/questionnaire';
import { mockUserContext } from 'mocks/userContext';
import { UserContext } from 'provider';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      reset: mockedNavigate,
    }),
  };
});

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('Questionnaire1', () => {
  const validFormValues: { [_: string]: string } = {
    city_and_area_of_residence: 'Jakarta',
    handphone_no: '08123567',
    whatsapp_no: '09',
    waist_size: '765',
  };

  const userProviderValues = {
    ...mockUserContext,
  };

  it('does not redirect to extended questionnaire if form values are invalid', async () => {
    const updateDietQuestionnaireApi = () =>
      Promise.resolve({
        status: 201,
        data: mockDietQuestionnaire,
      });
    mockAxios.request.mockImplementationOnce(updateDietQuestionnaireApi);

    const { getByPlaceholderText, getByText } = render(
      <UserContext.Provider value={userProviderValues}>
        <Questionnaire1 route={{ params: mockDietQuestionnaire }} />
      </UserContext.Provider>,
    );

    textFields.identity.forEach(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validFormValues[name]);
    });

    const submitButton = getByText(/Simpan/i);
    await waitFor(() => fireEvent.press(submitButton));
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
});
