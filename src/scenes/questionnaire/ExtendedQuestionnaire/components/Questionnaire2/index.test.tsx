import React from 'react';
import { render } from '@testing-library/react-native';

import Questionnaire2 from '.';
import { mockDietQuestionnaire } from 'mocks/dietQuestionnaire';
import { mockUserContext } from 'mocks/userContext';
import { UserContext } from 'provider';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      reset: mockedNavigate,
    }),
  };
});

describe('Questionnaire2', () => {
  const userProviderValues = {
    ...mockUserContext,
  };

  it('renders correctly', () => {
    render(
      <UserContext.Provider value={userProviderValues}>
        <Questionnaire2 route={{ params: mockDietQuestionnaire }} />
      </UserContext.Provider>,
    );
  });
});
