import { FieldValidation, FieldType } from 'utils/form';
import { textFields, selectFields } from 'constants/questionnaire';

export const initialValues = {
  breakfast_frequency: 0,
  breakfast_meal_type: 0,
  sweet_tea_consumption_frequency: 0,
  coffee_consumption_frequency: 0,
  milk_consumption_frequency: 0,
  other_drink_consumption_frequency: 0,
  additional_sugar_in_a_day: 0,
  liquid_consumption_frequency: 0,
  meal_consumed_almost_every_day: '',
  unliked_food: '',
  preferred_food_taste: '',
  expected_food_on_breakfast: '',
  expected_food_on_lunch_dinner: '',
  step: 3,
};

export const fieldValidations: FieldValidation[] = [
  ...textFields.eatingPattern.map((field) => ({
    name: field.name,
    required: true,
    label: field.label,
    errorMessage: field.errorMessage,
    type: FieldType.TEXT,
  })),
  ...selectFields.eatingPattern.map((field) => ({
    name: field.name,
    required: true,
    label: field.label,
    type: FieldType.RADIO_BUTTON,
  })),
];
