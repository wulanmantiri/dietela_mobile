import React, { FC } from 'react';
import { View } from 'react-native';
import { RadioButtonGroup, TextField, Picker } from 'components/form';

import { initialValues, fieldValidations } from './schema';
import { textFields, selectFields } from 'constants/questionnaire';
import QuestionnaireWrapper, { styles } from '../QuestionnaireWrapper';
import { NavProps } from '../QuestionnaireWrapper/types';

const Questionnaire2: FC<NavProps> = (navProps) => (
  <QuestionnaireWrapper
    {...navProps}
    Questionnaire={({ getTextInputProps, getFormFieldProps }) => (
      <View>
        {selectFields.eatingPattern.map((props, i) => {
          const FormField = props.picker ? Picker : RadioButtonGroup;
          return (
            <View key={`select${i}`} style={styles.spacing}>
              <FormField
                {...props}
                choices={props.choiceList.map((label, id) => ({
                  label,
                  value: id + 1,
                }))}
                {...getFormFieldProps(props.name)}
                required
              />
            </View>
          );
        })}
        {textFields.eatingPattern.map((props, i) => (
          <TextField
            {...props}
            {...getTextInputProps(props.name)}
            required
            key={`textfield${i}`}
          />
        ))}
      </View>
    )}
    initialValues={initialValues}
    fieldValidations={fieldValidations}
  />
);

export default Questionnaire2;
