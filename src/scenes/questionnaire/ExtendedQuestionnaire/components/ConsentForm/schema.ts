import { yesOrNo } from 'constants/options';

export const termsAndConditions = [
  'Menyampaikan informasi tentang pola makan, aktivitas fisik, dan kondisi kesehatan dirinya dengan jujur',
  'Memberikan data tambahan terkait diagnosis medis atau kondisi klinis yang diderita (jika ada)',
  'Mengikuti alur program yang telah didesain oleh Dietela',
  'Menjawab setiap pertanyaan diet coaching, monitoring rutin, dan follow-up gizi yang diajukan oleh Ahli Gizi atau Dietisien yang menangani klien',
  'Mengirimkan foto makanan selama 3 x 24 jam setiap minggunya, yaitu 2 x 24 jam di hari kerja dan 1 x 24 jam di akhir pekan. Foto makanan ini penting untuk  Nutrisionis atau Dietisien menganalisa pola makan dan keberhasilan program yang dilakukan oleh klien (Khusus bagi Klien One-Time Consultation dan Holistic Diet Coaching)',
  'Foto seluruh badan tampak badan, samping dan belakang yang sangat berguna untuk proses assessment atau pemeriksaan fisik dan evaluasi oleh ahli gizi mengenai penerapan meal plan dan coaching terhadap komposisi tubuh',
  'Mengirimkan foto pribadi yang akan digunakan untuk identifikasi pada dokumen meal plan klien',
];

export const initialValues = {
  agree_to_all_statements_consent: 0,
  personal_data_consent: 0,
  step: 1,
};

export const consentQuestions = [
  {
    name: 'agree_to_all_statements_consent',
    label: 'Saya menyetujui semua pernyataan diatas.',
    required: true,
    choiceList: yesOrNo,
  },
  {
    name: 'personal_data_consent',
    label:
      'Saya menyetujui data saya boleh digunakan untuk keperluan pengembangan & penelitian ilmu gizi dan keseharan dengan data identitas saya yang dirahasiakan.',
    required: true,
    choiceList: yesOrNo,
  },
];
