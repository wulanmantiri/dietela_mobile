import React, { FC } from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

import { InfoCard } from 'components/core';
import { RadioButtonGroup } from 'components/form';
import { Section } from 'components/layout';
import { typographyStyles } from 'styles';

import { styles } from './styles';
import { consentQuestions, termsAndConditions, initialValues } from './schema';

import QuestionnaireWrapper from '../QuestionnaireWrapper';
import { NavProps } from '../QuestionnaireWrapper/types';

const ConsentForm: FC<NavProps> = (navProps) => (
  <QuestionnaireWrapper
    {...navProps}
    Questionnaire={({ getFormFieldProps }) => (
      <View>
        <View style={styles.tnc}>
          <Text style={[typographyStyles.bodyMedium, styles.spacing]}>
            Pelayanan gizi di Dietela, dilakukan dalam bentuk program perubahan
            pola makan dan gaya hidup menjadi lebih sehat yang berlangsung mulai
            dari 3 hari sampai dengan 6 bulan. Selama program berlangsung Anda
            sebagai klien akan didampingi oleh seorang Nutrisionis atau
            Dietisien yang didelegasikan oleh Dietela.
          </Text>
          <Text style={[typographyStyles.bodyMedium, styles.spacing]}>
            Dalam program tersebut akan diberikan servis berupa, “pengecekan
            gizi” secara langsung atau online, “konseling gizi” via pesan
            elektronik atau telepon atau video call, “diet coaching” via pesan
            elektronik, “monitoring rutin” melalui pesan elektronik, “kunjungan
            gizi ke lokasi” klien (hanya untuk layanan tertentu), “follow-up”
            (hanya untuk klien layanan berjangka 1 bulan atau lebih) melalui
            telepon atau video call, dan “exit counseling” atau konseling akhir
            (hanya untuk klien layanan berjangka 1 bulan atau lebih) yang
            dilakukan 1 bulan sekali melalui telepon atau video call.
          </Text>
          <Text style={[typographyStyles.bodyMedium, styles.spacing]}>
            Seluruh data dan informasi klien akan disimpan pada lokasi
            penyimpanan yang aman dan tidak akan kami berikan kepada pihak
            manapun.
          </Text>
          <Text style={[typographyStyles.bodyMedium, styles.spacing]}>
            Dengan menceklis kolom dibawah ini, maka klien setuju untuk:
          </Text>
          {termsAndConditions.map((condition, i) => (
            <Section key={`condition${i}`}>
              <InfoCard content={`${i + 1}. ${condition}`} />
            </Section>
          ))}
        </View>
        {consentQuestions.map((props, i) => (
          <Section key={`radiobuttongroup${i}`}>
            <RadioButtonGroup
              {...props}
              {...getFormFieldProps(props.name)}
              choices={props.choiceList.map((label, id) => ({
                label,
                value: id + 1,
              }))}
            />
          </Section>
        ))}
      </View>
    )}
    initialValues={initialValues}
  />
);

export default ConsentForm;
