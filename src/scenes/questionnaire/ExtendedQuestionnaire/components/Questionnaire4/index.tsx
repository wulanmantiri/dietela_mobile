import React, { FC } from 'react';
import { View } from 'react-native';

import { TextField, CheckboxGroup } from 'components/form';
import { textFields, selectFields } from 'constants/questionnaire';

import { initialValues, convertPayload } from './schema';
import QuestionnaireWrapper, { styles } from '../QuestionnaireWrapper';
import { NavProps } from '../QuestionnaireWrapper/types';

const Questionnaire4: FC<NavProps> = (navProps) => (
  <QuestionnaireWrapper
    {...navProps}
    Questionnaire={({ getTextInputProps, getFormFieldProps }) => (
      <View>
        {selectFields.lifestyle.map((props, i) => {
          let otherProps = {};
          if (props.hasOtherChoice) {
            const textFieldProps = getTextInputProps(`other_${props.name}`);
            otherProps = {
              hasOtherChoice: true,
              otherChoiceValue: props.otherChoiceValue,
              otherValue: textFieldProps.value,
              setOtherValue: textFieldProps.onChangeText,
            };
          }
          return (
            <View key={`checkbox${i}`} style={styles.spacing}>
              <CheckboxGroup
                {...props}
                choices={props.choiceList.map((label, id) => ({
                  label,
                  value: id + 1,
                }))}
                {...getFormFieldProps(props.name)}
                {...otherProps}
              />
            </View>
          );
        })}
        {textFields.lifestyle.map((props, i) => (
          <TextField
            {...props}
            {...getTextInputProps(props.name)}
            multiline={props.multiline}
            key={`textfield${i}`}
          />
        ))}
      </View>
    )}
    initialValues={initialValues}
    convertPayload={convertPayload}
  />
);

export default Questionnaire4;
