import React from 'react';
import { render, waitFor, fireEvent } from '@testing-library/react-native';
import axios from 'axios';

import Questionnaire4 from '.';
import { mockDietQuestionnaire } from 'mocks/dietQuestionnaire';
import { mockUserContext } from 'mocks/userContext';
import { UserContext } from 'provider';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      reset: mockedNavigate,
    }),
  };
});

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('Questionnaire4', () => {
  const userProviderValues = {
    ...mockUserContext,
  };

  it('renders correctly', () => {
    render(
      <UserContext.Provider value={userProviderValues}>
        <Questionnaire4 route={{ params: mockDietQuestionnaire }} />
      </UserContext.Provider>,
    );
  });

  it('redirects to extended questionnaire if all form values are valid and submit success', async () => {
    const updateDietQuestionnaireApi = () =>
      Promise.resolve({
        status: 200,
        data: mockDietQuestionnaire,
      });
    mockAxios.request.mockImplementationOnce(updateDietQuestionnaireApi);

    const { getByText } = render(
      <UserContext.Provider value={userProviderValues}>
        <Questionnaire4 route={{ params: mockDietQuestionnaire }} />
      </UserContext.Provider>,
    );

    const submitButton = getByText(/Simpan/i);
    await waitFor(() => fireEvent.press(submitButton));

    expect(mockedNavigate).toHaveBeenCalled();
  });
});
