import { filterArr } from 'utils/form';

export const initialValues = {
  meal_provider: [],
  cigarette_alcohol_condition: [],
  physical_activity: [],
  other_physical_activity: '',
  food_alergies: '',
  diet_drinks: '',
  multivitamin_tablet_suplement: '',
  diet_and_life_style_story: '',
  step: 5,
};

export const convertPayload = (values: typeof initialValues) => ({
  ...values,
  cigarette_alcohol_condition:
    values.cigarette_alcohol_condition.length === 0
      ? [4]
      : filterArr(values.cigarette_alcohol_condition, 4),
});
