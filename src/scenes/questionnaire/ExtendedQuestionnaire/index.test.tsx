import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import axios from 'axios';

import { mockDietQuestionnaire } from 'mocks/dietQuestionnaire';
import ExtendedQuestionnaire from '.';

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('ExtendedQuestionnaire', () => {
  const dietQuestionnaire = [mockDietQuestionnaire];
  const retrieveDietQuestionnaireApi = () =>
    Promise.resolve({
      status: 200,
      data: dietQuestionnaire,
    });

  it('renders correctly', async () => {
    mockAxios.request.mockImplementationOnce(retrieveDietQuestionnaireApi);
    render(<ExtendedQuestionnaire />);
    await waitFor(() => expect(mockAxios.request).toBeCalled());
  });
});
