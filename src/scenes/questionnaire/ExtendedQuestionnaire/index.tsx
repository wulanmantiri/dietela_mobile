import React, { FC } from 'react';

import * as ROUTES from 'constants/routes';
import { StepByStepForm } from 'components/form';
import { pageHeaders } from 'constants/questionnaire';
import { retrieveDietQuestionnaireApi } from 'services/dietQuestionnaire';
import { Loader } from 'components/core';
import { useApi } from 'hooks';

const ExtendedQuestionnaire: FC = () => {
  const { isLoading, data: questionnaire } = useApi(
    retrieveDietQuestionnaireApi,
  );
  const data =
    questionnaire && questionnaire.length > 0
      ? questionnaire[0]
      : { finished_steps: [] };

  if (isLoading) {
    return <Loader />;
  }
  return (
    <StepByStepForm
      currentPage={data.finished_steps.length}
      pages={pageHeaders.map((name, i) => ({
        name,
        route: ROUTES.extendedQuestionnaireById(i),
      }))}
      defaultValues={data}
    />
  );
};

export default ExtendedQuestionnaire;
