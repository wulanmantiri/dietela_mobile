import React, { FC, useState, useEffect } from 'react';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { WizardContainer, Toast } from 'components/core';
import { MultipleChoice, MultipleCheckbox } from 'components/form';
import CACHE_KEYS from 'constants/cacheKeys';
import * as ROUTES from 'constants/routes';
import { useForm } from 'hooks';
import { createDietProfileApi } from 'services/dietelaQuiz';
import { layoutStyles } from 'styles';

import { BiodataForm } from './components';
import {
  allAccessQuestions,
  initialValues,
  textFields,
  radioButtonGroups,
  fieldValidations,
  convertPayload,
} from './schema';
import { generateValidationSchema } from 'utils/form';
import { setCache } from 'utils/cache';

const AllAccessQuestionnaire: FC = () => {
  const navigation = useNavigation();

  const [currentPage, setCurrentPage] = useState(1);

  const {
    getTextInputProps,
    getFormFieldProps,
    isFieldError,
    isFormUntouched,
    handleSubmit,
    isSubmitting,
    values: formValues,
    setFieldValue,
  } = useForm({
    initialValues,
    validationSchema: generateValidationSchema(fieldValidations),
    onSubmit: async (values) => {
      const response = await createDietProfileApi(convertPayload(values));

      if (response.success && response.data) {
        await setCache(CACHE_KEYS.dietProfileId, response.data.id);
        await setCache(
          CACHE_KEYS.programRecommendations,
          JSON.stringify(response.data.quiz_result.program_recommendation),
        );
        navigation.navigate(ROUTES.dietelaQuizResult, response.data);
      } else {
        Toast.show({
          type: 'error',
          text1: 'Gagal menyimpan data',
          text2: 'Terjadi kesalahan pada sisi kami. Silakan coba lagi',
        });
      }
    },
  });

  const isCurrentPageError = (): boolean => {
    if (currentPage === 1) {
      const fields = [...textFields, ...radioButtonGroups];
      return (
        isFormUntouched() ||
        fields.reduce(
          (acc: boolean, item) => acc || isFieldError(item.name),
          false,
        )
      );
    }
    const fieldPage = currentPage - (formValues.gender === 1 ? 1 : 2);
    const fieldName = allAccessQuestions[fieldPage].fieldName;
    return isFieldError(fieldName);
  };

  const questions = allAccessQuestions.slice(formValues.gender === 1 ? 1 : 0);

  useEffect(() => {
    if (formValues.gender === 1) {
      setFieldValue('special_condition', 1);
    }
  }, [formValues.gender, setFieldValue]);

  return (
    <WizardContainer
      currentStep={currentPage}
      setCurrentStep={setCurrentPage}
      onFinish={handleSubmit}
      isLoading={isSubmitting}
      isNextDisabled={isCurrentPageError()}
      components={[
        <BiodataForm
          textFields={textFields.map((fieldProps) => ({
            ...fieldProps,
            ...getTextInputProps(fieldProps.name),
          }))}
          radioButtonGroups={radioButtonGroups.map((fieldProps) => ({
            ...fieldProps,
            ...getFormFieldProps(fieldProps.name),
          }))}
        />,
        ...questions.map((question, i) => {
          const FormField = question.multiple
            ? MultipleCheckbox
            : MultipleChoice;

          return (
            <View style={layoutStyles}>
              <FormField
                key={`allAccessQn${i}`}
                questionNumber={i + 1}
                questionLabel={question.questionLabel}
                totalQuestions={questions.length}
                helperText={question.helperText}
                choices={question.choiceList.map((choice, choiceId) => ({
                  label: choice,
                  value: choiceId + (question.multiple ? 2 : 1),
                }))}
                {...getFormFieldProps(question.fieldName)}
              />
            </View>
          );
        }),
      ]}
    />
  );
};

export default AllAccessQuestionnaire;
