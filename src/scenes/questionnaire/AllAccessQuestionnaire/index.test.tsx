import React from 'react';
import { act, render, fireEvent, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';
import axios from 'axios';

import AllAccessQuestionnaire from '.';
import { allAccessQuestions, textFields } from './schema';
import { mockQuizResult } from 'mocks/quizResult';

jest.mock('react-native-toast-message');
jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('AllAccessQuestionnaire', () => {
  const validFormValues: { [_: string]: any } = {
    name: 'Dietela',
    email: 'dietela@gmail.com',
    age: '20',
    weight: '60',
    height: '172',
    gender: 1,
    special_condition: 1,
    body_activity: 1,
    vegetables_in_one_day: 1,
    fruits_in_one_day: 1,
    fried_food_in_one_day: 1,
    sweet_snacks_in_one_day: 1,
    sweet_drinks_in_one_day: 1,
    packaged_food_in_one_day: 1,
    large_meal_in_one_day: 1,
    snacks_in_one_day: 1,
    breakfast_type: 1,
    current_condition: 1,
    problem_to_solve: 1,
    health_problem: [2, 3],
  };

  it('initially has disabled next button', () => {
    const { getByText, queryByText } = render(
      <AllAccessQuestionnaire />,
      ROUTES.allAccessQuestionnaire,
    );

    const biodataForm = queryByText(/Data Diri/i);
    expect(biodataForm).toBeTruthy();

    const nextButton = getByText(/Lanjut/i);
    expect(nextButton).toBeTruthy();
    fireEvent.press(nextButton);

    expect(queryByText(/Data Diri/i)).toBeTruthy();
  });

  it('redirects to quiz result page if all form values are valid and submit success', async () => {
    const createDietProfileApi = () =>
      Promise.resolve({
        status: 201,
        data: mockQuizResult,
      });
    mockAxios.request.mockImplementationOnce(createDietProfileApi);

    const { getByText, getByPlaceholderText } = render(
      <AllAccessQuestionnaire />,
      ROUTES.allAccessQuestionnaire,
    );

    textFields.forEach(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validFormValues[name]);
    });

    const maleChoice = getByText(/Pria/i);
    act(() => fireEvent.press(maleChoice));

    allAccessQuestions.slice(1).forEach(({ choiceList }) => {
      const nextButton = getByText(/Lanjut/i);
      fireEvent.press(nextButton);

      const firstChoice = getByText(choiceList[0]);
      fireEvent.press(firstChoice);
    });

    const submitButton = getByText('Selesai');
    await waitFor(() => fireEvent.press(submitButton));

    const quizResultPage = getByText(/Quiz Result/i);
    expect(quizResultPage).toBeTruthy();
  });

  it('does not redirect to quiz result page if all form values are valid but submit fails', async () => {
    const createDietProfileApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      });
    mockAxios.request.mockImplementationOnce(createDietProfileApi);

    const { getByText, getByPlaceholderText, queryByText } = render(
      <AllAccessQuestionnaire />,
      ROUTES.allAccessQuestionnaire,
    );

    textFields.forEach(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validFormValues[name]);
    });

    const femaleChoice = getByText(/Wanita/i);
    act(() => fireEvent.press(femaleChoice));

    allAccessQuestions.forEach(({ choiceList }) => {
      const nextButton = getByText(/Lanjut/i);
      fireEvent.press(nextButton);

      const firstChoice = getByText(choiceList[0]);
      fireEvent.press(firstChoice);
    });

    const submitButton = getByText('Selesai');
    await waitFor(() => fireEvent.press(submitButton));

    const quizResultPage = queryByText(/Quiz Result/i);
    expect(quizResultPage).toBeFalsy();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
});
