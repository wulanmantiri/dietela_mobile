import { FieldValidation, FieldType, filterArr } from 'utils/form';
import { TextFieldSchema, RadioButtonGroupSchema } from 'types/form';
import { DietProfileRequest } from 'services/dietelaQuiz/models';

export const textFields: TextFieldSchema[] = [
  {
    label: 'Nama',
    placeholder: 'Wendy Asri Karim',
    required: true,
    name: 'name',
  },
  {
    label: 'Email',
    placeholder: 'example@address.com',
    required: true,
    fieldType: FieldType.EMAIL,
    name: 'email',
  },
  {
    label: 'Usia (tahun)',
    placeholder: 'Ex: 20',
    required: true,
    name: 'age',
    fieldType: FieldType.NUMBER,
    max: 80,
    keyboardType: 'numeric',
  },
  {
    label: 'Berat badan (kg)',
    placeholder: 'Ex: 60',
    required: true,
    name: 'weight',
    fieldType: FieldType.NUMBER,
    keyboardType: 'numeric',
  },
  {
    label: 'Tinggi badan (cm)',
    placeholder: 'Ex: 160',
    required: true,
    fieldType: FieldType.NUMBER,
    name: 'height',
    keyboardType: 'numeric',
  },
];

export const radioButtonGroups: RadioButtonGroupSchema[] = [
  {
    label: 'Jenis kelamin',
    required: true,
    name: 'gender',
    choices: [
      {
        value: 1,
        label: 'Pria',
      },
      {
        value: 2,
        label: 'Wanita',
      },
    ],
  },
];

export const initialValues = {
  name: '',
  email: '',
  age: '',
  weight: '',
  height: '',
  gender: 0,
  special_condition: 0,
  body_activity: 0,
  vegetables_in_one_day: 0,
  fruits_in_one_day: 0,
  fried_food_in_one_day: 0,
  sweet_snacks_in_one_day: 0,
  sweet_drinks_in_one_day: 0,
  packaged_food_in_one_day: 0,
  large_meal_in_one_day: 0,
  snacks_in_one_day: 0,
  breakfast_type: 0,
  current_condition: 0,
  problem_to_solve: 0,
  health_problem: [],
};

export const allAccessQuestions = [
  {
    fieldName: 'special_condition',
    questionLabel: 'Apakah kamu punya kondisi keibuan?',
    choiceList: [
      'Tidak ada kondisi keibuan',
      'Sedang hamil trimester 1',
      'Sedang hamil trimester 2',
      'Sedang hamil trimester 3',
      'Menyusui bayi 0-6 bulan',
      'Menyusui bayi 7-12 bulan',
    ],
  },
  {
    fieldName: 'body_activity',
    questionLabel: 'Seberapa aktif badanmu bergerak dalam sehari?',
    choiceList: [
      'Saya lebih banyak duduk dan tidak rutin olahraga',
      'Banyak jalan, banyak bergerak, tapi tidak rutin olahraga',
      'Olahraga rutin 30-45 menit setiap hari',
      'Olahraga rutin 45-60 menit setiap hari',
      'Olahraga rutin lebih dari 60 menit setiap hari',
    ],
  },
  {
    fieldName: 'vegetables_in_one_day',
    questionLabel:
      'Biasanya, berapa banyak sayur yang dimakan dalam satu hari?',
    choiceList: [
      '0 porsi sayur per hari',
      '1 porsi sayur per hari',
      '2 porsi sayur per hari',
      '3 porsi sayur per hari',
      'Lebih dari 3 porsi sayur per hari',
    ],
  },
  {
    fieldName: 'fruits_in_one_day',
    questionLabel: 'Biasanya, berapa banyak buah yang dimakan dalam satu hari?',
    choiceList: [
      '0 porsi buah per hari',
      '1 porsi buah per hari',
      '2 porsi buah per hari',
      '3 porsi buah per hari',
      'Lebih dari 3 porsi buah per hari',
    ],
  },
  {
    fieldName: 'fried_food_in_one_day',
    questionLabel:
      'Biasanya, berapa potong gorengan yang kamu makan dalam satu hari?',
    choiceList: [
      '0 potong gorengan per hari',
      '1 potong gorengan per hari',
      '2 potong gorengan per hari',
      '3 potong gorengan per hari',
      'Lebih dari 3 potong gorengan per hari',
    ],
  },
  {
    fieldName: 'sweet_snacks_in_one_day',
    questionLabel:
      'Biasanya, berapa potong cemilan manis yang kamu makan dalam satu hari?',
    helperText:
      'Contoh cemilan manis adalah berupa kue-kue manis, cokelat, wafer, biskuit, dan cemilan lainnya. Pilih satu yang paling cocok.',
    choiceList: [
      '0 potong cemilan manis per hari',
      '1 potong cemilan manis per hari',
      '2 potong cemilan manis per hari',
      '3 potong cemilan manis per hari',
      'Lebih dari 3 potong cemilan manis per hari',
    ],
  },
  {
    fieldName: 'sweet_drinks_in_one_day',
    questionLabel:
      'Biasanya, berapa gelas minuman manis yang kamu habiskan dalam satu hari?',
    helperText:
      'Contoh minuman manis adalah kopi susu, teh manis, soda, dan lain-lain. Pilih satu yang paling cocok.',
    choiceList: [
      '0 gelas minuman manis per hari',
      '1 gelas minuman manis per hari',
      '2 gelas minuman manis per hari',
      '3 gelas minuman manis per hari',
      'Lebih dari 3 gelas minuman manis per hari',
    ],
  },
  {
    fieldName: 'packaged_food_in_one_day',
    questionLabel:
      'Biasanya, berapa porsi makanan kemasan dan makanan cepat saji yang kamu makan dalam satu hari?',
    choiceList: [
      '0 porsi per hari',
      '1 porsi per hari',
      '2 porsi per hari',
      '3 porsi per hari',
      'Lebih dari 3 porsi per hari',
    ],
  },
  {
    fieldName: 'large_meal_in_one_day',
    questionLabel: 'Berapa kali makan besar dalam satu hari?',
    choiceList: [
      '1 kali makan besar per hari',
      '2 kali makan besar per hari',
      '3 kali makan besar per hari',
      'Lebih dari 3 kali makan besar per hari',
    ],
  },
  {
    fieldName: 'snacks_in_one_day',
    questionLabel: 'Berapa kali makan cemilan dalam satu hari?',
    choiceList: [
      'Tidak makan cemilan',
      '1 kali ngemil per hari',
      '2 kali ngemil per hari',
      '3 kali ngemil per hari',
      'Lebih dari 3 kali ngemil per hari',
    ],
  },
  {
    fieldName: 'breakfast_type',
    questionLabel: 'Jenis makanan saat makan pagi?',
    choiceList: [
      'Tidak pernah makan pagi',
      'Makanan yang ringan saja',
      'Makanan yang sampai mengenyangkan perut',
    ],
  },
  {
    fieldName: 'current_condition',
    questionLabel: 'Kondisi mana yang paling sesuai denganmu saat ini?',
    helperText: 'Saya...',
    choiceList: [
      'BELUM tertarik melakukan diet dan gaya hidup sehat dan TIDAK TAHU kondisi gizi serta kesehatan saya',
      'BELUM tertarik melakukan diet dan gaya hidup sehat meskipun TAHU kondisi gizi serta kesehatan saya',
      'MULAI tertarik melakukan diet dan gaya hidup sehat meskipun saya TIDAK TAHU kondisi gizi serta kesehatan saya',
      'MULAI tertarik melakukan diet dan gaya hidup sehat dan TAHU kondisi gizi dan kesehatan saya',
      'Sudah tahu manfaat diet dan gaya hidup sehat, tapi tidak tahu harus mulai menerapkannya dari mana',
      'Sudah pernah mengubah diet dan gaya hidup menjadi lebih sehat tetapi tidak bertahan lama (dilakukan kurang dari 6 bulan)',
      'Sudah pernah berhasil menjalani diet dan gaya hidup sehat selama lebih dari 6 bulan dan ingin melakukannya lagi',
    ],
  },
  {
    fieldName: 'problem_to_solve',
    questionLabel: 'Masalah yang ingin kamu selesaikan?',
    helperText: 'Saya ingin...',
    choiceList: [
      'Turun berat badan dengan target yang realistis',
      'Naik berat badan dengan target yang realistis',
      'Berat badan yang bertahan permanen, menjaga agar berat badan tidak naik, anti yo-yo diet',
      'Butuh pengaturan gizi Ibu Hamil',
      'Butuh pengaturan gizi Ibu Menyusui',
      'Menyiapkan tubuh untuk mencapai kesuburan dan persiapan kehamilan',
      'Mengatur gula darah tinggi (Pre-diabetes), hiperlipidemia (kolesterol), hipertensi (tekanan darah tinggi)',
      'Capai pola makan sehat dan tetap enjoy menikmati makanan yang disukai',
      'Transisi vegan',
      'Butuh pengaturan gizi pada anak-anak atau remaja',
    ],
  },
  {
    fieldName: 'health_problem',
    questionLabel: 'Masalah kesehatan yang kamu miliki?',
    multiple: true,
    choiceList: [
      'Diabetes Tipe 2',
      'Diabetes Tipe 1',
      'PCOS (Polycystic Ovary Syndrome)',
      'Kolesterol tinggi',
      'Asam urat tinggi',
      'Tekanan darah tinggi',
      'Gula darah tinggi',
      'Kanker',
      'HIV/AIDS',
      'Maag/GERD/Dispepsia/Gangguan lambung',
      'Penyintas Kanker',
      'Penyakit Kronis Lainnya',
    ],
  },
];

export const fieldValidations: FieldValidation[] = [
  ...textFields.map((field) => ({
    name: field.name,
    required: field.required,
    label: field.label,
    type: field.fieldType || FieldType.TEXT,
    max: field.max,
  })),
  ...radioButtonGroups.map((field) => ({
    name: field.name,
    required: field.required,
    label: field.label,
    type: FieldType.RADIO_BUTTON,
  })),
  ...allAccessQuestions
    .filter((field) => !field.multiple)
    .map((field) => ({
      name: field.fieldName,
      type: FieldType.RADIO_BUTTON,
    })),
];

export const convertPayload = (
  values: typeof initialValues,
): DietProfileRequest => ({
  ...values,
  age: parseInt(values.age, 10),
  height: parseInt(values.height, 10),
  weight: parseInt(values.weight, 10),
  special_condition: values.gender === 1 ? 1 : values.special_condition,
  health_problem:
    values.health_problem.length === 0
      ? [1]
      : filterArr(values.health_problem, 1),
});
