import React, { FC } from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-elements';

import { TextField, RadioButtonGroup } from 'components/form';
import { layoutStyles, typographyStyles } from 'styles';

import { styles } from './styles';
import { Props } from './types';

const BiodataForm: FC<Props> = ({ textFields, radioButtonGroups }) => (
  <View style={layoutStyles}>
    <Text style={[typographyStyles.overlineBig, styles.spacing]}>
      Data Diri (Pra-Kuis)
    </Text>
    <Text style={[typographyStyles.headingLarge, styles.spacing]}>
      Halo! Yuk, mulai dari berkenalan dulu
    </Text>
    <Text style={[typographyStyles.bodyMedium, styles.bigSpacing]}>
      Informasi kamu hanya kami gunakan untuk personalisasi kebutuhan diet saja.
    </Text>
    {textFields.map((props, i) => (
      <TextField {...props} key={`textfield${i}`} />
    ))}
    {radioButtonGroups.map((props, i) => (
      <RadioButtonGroup {...props} key={`radiobuttongroup${i}`} />
    ))}
  </View>
);

export default BiodataForm;
