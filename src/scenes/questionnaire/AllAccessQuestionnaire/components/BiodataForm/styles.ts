import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  gender: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    padding: 0,
    margin: 0,
  },
  radioButton: {
    flex: 1,
  },
  spacing: {
    marginBottom: 14,
  },
  bigSpacing: {
    marginBottom: 24,
  },
});
