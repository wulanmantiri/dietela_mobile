import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import LoginChoosePlan from '.';

describe('LoginChoosePlan', () => {
  it('renders correctly', () => {
    render(<LoginChoosePlan />, ROUTES.loginChoosePlan);
  });

  test('has link button that navigates to Registration Page', async () => {
    const { getByText, queryByText, queryAllByText } = render(
      <LoginChoosePlan />,
      ROUTES.loginChoosePlan,
    );

    expect(queryByText(/Kembali ke Registrasi/i)).toBeTruthy();
    await waitFor(() => fireEvent.press(getByText(/Kembali ke Registrasi/i)));

    expect(queryAllByText(/Registrasi/i)).toBeTruthy();
  });
});
