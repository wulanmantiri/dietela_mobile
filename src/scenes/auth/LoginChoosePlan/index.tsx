import { useNavigation } from '@react-navigation/native';
import { Link } from 'components/core';
import React, { FC } from 'react';
import Login from '../Login';

const LoginChoosePlan: FC = () => {
  const navigation = useNavigation();

  return (
    <>
      <Login />
      <Link title="Kembali ke Registrasi" onPress={() => navigation.goBack()} />
    </>
  );
};

export default LoginChoosePlan;
