import React, { FC, useContext, useState } from 'react';
import { ScrollView, StyleSheet, Text } from 'react-native';
import { UserContext } from 'provider';
import { useForm } from 'hooks';

import { BigButton, Link, Toast } from 'components/core';

import {
  fieldValidation,
  initialValues,
  radioButtonGroups,
  textField,
} from './schema';
import { generateValidationSchema } from 'utils/form';
import { layoutStyles } from 'styles';
import { RadioButtonGroup, TextField } from 'components/form';
import { Section } from 'components/layout';
import { useNavigation } from '@react-navigation/native';
import * as ROUTES from 'constants/routes';

const isPasswordField = (name: string) => name === 'password';

const NutritionistAdminLogin: FC = () => {
  const { login } = useContext(UserContext);
  const [nonFieldError, setNonFieldError] = useState<string | null>();
  const navigation = useNavigation();

  const {
    getTextInputProps,
    getFormFieldProps,
    handleSubmit,
    isSubmitting,
    setFieldError,
  } = useForm({
    initialValues,
    validationSchema: generateValidationSchema(fieldValidation),
    onSubmit: async (values) => {
      const response = await login(values);

      if (!response.success) {
        const error = response.error;

        setFieldError('email', error.email);
        setFieldError('password', error.password);
        setNonFieldError(error.non_field_errors);

        Toast.show({
          type: 'error',
          text1: 'Gagal login akun',
          text2: 'Terjadi kesalahan login. Silakan coba lagi',
        });
      }
    },
  });

  return (
    <ScrollView contentContainerStyle={layoutStyles}>
      {textField.map(({ name, label, required, placeholder }, i) => (
        <TextField
          key={`field${i}`}
          label={label}
          required={required}
          placeholder={placeholder}
          {...getTextInputProps(name)}
          secureTextEntry={isPasswordField(name)}
        />
      ))}
      {radioButtonGroups.map((fieldProps, i) => (
        <RadioButtonGroup
          {...fieldProps}
          {...getFormFieldProps(fieldProps.name)}
          key={`radiobuttongroup${i}`}
        />
      ))}
      {nonFieldError && (
        <Text style={styles.nonfieldError}>{nonFieldError}</Text>
      )}
      <Section>
        <BigButton
          title="login"
          onPress={handleSubmit}
          loading={isSubmitting}
          testID="timDietelaLogin"
        />
      </Section>
      <Section>
        <Link
          title="Login sebagai Pengguna"
          onPress={() => navigation.navigate(ROUTES.login)}
        />
      </Section>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  nonfieldError: { color: 'red' },
});

export default NutritionistAdminLogin;
