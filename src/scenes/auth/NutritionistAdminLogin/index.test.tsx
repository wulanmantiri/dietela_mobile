import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';
import axios from 'axios';

import NutritionistAdminLogin from '.';
import { authResponse, validLoginValues } from 'mocks/auth';
import { textField } from './schema';

jest.mock('react-native-toast-message');
jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('NutritionistAdminLogin', () => {
  it('renders correctly', () => {
    render(<NutritionistAdminLogin />, ROUTES.nutritionistAdminLogin);
  });

  it('success when field is valid and submit success', async () => {
    const loginApi = () =>
      Promise.resolve({
        status: 201,
        data: authResponse,
      });
    mockAxios.request.mockImplementationOnce(loginApi);

    const { getByPlaceholderText, getByTestId } = render(
      <NutritionistAdminLogin />,
      ROUTES.nutritionistAdminLogin,
    );

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validLoginValues[name]);
    });

    const loginButton = getByTestId('timDietelaLogin');
    await waitFor(() => fireEvent.press(loginButton));
  });

  it('fails when field is valid and submit fails', async () => {
    const loginApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      });
    mockAxios.request.mockImplementationOnce(loginApi);

    const { getByPlaceholderText, queryByText, getByTestId } = render(
      <NutritionistAdminLogin />,
      ROUTES.nutritionistAdminLogin,
    );

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validLoginValues[name]);
    });

    const loginButton = getByTestId('timDietelaLogin');
    await waitFor(() => fireEvent.press(loginButton));

    const toastWarning = queryByText(/Profile/i);
    expect(toastWarning).toBeFalsy();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  test('has link button that navigates to Nutritionist/Admin Login Page', async () => {
    const { getByText, queryByText, queryAllByText } = render(
      <NutritionistAdminLogin />,
      ROUTES.nutritionistAdminLogin,
    );

    expect(queryByText(/Login sebagai Pengguna/i)).toBeTruthy();
    await waitFor(() => fireEvent.press(getByText(/Login sebagai Pengguna/i)));

    expect(queryAllByText(/Login/i)).toBeTruthy();
  });
});
