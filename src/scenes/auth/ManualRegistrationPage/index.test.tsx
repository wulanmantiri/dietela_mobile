import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';
import axios from 'axios';

import ManualRegistrationPage from '.';
import CACHE_KEYS from 'constants/cacheKeys';
import { setCache } from 'utils/cache';
import { textField } from './schema';
import {
  authResponse,
  validRegistrationValues,
  invalidRegistrationValues,
} from 'mocks/auth';

jest.mock('react-native-toast-message');
jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('ManualRegistrationPage', () => {
  it('shows dietela cover loader when is loading', async () => {
    const { queryByText } = render(
      <ManualRegistrationPage />,
      ROUTES.registration,
    );

    await waitFor(() => expect(queryByText(/daftarkan akun/i)).toBeFalsy());
  });

  it('renders correctly', async () => {
    await setCache(CACHE_KEYS.cartId, 1);
    await setCache(CACHE_KEYS.dietProfileId, 1);

    render(<ManualRegistrationPage />, ROUTES.registration);
  });

  it('success when field is valid and submit success', async () => {
    const signupApi = () =>
      Promise.resolve({
        status: 201,
        data: authResponse,
      });
    mockAxios.request.mockImplementationOnce(signupApi);

    const { getByPlaceholderText, queryByText, getByTestId } = render(
      <ManualRegistrationPage />,
      ROUTES.registration,
    );

    await waitFor(() => expect(queryByText(/daftarkan akun/i)).toBeTruthy());

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validRegistrationValues[name]);
    });

    const submitButton = getByTestId('submitButton');
    await waitFor(() => fireEvent.press(submitButton));
  });

  it('fails when field is valid and submit fails', async () => {
    const signupApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      });
    mockAxios.request.mockImplementationOnce(signupApi);

    const { getByPlaceholderText, queryByText, getByTestId } = render(
      <ManualRegistrationPage />,
      ROUTES.registration,
    );

    await waitFor(() => expect(queryByText(/daftarkan akun/i)).toBeTruthy());

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validRegistrationValues[name]);
    });

    const submitButton = getByTestId('submitButton');
    await waitFor(() => fireEvent.press(submitButton));

    const nextPageText = queryByText(/Checkout/i);
    expect(nextPageText).toBeFalsy();
  });

  it('fails when field is invalid and submit success', async () => {
    const alreadyRegistered = 'Email is already registered';

    const signupApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: {
            name: 'Wrong name',
            email: alreadyRegistered,
            password1: 'Wrong password',
            password2: 'Wrong password',
          },
        },
      });
    mockAxios.request.mockImplementationOnce(signupApi);

    const { getByPlaceholderText, queryByText, getByTestId } = render(
      <ManualRegistrationPage />,
      ROUTES.registration,
    );

    await waitFor(() => expect(queryByText(/daftarkan akun/i)).toBeTruthy());

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, invalidRegistrationValues[name]);
    });

    const submitButton = getByTestId('submitButton');
    await waitFor(() => fireEvent.press(submitButton));

    const nextPageText = queryByText(/Checkout/i);
    expect(nextPageText).toBeFalsy();
  });

  test('has link button that navigates to Login Page', async () => {
    const { getByText, queryByText, queryAllByText } = render(
      <ManualRegistrationPage />,
      ROUTES.registration,
    );

    await waitFor(() => expect(queryByText(/daftarkan akun/i)).toBeTruthy());

    expect(queryByText(/Login disini/i)).toBeTruthy();
    await waitFor(() => fireEvent.press(getByText(/Login disini/i)));

    expect(queryAllByText(/Login/i)).toBeTruthy();
  });
});
