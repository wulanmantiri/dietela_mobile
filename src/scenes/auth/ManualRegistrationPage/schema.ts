import { RegistrationRequest } from 'services/auth/models';
import { TextFieldSchema } from 'types/form';
import { FieldType, FieldValidation } from 'utils/form';

export const textField: TextFieldSchema[] = [
  {
    label: 'Nama',
    placeholder: 'Masukkan nama Anda',
    required: true,
    name: 'name',
  },
  {
    label: 'Email address',
    placeholder: 'Masukkan email Anda',
    required: true,
    name: 'email',
  },
  {
    label: 'Password',
    placeholder: 'Masukkan password Anda',
    required: true,
    name: 'password1',
  },
  {
    label: 'Konfirmasi password',
    placeholder: 'Konfirmasi password Anda',
    required: true,
    name: 'password2',
  },
];

export const initialValues: RegistrationRequest = {
  name: '',
  email: '',
  password1: '',
  password2: '',
};

export const fieldValidation: FieldValidation[] = [
  {
    name: 'name',
    required: true,
    label: 'Nama',
    type: FieldType.TEXT,
  },
  {
    name: 'email',
    required: true,
    label: 'Email address',
    type: FieldType.EMAIL,
  },
  {
    name: 'password1',
    required: true,
    label: 'Password',
    type: FieldType.PASSWORD,
  },
  {
    name: 'password2',
    required: true,
    label: 'Konfirmasi password',
    type: FieldType.CONFIRM_PASSWORD,
    matches: 'password1',
  },
];
