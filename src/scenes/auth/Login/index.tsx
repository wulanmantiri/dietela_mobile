import React, { FC, useContext, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { UserContext } from 'provider';
import { useForm } from 'hooks';

import { GoogleLoginButton } from '../components';
import { BigButton, Link, Toast } from 'components/core';

import { fieldValidation, initialValues, textField } from './schema';
import { generateValidationSchema } from 'utils/form';
import { layoutStyles } from 'styles';
import { TextField } from 'components/form';
import { Section } from 'components/layout';
import { useNavigation } from '@react-navigation/native';
import * as ROUTES from 'constants/routes';

const isPasswordField = (name: string) => name === 'password';

const Login: FC = () => {
  const { login, isLoading, loginWithGoogle } = useContext(UserContext);
  const [nonFieldError, setNonFieldError] = useState<string | null>();
  const navigation = useNavigation();

  const {
    getTextInputProps,
    handleSubmit,
    isSubmitting,
    setFieldError,
  } = useForm({
    initialValues,
    validationSchema: generateValidationSchema(fieldValidation),
    onSubmit: async (values) => {
      const response = await login(values);

      if (!response.success) {
        const error = response.error;

        setFieldError('email', error.email);
        setFieldError('password', error.password);
        setNonFieldError(error.non_field_errors);

        Toast.show({
          type: 'error',
          text1: 'Gagal login akun',
          text2: 'Terjadi kesalahan login. Silakan coba lagi',
        });
      }
    },
  });

  return (
    <View style={layoutStyles}>
      {textField.map(({ name, label, required, placeholder }, i) => (
        <TextField
          key={`field${i}`}
          label={label}
          required={required}
          placeholder={placeholder}
          {...getTextInputProps(name)}
          secureTextEntry={isPasswordField(name)}
        />
      ))}
      {nonFieldError && (
        <Text style={styles.nonfieldError}>{nonFieldError}</Text>
      )}
      <Section>
        <BigButton
          title="login"
          onPress={handleSubmit}
          loading={isSubmitting}
          testID="loginButton"
        />
      </Section>
      <Section>
        <GoogleLoginButton onPress={loginWithGoogle} isLoading={isLoading} />
      </Section>
      <Section>
        <Link
          title="Login sebagai Nutrisionis/Admin"
          onPress={() => navigation.navigate(ROUTES.nutritionistAdminLogin)}
        />
      </Section>
    </View>
  );
};

const styles = StyleSheet.create({
  nonfieldError: { color: 'red' },
});

export default Login;
