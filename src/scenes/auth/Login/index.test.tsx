import React from 'react';
import { render, fireEvent, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';
import axios from 'axios';

import Login from '.';
import { authResponse, validLoginValues } from 'mocks/auth';
import { textField } from './schema';

jest.mock('react-native-toast-message');
jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('Login page', () => {
  it('renders correctly if client has filled questionnaire and cart', async () => {
    const { queryByText } = render(<Login />, ROUTES.login);

    await waitFor(() =>
      expect(queryByText(/Lanjut dengan Google/i)).toBeTruthy(),
    );
  });

  it('success when field is valid and submit success', async () => {
    const loginApi = () =>
      Promise.resolve({
        status: 201,
        data: authResponse,
      });
    mockAxios.request.mockImplementationOnce(loginApi);

    const { getByPlaceholderText, getByTestId } = render(
      <Login />,
      ROUTES.login,
    );

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validLoginValues[name]);
    });

    const loginButton = getByTestId('loginButton');
    await waitFor(() => fireEvent.press(loginButton));
  });

  it('fails when field is valid and submit fails', async () => {
    const loginApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      });
    mockAxios.request.mockImplementationOnce(loginApi);

    const { getByPlaceholderText, queryByText, getByTestId } = render(
      <Login />,
      ROUTES.login,
    );

    textField.map(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validLoginValues[name]);
    });

    const loginButton = getByTestId('loginButton');
    await waitFor(() => fireEvent.press(loginButton));

    const toastWarning = queryByText(/Profile/i);
    expect(toastWarning).toBeFalsy();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  test('has link button that navigates to Nutritionist/Admin Login Page', async () => {
    const { getByText, queryByText, queryAllByText } = render(
      <Login />,
      ROUTES.login,
    );

    expect(queryByText(/Login sebagai Nutrisionis/i)).toBeTruthy();
    await waitFor(() =>
      fireEvent.press(getByText(/Login sebagai Nutrisionis/i)),
    );

    expect(queryAllByText(/Login Tim Dietela/i)).toBeTruthy();
  });
});
