import { LoginRequest, UserRole } from 'services/auth/models';
import { TextFieldSchema } from 'types/form';
import { FieldType, FieldValidation } from 'utils/form';

export const textField: TextFieldSchema[] = [
  {
    label: 'Email address',
    placeholder: 'Masukkan email Anda',
    required: true,
    name: 'email',
  },
  {
    label: 'Password',
    placeholder: 'Masukkan password Anda',
    required: true,
    name: 'password',
  },
];

export const initialValues: LoginRequest = {
  email: '',
  password: '',
  role: UserRole.CLIENT,
};

export const fieldValidation: FieldValidation[] = [
  {
    name: 'email',
    required: true,
    label: 'Email address',
    type: FieldType.EMAIL,
  },
  {
    name: 'password',
    required: true,
    label: 'Password',
    type: FieldType.PASSWORD,
  },
];

export const setRole = (role: UserRole) => (initialValues.role = role);
