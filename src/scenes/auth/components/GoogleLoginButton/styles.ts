import { StyleSheet } from 'react-native';
import { colors, typography } from 'styles';

export const styles = StyleSheet.create({
  googleIcon: {
    width: 24,
    height: 24,
  },
  button: {
    backgroundColor: 'white',
    borderColor: colors.border,
    borderWidth: 0.5,
  },
  container: {
    elevation: 1,
  },
  title: {
    paddingLeft: 24,
    color: 'black',
    ...typography.bodyMedium,
  },
});
