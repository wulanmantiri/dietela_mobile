import { FieldValidation, FieldType } from 'utils/form';
import { TextFieldSchema } from 'types/form';
import { NutritionistComment } from 'services/progress/models';

export const dietReportCommentTextFields: TextFieldSchema[] = [
  {
    required: true,
    name: 'weight',
  },
  {
    required: true,
    name: 'height',
  },
  {
    required: true,
    name: 'waist_size',
  },
  {
    required: true,
    name: 'changes_felt',
  },
  {
    required: true,
    name: 'hunger_level',
  },
  {
    required: true,
    name: 'fullness_level',
  },
  {
    required: true,
    name: 'heavy_meal',
  },
  {
    required: true,
    name: 'average_consumption',
  },
  {
    required: true,
    name: 'water_consumption',
  },
  {
    required: true,
    name: 'physical_activity',
  },
  {
    required: true,
    name: 'time_for_activity',
  },
  {
    required: true,
    name: 'feeling_rating',
  },
  {
    required: true,
    name: 'lesson_learned',
  },
  {
    required: true,
    name: 'problem_faced_and_feedbacks',
  },
];

export const dietReportCommentInitialValues: NutritionistComment = {
  weight: '',
  height: '',
  waist_size: '',
  changes_felt: '',
  hunger_level: '',
  fullness_level: '',
  heavy_meal: '',
  snacks: '',
  average_consumption: '',
  water_consumption: '',
  physical_activity: '',
  time_for_activity: '',
  feeling_rating: '',
  lesson_learned: '',
  problem_faced_and_feedbacks: '',
};

export const fieldValidations: FieldValidation[] = [
  ...dietReportCommentTextFields.map((field) => ({
    name: field.name,
    required: field.required,
    label: 'Komentar',
    type: FieldType.TEXT,
  })),
];
