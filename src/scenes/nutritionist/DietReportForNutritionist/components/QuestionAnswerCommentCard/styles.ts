import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  label: {
    marginBottom: 5,
    marginTop: 5,
  },
  labelContainer: {
    alignSelf: 'stretch',
  },
  answer: {
    flexDirection: 'row',
  },
  nameSize: {
    fontSize: 32,
  },
  bottomMargin: {
    marginBottom: 20,
  },
  smallBottomMargin: {
    marginBottom: 10,
  },
  topMargin: {
    marginTop: 10,
  },
  descMargin: {
    marginTop: 5,
  },
  input: {
    height: 90,
  },
  negativeBottomMargin: {
    marginBottom: -15,
  },
  red: {
    color: 'red',
  },
});
