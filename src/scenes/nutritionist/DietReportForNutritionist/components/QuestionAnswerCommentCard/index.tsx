import { FC } from 'react';
import { View, Text } from 'react-native';

import React from 'react';
import { InfoCard } from 'components/core';
import { typographyStyles } from 'styles';
import { styles } from './styles';
import { dietReportPage } from '../../pages/types';
import { TextField } from 'components/form';

interface Props {
  question: string;
  pageName: string;
  answer: string;
  textInputProps?: any;
  questionName?: string | null;
  lowestScoreDescription?: string | null;
  highestScoreDescription?: string | null;
  testID?: string;
}

const QuestionAnswerCommentCard: FC<Props> = ({
  questionName,
  pageName,
  question,
  answer,
  textInputProps,
  lowestScoreDescription,
  highestScoreDescription,
  testID,
}) => {
  return (
    <View style={styles.labelContainer} testID={testID}>
      {questionName === 'name' ? (
        <Text style={[typographyStyles.headingLarge, styles.nameSize]}>
          {answer}
        </Text>
      ) : null}
      {questionName === 'email' ? (
        <Text style={[typographyStyles.bodySmall, styles.bottomMargin]}>
          {answer}
        </Text>
      ) : null}

      {pageName === dietReportPage.PAGE3 ? (
        <View>
          <Text
            style={[
              typographyStyles.bodyLarge,
              styles.topMargin,
              styles.smallBottomMargin,
            ]}>
            {question}
          </Text>
          <InfoCard content={answer} />
        </View>
      ) : null}

      {pageName !== dietReportPage.PAGE3 &&
      questionName !== 'name' &&
      questionName !== 'email' ? (
        <View>
          <Text style={[typographyStyles.bodyLarge]}>{question}</Text>
          {lowestScoreDescription ? (
            <Text style={[typographyStyles.bodySmall, styles.descMargin]}>
              {lowestScoreDescription}
            </Text>
          ) : null}
          {highestScoreDescription ? (
            <Text
              style={[typographyStyles.bodySmall, styles.smallBottomMargin]}>
              {highestScoreDescription}
            </Text>
          ) : null}
          <InfoCard content={answer} />
          <Text
            style={[
              typographyStyles.bodySmall,
              styles.topMargin,
              styles.negativeBottomMargin,
            ]}>
            Komentar: <Text style={styles.red}>*</Text>
          </Text>
          <TextField
            {...textInputProps}
            placeholder="Tuliskan komentar..."
            multiline={true}
          />
        </View>
      ) : null}
    </View>
  );
};

export default QuestionAnswerCommentCard;
