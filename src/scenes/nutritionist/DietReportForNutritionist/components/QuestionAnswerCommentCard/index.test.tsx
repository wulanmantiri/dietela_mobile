import React from 'react';
import { render } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import QuestionAnswerCommentCard from '.';
import { dietReportPage } from '../../pages/types';

describe('DietReportPage', () => {
  it('name renders correctly', () => {
    const { getByText } = render(
      <QuestionAnswerCommentCard
        pageName={dietReportPage.PAGE1}
        questionName={'name'}
        question={'name'}
        answer={'Ryujin'}
      />,
      ROUTES.clientDietReportNutritionist,
    );
    expect(getByText(/Ryujin/i)).toBeTruthy();
  });

  it('email renders correctly', () => {
    const { getByText } = render(
      <QuestionAnswerCommentCard
        pageName={dietReportPage.PAGE1}
        questionName={'email'}
        question={'email'}
        answer={'ryujin@ryujin.com'}
      />,
      ROUTES.clientDietReportNutritionist,
    );
    expect(getByText(/ryujin@ryujin.com/i)).toBeTruthy();
  });

  it('page 3 renders correctly', () => {
    const { getByText } = render(
      <QuestionAnswerCommentCard
        pageName={dietReportPage.PAGE3}
        questionName={'sweet_beverages'}
        question={'Minuman manis'}
        answer={'4'}
      />,
      ROUTES.clientDietReportNutritionist,
    );
    expect(getByText(/Minuman manis/i)).toBeTruthy();
    expect(getByText(/4/i)).toBeTruthy();
  });

  it('page 2 renders correctly with score description', () => {
    const { getByText } = render(
      <QuestionAnswerCommentCard
        pageName={dietReportPage.PAGE2}
        questionName={'hunger_level'}
        question={
          'Secara rata-rata, sebelum waktu makan selama 1 minggu terakhir ini, dimana level rasa lapar yang Anda rasakan?'
        }
        answer={'4'}
        lowestScoreDescription={'Sangat kelaparan'}
        highestScoreDescription={'Sangat begah (kenyang berlebihan)'}
      />,
      ROUTES.clientDietReportNutritionist,
    );
    expect(getByText(/Secara rata-rata, sebelum waktu makan/i)).toBeTruthy();
    expect(getByText(/4/i)).toBeTruthy();
    expect(getByText(/Sangat kelaparan/i)).toBeTruthy();
    expect(getByText(/Sangat begah/i)).toBeTruthy();
  });

  it('page 2 renders correctly without score description', () => {
    const { getByText } = render(
      <QuestionAnswerCommentCard
        pageName={dietReportPage.PAGE2}
        questionName={'water_consumption'}
        question={
          'Selama 1 minggu terakhir, berapa rata-rata total gelas air putih yang Anda minum?'
        }
        answer={'3'}
      />,
      ROUTES.clientDietReportNutritionist,
    );
    expect(
      getByText(/Selama 1 minggu terakhir, berapa rata-rata total gelas air/i),
    ).toBeTruthy();
    expect(getByText(/3/i)).toBeTruthy();
  });
});
