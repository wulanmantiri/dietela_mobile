import { FC } from 'react';
import { ScrollView, View } from 'react-native';

import { styles } from './styles';
import React from 'react';
import { DietReportPageContent, dietReportPage } from '../../pages/types';
import QuestionAnswerCommentCard from '../QuestionAnswerCommentCard';
import { Text } from 'react-native-elements';
import { typographyStyles } from 'styles';
import { TextField } from 'components/form';

const DietReportPage: FC<{
  content: DietReportPageContent;
  pageName: string;
  getTextInputProps: any;
}> = ({ content, pageName, getTextInputProps }) => {
  return (
    <ScrollView style={styles.container}>
      {pageName === dietReportPage.PAGE3 ? (
        <Text style={[typographyStyles.headingMedium, styles.bottomMargin]}>
          Selama 1 minggu terakhir, berapa rata-rata Anda mengonsumsi jenis
          makanan dibawah ini selama seharian
        </Text>
      ) : null}
      {content.questions.map((qst, idx) => {
        if (pageName === dietReportPage.PAGE3) {
          return (
            <QuestionAnswerCommentCard
              key={idx}
              pageName={pageName}
              questionName={qst.questionName}
              question={qst.label}
              answer={qst.answer}
            />
          );
        }
        return (
          <QuestionAnswerCommentCard
            key={idx}
            pageName={pageName}
            questionName={qst.questionName}
            question={qst.label}
            answer={qst.answer}
            textInputProps={qst.textInputProps}
            lowestScoreDescription={qst.lowestScoreDescription}
            highestScoreDescription={qst.highestScoreDescription}
          />
        );
      })}
      {pageName === dietReportPage.PAGE3 ? (
        <View>
          <Text
            style={[
              typographyStyles.bodySmall,
              styles.topMargin,
              styles.negativeBottomMargin,
            ]}>
            Komentar: <Text style={styles.red}>*</Text>
          </Text>
          <TextField
            {...getTextInputProps('average_consumption')}
            placeholder="Tuliskan komentar..."
            multiline={true}
          />
        </View>
      ) : null}
    </ScrollView>
  );
};

export default DietReportPage;
