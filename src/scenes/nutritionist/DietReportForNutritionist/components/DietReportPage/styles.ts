import { StyleSheet, Dimensions } from 'react-native';

export const styles = StyleSheet.create({
  nameMargin: {
    marginBottom: 15,
  },
  container: {
    width: Dimensions.get('window').width - 40,
  },
  input: {
    height: 90,
  },
  bottomMargin: {
    marginBottom: 10,
  },
  topMargin: {
    marginTop: 10,
  },
  negativeBottomMargin: {
    marginBottom: -15,
  },
  red: {
    color: 'red',
  },
});
