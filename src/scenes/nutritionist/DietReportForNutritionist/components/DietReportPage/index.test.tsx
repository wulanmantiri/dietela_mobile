import React from 'react';
import { render } from 'utils/testing';
import * as ROUTES from 'constants/routes';

import DietReportPage from '.';
import { dietReportPage1 } from '../../pages/DietReportPage1';
import { mockUserReportResponse } from 'mocks/userReport';
import { dietReportPage } from '../../pages/types';
import { dietReportPage2 } from '../../pages/DietReportPage2';
import { dietReportPage3 } from '../../pages/DietReportPage3';
import { dietReportPage4 } from '../../pages/DietReportPage4';

describe('DietReportPage', () => {
  it('diet report page 1 renders correctly', () => {
    render(
      <DietReportPage
        pageName={dietReportPage.PAGE1}
        content={dietReportPage1(mockUserReportResponse, jest.fn())}
        getTextInputProps={jest.fn()}
      />,
      ROUTES.clientDietReportNutritionist,
    );
  });
  it('diet report page 2 renders correctly', () => {
    render(
      <DietReportPage
        pageName={dietReportPage.PAGE2}
        content={dietReportPage2(mockUserReportResponse, jest.fn())}
        getTextInputProps={jest.fn()}
      />,
      ROUTES.clientDietReportNutritionist,
    );
  });
  it('diet report page 3 renders correctly', () => {
    render(
      <DietReportPage
        pageName={dietReportPage.PAGE3}
        content={dietReportPage3(mockUserReportResponse, jest.fn())}
        getTextInputProps={jest.fn()}
      />,
      ROUTES.clientDietReportNutritionist,
    );
  });
  it('diet report page 4 renders correctly', () => {
    render(
      <DietReportPage
        pageName={dietReportPage.PAGE4}
        content={dietReportPage4(mockUserReportResponse, jest.fn())}
        getTextInputProps={jest.fn()}
      />,
      ROUTES.clientDietReportNutritionist,
    );
  });
});
