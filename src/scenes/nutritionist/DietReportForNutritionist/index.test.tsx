import React from 'react';
import { render, waitFor, fireEvent } from 'utils/testing';

import DietReportForNutritionist from '.';
import { mockUserReportResponse } from 'mocks/userReport';
import axios from 'axios';
import * as ROUTES from 'constants/routes';

jest.mock('react-native-toast-message');
jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('DietReportForNutritionist', () => {
  const data = mockUserReportResponse;

  it('renders and submits correctly when given valid comments', async () => {
    const { queryByText, getByText, getAllByPlaceholderText } = render(
      <DietReportForNutritionist />,
      ROUTES.clientDietReportNutritionist,
      {
        routeParams: data,
      },
    );

    const textFieldsPage1 = getAllByPlaceholderText(/Tuliskan komentar.../i);
    textFieldsPage1.forEach((field) => {
      fireEvent.changeText(field, 'comment');
    });

    fireEvent.press(getByText(/Lanjut/i));

    const textFieldsPage2 = getAllByPlaceholderText(/Tuliskan komentar.../i);
    textFieldsPage2.forEach((field) => {
      fireEvent.changeText(field, 'comment (2)');
    });

    fireEvent.press(getByText(/Lanjut/i));

    const textFieldsPage3 = getAllByPlaceholderText(/Tuliskan komentar.../i);
    textFieldsPage3.forEach((field) => {
      fireEvent.changeText(field, 'comment (3)');
    });

    fireEvent.press(getByText(/Lanjut/i));

    const textFieldsPage4 = getAllByPlaceholderText(/Tuliskan komentar.../i);
    textFieldsPage4.forEach((field) => {
      fireEvent.changeText(field, 'comment (4)');
    });

    mockAxios.request.mockImplementationOnce(() =>
      Promise.resolve({
        status: 201,
        data: mockUserReportResponse,
      }),
    );

    const submitButton = getByText('Selesai');
    await waitFor(() => fireEvent.press(submitButton));
    expect(queryByText(/Daftar Klien/i)).toBeTruthy();
  });

  it('renders and does not redirect when api fails', async () => {
    const { queryByText, getByText, getAllByPlaceholderText } = render(
      <DietReportForNutritionist />,
      ROUTES.clientDietReportNutritionist,
      {
        routeParams: data,
      },
    );

    const textFieldsPage1 = getAllByPlaceholderText(/Tuliskan komentar.../i);
    textFieldsPage1.forEach((field) => {
      fireEvent.changeText(field, 'comment');
    });

    fireEvent.press(getByText(/Lanjut/i));

    const textFieldsPage2 = getAllByPlaceholderText(/Tuliskan komentar.../i);
    textFieldsPage2.forEach((field) => {
      fireEvent.changeText(field, 'comment (2)');
    });

    fireEvent.press(getByText(/Lanjut/i));

    const textFieldsPage3 = getAllByPlaceholderText(/Tuliskan komentar.../i);
    textFieldsPage3.forEach((field) => {
      fireEvent.changeText(field, 'comment (3)');
    });

    fireEvent.press(getByText(/Lanjut/i));

    const textFieldsPage4 = getAllByPlaceholderText(/Tuliskan komentar.../i);
    textFieldsPage4.forEach((field) => {
      fireEvent.changeText(field, 'comment (4)');
    });

    mockAxios.request.mockImplementationOnce(() =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      }),
    );

    const submitButton = getByText('Selesai');
    await waitFor(() => fireEvent.press(submitButton));
    expect(queryByText(/Daftar Klien/i)).toBeFalsy();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
});
