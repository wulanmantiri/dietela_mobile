import {
  dietReportTextFields,
  dietReportSelectFields,
} from 'constants/weeklyReport';
import { answerTypes } from 'scenes/questionnaire/ReadOnlyDietProfile/pages/types';
import { getSelectedAnswerString } from 'scenes/questionnaire/ReadOnlyDietProfile/pages/utils';
import { dietReportPage } from './types';

export const getQuestionAnswerCommentString = (
  page: string,
  question: string,
  answer: any,
  answerType: string,
  getTextInputProps: any,
): {
  label: string;
  answer: string;
  textInputProps: any;
  lowestScoreDescription: string | null;
  highestScoreDescription: string | null;
} => {
  let finalLowestScoreDescription = null;
  let finalHighestScoreDescription = null;

  let combinedFields = [];
  if (page === dietReportPage.PAGE1) {
    combinedFields = dietReportTextFields[page];
  } else if (page === dietReportPage.PAGE3) {
    combinedFields = dietReportSelectFields[page];
  } else if (page === dietReportPage.PAGE2 || page === dietReportPage.PAGE4) {
    combinedFields = dietReportSelectFields[page].concat(
      dietReportTextFields[page],
    );
  }

  let finalAnswer = answer;
  if (answerType === answerTypes.SELECT) {
    finalAnswer = getSelectedAnswerString(
      page,
      combinedFields,
      question,
      answer,
    );
    const lowestScore = combinedFields.find((qst) => qst.name === question)!
      .lowestScoreDescription;
    const highestScore = combinedFields.find((qst) => qst.name === question)!
      .highestScoreDescription;
    if (lowestScore && highestScore) {
      finalLowestScoreDescription = lowestScore;
      finalHighestScoreDescription = highestScore;
    }
  }
  if (finalAnswer === '') {
    finalAnswer = '-';
  }

  return {
    label: combinedFields.find((qst) => qst.name === question)!.label,
    answer: finalAnswer,
    textInputProps: getTextInputProps ? getTextInputProps(question) : null,
    lowestScoreDescription: finalLowestScoreDescription,
    highestScoreDescription: finalHighestScoreDescription,
  };
};
