import { DietReportPageContent, dietReportPage } from '../types';
import { answerTypes } from 'scenes/questionnaire/ReadOnlyDietProfile/pages/types';
import { UserReportResponse } from 'services/progress/models';
import { getQuestionAnswerCommentString } from '../utils';

export const dietReportPage3 = (
  {
    sweet_beverages,
    sugary_ingredients,
    fried_snacks,
    umami_snacks,
    sweet_snacks,
    fruits_portion,
    vegetables_portion,
  }: UserReportResponse,
  getTextInputProps: any,
): DietReportPageContent => {
  const page = dietReportPage.PAGE3;
  return {
    questions: [
      getQuestionAnswerCommentString(
        page,
        'sweet_beverages',
        sweet_beverages,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'sugary_ingredients',
        sugary_ingredients,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'fried_snacks',
        fried_snacks,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'umami_snacks',
        umami_snacks,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'sweet_snacks',
        sweet_snacks,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'fruits_portion',
        fruits_portion,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'vegetables_portion',
        vegetables_portion,
        answerTypes.SELECT,
        getTextInputProps,
      ),
    ],
  };
};
