import { DietReportPageContent, dietReportPage } from '../types';
import { answerTypes } from 'scenes/questionnaire/ReadOnlyDietProfile/pages/types';
import { UserReportResponse } from 'services/progress/models';
import { getQuestionAnswerCommentString } from '../utils';

export const dietReportPage1 = (
  { client, weight, height, waist_size }: UserReportResponse,
  getTextInputProps: any,
): DietReportPageContent => {
  const page = dietReportPage.PAGE1;
  return {
    questions: [
      {
        questionName: 'name',
        label: 'Nama Lengkap',
        answer: client.name,
      },
      {
        questionName: 'email',
        label: 'Email',
        answer: client.email,
      },
      getQuestionAnswerCommentString(
        page,
        'weight',
        weight,
        answerTypes.TEXT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'height',
        height,
        answerTypes.TEXT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'waist_size',
        waist_size,
        answerTypes.TEXT,
        getTextInputProps,
      ),
    ],
  };
};
