import { DietReportPageContent, dietReportPage } from '../types';
import { answerTypes } from 'scenes/questionnaire/ReadOnlyDietProfile/pages/types';
import { UserReportResponse } from 'services/progress/models';
import { getQuestionAnswerCommentString } from '../utils';

export const dietReportPage2 = (
  {
    changes_felt,
    hunger_level,
    fullness_level,
    heavy_meal,
    snacks,
    water_consumption,
  }: UserReportResponse,
  getTextInputProps: any,
): DietReportPageContent => {
  const page = dietReportPage.PAGE2;
  return {
    questions: [
      getQuestionAnswerCommentString(
        page,
        'changes_felt',
        changes_felt,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'hunger_level',
        hunger_level,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'fullness_level',
        fullness_level,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'heavy_meal',
        heavy_meal,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'snacks',
        snacks,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'water_consumption',
        water_consumption,
        answerTypes.TEXT,
        getTextInputProps,
      ),
    ],
  };
};
