import { DietReportPageContent, dietReportPage } from '../types';
import { answerTypes } from 'scenes/questionnaire/ReadOnlyDietProfile/pages/types';
import { UserReportResponse } from 'services/progress/models';
import { getQuestionAnswerCommentString } from '../utils';

export const dietReportPage4 = (
  {
    physical_activity,
    time_for_activity,
    feeling_rating,
    lesson_learned,
    problem_faced_and_feedbacks,
  }: UserReportResponse,
  getTextInputProps: any,
): DietReportPageContent => {
  const page = dietReportPage.PAGE4;
  return {
    questions: [
      getQuestionAnswerCommentString(
        page,
        'physical_activity',
        physical_activity,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'time_for_activity',
        time_for_activity,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'feeling_rating',
        feeling_rating,
        answerTypes.SELECT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'lesson_learned',
        lesson_learned,
        answerTypes.TEXT,
        getTextInputProps,
      ),
      getQuestionAnswerCommentString(
        page,
        'problem_faced_and_feedbacks',
        problem_faced_and_feedbacks,
        answerTypes.TEXT,
        getTextInputProps,
      ),
    ],
  };
};
