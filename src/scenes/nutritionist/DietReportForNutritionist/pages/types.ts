export interface DietReportPageContent {
  questions: {
    questionName?: string;
    label: string;
    answer: string;
    textInputProps?: any;
    lowestScoreDescription?: string | null;
    highestScoreDescription?: string | null;
  }[];
}

export const dietReportPage = {
  PAGE1: 'dietReportPage1',
  PAGE2: 'dietReportPage2',
  PAGE3: 'dietReportPage3',
  PAGE4: 'dietReportPage4',
};
