import { dietReportPage1 } from './DietReportPage1';
import { dietReportPage2 } from './DietReportPage2';
import { dietReportPage3 } from './DietReportPage3';
import { dietReportPage4 } from './DietReportPage4';
import { dietReportPage } from './types';

export const pages = [
  { pageName: dietReportPage.PAGE1, pageContent: dietReportPage1 },
  { pageName: dietReportPage.PAGE2, pageContent: dietReportPage2 },
  { pageName: dietReportPage.PAGE3, pageContent: dietReportPage3 },
  { pageName: dietReportPage.PAGE4, pageContent: dietReportPage4 },
];
