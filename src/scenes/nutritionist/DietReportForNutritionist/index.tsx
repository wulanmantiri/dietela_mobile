import React, { FC, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { layoutStyles } from 'styles';
import { pages } from './pages';
import { WizardContainer, Toast } from 'components/core';
import { useForm } from 'hooks';
import { dietReportCommentInitialValues, fieldValidations } from './schema';
import { generateValidationSchema } from 'utils/form';
import { DietReportPage } from './components';
import { useRoute, useNavigation } from '@react-navigation/native';
import { createNutritionistCommentApi } from 'services/progress';
import {
  NutritionistCommentRequest,
  UserReportResponse,
} from 'services/progress/models';
import * as ROUTES from 'constants/routes';
import {
  dietReportTextFields,
  dietReportSelectFields,
} from 'constants/weeklyReport';

interface ParamsDietReport {
  id: number;
}

const DietReportForNutritionist: FC = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const data = route.params as UserReportResponse;

  const [activeSlide, setActiveSlide] = useState(1);

  const {
    getTextInputProps,
    handleSubmit,
    isSubmitting,
    isFieldError,
    values: formValues,
  } = useForm({
    initialValues: dietReportCommentInitialValues,
    validationSchema: generateValidationSchema(fieldValidations),
    onSubmit: async (values) => {
      const payload: NutritionistCommentRequest = {
        weekly_report: data.id,
        ...values,
      };
      const response = await createNutritionistCommentApi(payload);
      if (response.success) {
        Toast.show({
          type: 'success',
          text1: 'Sukses membuat komen',
          text2: 'Komen anda terhadap laporan mingguan klien sudah tersimpan.',
        });
        navigation.navigate(ROUTES.clientListForNutritionist);
      } else {
        Toast.show({
          type: 'error',
          text1: 'Gagal membuat komen',
          text2: 'Komen anda terhadap laporan mingguan klien gagal tersimpan.',
        });
      }
    },
  });

  const userReport = data;

  const isCurrentPageError = (): boolean => {
    if (activeSlide === 1) {
      return [
        formValues.height,
        formValues.weight,
        formValues.waist_size,
      ].reduce((acc: boolean, item) => acc || item === '', false);
    }
    if (activeSlide === 3) {
      return isFieldError('average_consumption');
    }
    const fields = [
      ...dietReportTextFields[`dietReportPage${activeSlide}`],
      ...dietReportSelectFields[`dietReportPage${activeSlide}`],
    ];
    return fields.reduce(
      (acc: boolean, item) => acc || isFieldError(item.name),
      false,
    );
  };

  return (
    <View style={[layoutStyles, styles.reportContainer]}>
      <WizardContainer
        currentStep={activeSlide}
        setCurrentStep={setActiveSlide}
        onFinish={handleSubmit}
        isLoading={isSubmitting}
        isNextDisabled={isCurrentPageError()}
        components={pages.map((page, idx) => (
          <DietReportPage
            key={idx}
            content={page.pageContent(userReport, getTextInputProps)}
            pageName={page.pageName}
            getTextInputProps={getTextInputProps}
          />
        ))}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
  },
  reportContainer: {
    position: 'relative',
    flex: 1,
    paddingBottom: 20,
  },
  searchContainer: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    justifyContent: 'center',
    height: 58,
  },
  backgroundWhite: { backgroundColor: 'white' },
});

export default DietReportForNutritionist;
