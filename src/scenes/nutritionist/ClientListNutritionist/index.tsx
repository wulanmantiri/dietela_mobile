import React, { FC } from 'react';
import * as ROUTES from 'constants/routes';
import { UserRole } from 'services/auth/models';
import { ClientList } from 'components/core';

const ClientListNutritionist: FC = () => {
  return (
    <ClientList
      role={UserRole.NUTRITIONIST}
      clientProfileRoute={ROUTES.clientProfileNutritionist}
      clientDietReportRoute={ROUTES.weeklyReportChooseWeekForNutritionist}
    />
  );
};

export default ClientListNutritionist;
