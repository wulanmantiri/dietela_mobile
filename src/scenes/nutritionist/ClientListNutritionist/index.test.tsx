import React from 'react';
import { render, waitFor } from 'utils/testing';
import * as ROUTES from 'constants/routes';
import axios from 'axios';

import ClientListNutritionist from '.';

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('ClientListNutritionist', () => {
  it('renders correctly', async () => {
    mockAxios.request.mockImplementationOnce(() =>
      Promise.resolve({
        status: 200,
        data: [],
      }),
    );
    render(<ClientListNutritionist />, ROUTES.clientListForNutritionist);
    await waitFor(() => expect(mockAxios.request).toBeCalled());
  });
});
