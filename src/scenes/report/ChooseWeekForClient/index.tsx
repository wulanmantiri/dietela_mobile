import React, { FC, useContext } from 'react';

import ChooseWeek from '../ChooseWeek';
import { Loader } from 'components/core';
import { retrieveUserReportsByIdApi } from 'services/progress';
import { useApi } from 'hooks';
import { UserContext } from 'provider';

const ChooseWeekForClient: FC = () => {
  const { user } = useContext(UserContext);

  const { isLoading, data } = useApi(() => retrieveUserReportsByIdApi(user.id));

  if (isLoading) {
    return <Loader />;
  }

  return <ChooseWeek data={data} />;
};

export default ChooseWeekForClient;
