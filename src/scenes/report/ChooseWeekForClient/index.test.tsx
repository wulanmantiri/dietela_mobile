import React from 'react';
import { render } from '@testing-library/react-native';

import ChooseWeekForClient from '.';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
  };
});

describe('ChooseWeekForClient', () => {
  it('renders correctly', () => {
    render(<ChooseWeekForClient />);
  });
});
