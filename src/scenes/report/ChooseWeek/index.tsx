import React, { FC } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, ScrollView } from 'react-native';
import { Button, Text } from 'react-native-elements';
import * as ROUTES from 'constants/routes';

import { styles } from './styles';
import { Props } from './types';
import { getDateRange } from 'utils/format';
import { EmptyDataPage } from 'components/core';

const ChooseWeek: FC<Props> = ({ data, isNutritionist }) => {
  const redirectViewRoute = isNutritionist
    ? ROUTES.clientDietReportNutritionist
    : ROUTES.weeklyReportReadOnly;
  const navigation = useNavigation();

  if (!data) {
    return <EmptyDataPage text="Laporan mingguan tidak ditemukan" />;
  }

  const getDateRangeForWeek = (weekNum: number) => {
    const { mon, sun } = getDateRange(data.today_week - weekNum);
    return `${mon.format('DD MMM YYYY')} - ${sun.format('DD MMM YYYY')}`;
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.pageContainer}>
        {!isNutritionist && !data.has_submitted_this_week ? (
          <View style={styles.page}>
            <View style={styles.text}>
              <Text style={styles.bold}>Minggu {data.today_week}</Text>
              <Text>{getDateRangeForWeek(data.today_week)}</Text>
            </View>
            <View style={styles.buttonContainer}>
              <Button
                title="Isi"
                onPress={() => navigation.navigate(ROUTES.weeklyReportForm)}
                buttonStyle={styles.button}
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        ) : null}
        {data.data.map((report, i) => (
          <View style={styles.page} key={i}>
            <View style={styles.text}>
              <Text style={styles.bold}>Minggu {report.week_num}</Text>
              <Text>{getDateRangeForWeek(report.week_num)}</Text>
            </View>
            <View style={styles.buttonContainer}>
              <Button
                title="Lihat"
                type="outline"
                onPress={() => navigation.navigate(redirectViewRoute, report)}
                buttonStyle={styles.outlineButtonStyle}
                titleStyle={styles.buttonTitle}
              />
            </View>
          </View>
        ))}
      </View>
    </ScrollView>
  );
};

export default ChooseWeek;
