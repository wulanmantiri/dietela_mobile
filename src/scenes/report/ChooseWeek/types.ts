import { UserReportsResponse } from 'services/progress/models';

export interface Props {
  data: UserReportsResponse | undefined;
  isNutritionist?: boolean;
}
