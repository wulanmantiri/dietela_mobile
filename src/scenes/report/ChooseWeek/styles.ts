import { StyleSheet } from 'react-native';
import { colors, typography } from 'styles';

export const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    paddingHorizontal: 20,
  },
  pageContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  bold: {
    color: colors.textBlack,
    ...typography.headingMedium,
    fontSize: 18,
    marginBottom: 2,
  },
  page: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 14,
    borderBottomColor: colors.border,
    borderBottomWidth: 1,
  },
  buttonContainer: {
    paddingLeft: 10,
    flex: 0.3,
  },
  text: {
    flex: 0.7,
  },
  button: {
    padding: 10,
    paddingHorizontal: 0,
    backgroundColor: colors.primaryYellow,
  },
  outlineButtonStyle: {
    padding: 10,
    paddingHorizontal: 0,
    borderColor: colors.buttonYellow,
    borderWidth: 1,
  },
  buttonTitle: {
    color: colors.textBlack,
  },
});
