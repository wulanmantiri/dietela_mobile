import React from 'react';
import { fireEvent, render } from '@testing-library/react-native';

import ChooseWeek from '.';
import { mockUserReportHistory } from 'mocks/userReport';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
  };
});

describe('ChooseWeek component', () => {
  it('has "Isi" button when form is never filled and accessed by client', () => {
    const { getByText } = render(<ChooseWeek data={mockUserReportHistory} />);
    const fillButton = getByText(/Isi/i);
    expect(fillButton).toBeTruthy();
    fireEvent.press(fillButton);

    expect(mockedNavigate).toHaveBeenCalled();
  });

  it('does not have "Isi" button when form is never filled but accessed by nutritionist', () => {
    const { queryByText } = render(
      <ChooseWeek data={mockUserReportHistory} isNutritionist />,
    );
    expect(queryByText(/Isi/i)).toBeFalsy();
  });

  it('does not have "Isi" button when form is already filled', () => {
    const { queryByText } = render(
      <ChooseWeek
        data={{ ...mockUserReportHistory, has_submitted_this_week: true }}
      />,
    );
    expect(queryByText(/Isi/i)).toBeFalsy();
  });

  it('redirects to designated route when button "Lihat" is pressed', () => {
    const { getAllByText } = render(
      <ChooseWeek data={mockUserReportHistory} />,
    );
    const viewButton = getAllByText(/Lihat/i)[0];
    expect(viewButton).toBeTruthy();
    fireEvent.press(viewButton);

    expect(mockedNavigate).toHaveBeenCalled();
  });
});
