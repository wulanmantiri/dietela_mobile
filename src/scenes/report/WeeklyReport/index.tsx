import React, { FC, useState } from 'react';
import { useNavigation } from '@react-navigation/native';

import * as ROUTES from 'constants/routes';
import { WizardContainer, Toast } from 'components/core';
import {
  dietReportTextFields,
  dietReportSelectFields,
} from 'constants/weeklyReport';
import { useForm } from 'hooks';
import { generateValidationSchema } from 'utils/form';
import { createUserReportApi } from 'services/progress';

import { initialValues, fieldValidations, convertPayload } from './schema';
import { pages } from './components';

const WeeklyReport: FC = () => {
  const [currentPage, setCurrentPage] = useState(1);

  const navigation = useNavigation();
  const {
    getTextInputProps,
    getFormFieldProps,
    isFieldError,
    handleSubmit,
    isSubmitting,
    values: formValues,
  } = useForm({
    initialValues,
    validationSchema: generateValidationSchema(fieldValidations),
    onSubmit: async (values) => {
      const response = await createUserReportApi(convertPayload(values));
      if (response.success && response.data) {
        navigation.reset({
          index: 0,
          routes: [{ name: ROUTES.weeklyReportChooseWeek }],
        });
      } else {
        Toast.show({
          type: 'error',
          text1: 'Gagal menyimpan data',
          text2: 'Terjadi kesalahan pada sisi kami. Silakan coba lagi',
        });
      }
    },
  });

  const isCurrentPageError = (): boolean => {
    if (currentPage === 1) {
      return [
        formValues.height,
        formValues.weight,
        formValues.waist_size,
      ].reduce((acc: boolean, item) => acc || item === '', false);
    }

    const fields = [
      ...(dietReportTextFields[`dietReportPage${currentPage}`] || []),
      ...dietReportSelectFields[`dietReportPage${currentPage}`],
    ];
    return fields.reduce(
      (acc: boolean, item) => acc || isFieldError(item.name),
      false,
    );
  };

  return (
    <WizardContainer
      currentStep={currentPage}
      setCurrentStep={setCurrentPage}
      onFinish={handleSubmit}
      isLoading={isSubmitting}
      isNextDisabled={isCurrentPageError()}
      components={pages.map((Component) => (
        <Component
          getFormFieldProps={getFormFieldProps}
          getTextInputProps={getTextInputProps}
        />
      ))}
    />
  );
};

export default WeeklyReport;
