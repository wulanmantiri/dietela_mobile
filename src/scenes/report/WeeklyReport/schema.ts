import { FieldValidation, FieldType } from 'utils/form';
import {
  dietReportTextFields,
  dietReportSelectFields,
} from 'constants/weeklyReport';
import { UserReportRequest } from 'services/progress/models';

export const initialValues = {
  weight: '',
  height: '',
  waist_size: '',
  changes_felt: 0,
  hunger_level: 0,
  fullness_level: 0,
  heavy_meal: 0,
  snacks: 0,
  sweet_beverages: 0,
  sugary_ingredients: 0,
  fried_snacks: 0,
  umami_snacks: 0,
  sweet_snacks: 0,
  fruits_portion: 0,
  vegetables_portion: 0,
  water_consumption: '',
  physical_activity: [],
  physical_activity_other: '',
  time_for_activity: 0,
  feeling_rating: 0,
  lesson_learned: '',
  problem_faced_and_feedbacks: '',
};

export const fieldValidations: FieldValidation[] = [
  ...Object.values(dietReportTextFields)
    .reduce((acc, arr) => [...acc, ...arr], [])
    .map((field) => ({
      name: field.name,
      required: field.required,
      label: field.label,
      errorMessage: field.errorMessage,
      type: field.fieldType || FieldType.TEXT,
      max: field.max,
    })),
  ...Object.values(dietReportSelectFields)
    .reduce((acc, arr) => [...acc, ...arr], [])
    .map((field) => ({
      name: field.name,
      required: true,
      label: field.label,
      type: field.checkbox ? FieldType.CHECKBOX : FieldType.RADIO_BUTTON,
    })),
];

export const convertPayload = (
  values: typeof initialValues,
): UserReportRequest => ({
  ...values,
  weight: parseInt(values.weight, 10),
  height: parseInt(values.height, 10),
  waist_size: parseInt(values.waist_size, 10),
  water_consumption: parseInt(values.water_consumption, 10),
});
