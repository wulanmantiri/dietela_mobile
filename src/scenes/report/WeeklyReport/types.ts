export interface ReportProps {
  getTextInputProps: (_: string) => any;
  getFormFieldProps: (_: string) => any;
}
