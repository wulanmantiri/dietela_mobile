import React, { FC } from 'react';
import { View } from 'react-native';
import { TextField } from 'components/form';

import { dietReportTextFields } from 'constants/weeklyReport';
import { ReportProps } from '../../types';
import { layoutStyles } from 'styles';

const Report1: FC<ReportProps> = ({ getTextInputProps }) => (
  <View style={layoutStyles}>
    {dietReportTextFields.dietReportPage1.map((props, i) => (
      <TextField
        {...props}
        {...getTextInputProps(props.name)}
        key={`report1-textfield${i}`}
      />
    ))}
  </View>
);

export default Report1;
