import React, { FC } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import { LikertScale } from 'components/form';

import { dietReportSelectFields } from 'constants/weeklyReport';
import { ReportProps } from '../../types';
import { layoutStyles, typographyStyles } from 'styles';

const Report3: FC<ReportProps> = ({ getFormFieldProps }) => (
  <View style={layoutStyles}>
    <Text style={[typographyStyles.bodyMedium, styles.spacing]}>
      Selama 1 minggu terakhir, berapa rata-rata Anda mengonsumsi jenis makanan
      atau minuman dibawah ini selama seharian:
    </Text>
    {dietReportSelectFields.dietReportPage3.map((props, i) => (
      <View
        key={`report3-scale${i}`}
        style={styles.spacing}
        testID={props.name}>
        <LikertScale
          {...props}
          {...getFormFieldProps(props.name)}
          choices={props.choiceList.map((label: string, id: number) => ({
            label,
            value: id + 1,
          }))}
          required
        />
      </View>
    ))}
  </View>
);

export const styles = StyleSheet.create({
  spacing: {
    marginBottom: 24,
  },
});

export default Report3;
