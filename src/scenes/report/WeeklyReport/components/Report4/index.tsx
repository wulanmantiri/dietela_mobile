import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { TextField, CheckboxGroup, RadioButtonGroup } from 'components/form';

import {
  dietReportTextFields,
  dietReportSelectFields,
} from 'constants/weeklyReport';
import { ReportProps } from '../../types';
import { layoutStyles } from 'styles';

const Report4: FC<ReportProps> = ({ getTextInputProps, getFormFieldProps }) => (
  <View style={layoutStyles}>
    {dietReportSelectFields.dietReportPage4
      .filter((item) => item.checkbox)
      .map((props, i) => {
        const textFieldProps = getTextInputProps(`${props.name}_other`);
        return (
          <View key={`report4-checkbox${i}`} style={styles.spacing}>
            <CheckboxGroup
              {...props}
              choices={props.choiceList.map((label: string, id: number) => ({
                label,
                value: id + 1,
              }))}
              {...getFormFieldProps(props.name)}
              hasOtherChoice
              otherValue={textFieldProps.value}
              setOtherValue={textFieldProps.onChangeText}
              required
            />
          </View>
        );
      })}
    {dietReportSelectFields.dietReportPage4
      .filter((item) => !item.checkbox)
      .map((props, i) => (
        <View key={`report4-select${i}`} style={styles.spacing}>
          <RadioButtonGroup
            {...props}
            choices={props.choiceList.map((label: string, id: number) => ({
              label,
              value: id + 1,
            }))}
            {...getFormFieldProps(props.name)}
            required
          />
        </View>
      ))}
    {dietReportTextFields.dietReportPage4.map((props, i) => (
      <TextField
        {...props}
        {...getTextInputProps(props.name)}
        key={`report4-textfield${i}`}
      />
    ))}
  </View>
);

export const styles = StyleSheet.create({
  spacing: {
    marginBottom: 24,
  },
});

export default Report4;
