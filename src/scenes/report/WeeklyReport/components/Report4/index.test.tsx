import React from 'react';
import { render } from '@testing-library/react-native';

import Report4 from '.';

describe('Report4', () => {
  it('renders correctly', () => {
    render(
      <Report4
        getTextInputProps={() => ({
          value: 'hello',
          onChangeText: jest.fn(),
        })}
        getFormFieldProps={() => ({
          value: [],
          onChange: jest.fn(),
        })}
      />,
    );
  });
});
