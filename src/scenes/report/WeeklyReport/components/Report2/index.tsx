import React, { FC } from 'react';
import { View, StyleSheet } from 'react-native';
import { LikertScale, RadioButtonGroup, TextField } from 'components/form';

import {
  dietReportSelectFields,
  dietReportTextFields,
} from 'constants/weeklyReport';
import { ReportProps } from '../../types';
import { layoutStyles } from 'styles';

const Report2: FC<ReportProps> = ({ getTextInputProps, getFormFieldProps }) => (
  <View style={layoutStyles}>
    {dietReportSelectFields.dietReportPage2.map((props, i) => {
      const FormField = props.scaleDescription ? LikertScale : RadioButtonGroup;
      return (
        <View
          key={`report2-select${i}`}
          style={styles.spacing}
          testID={props.name}>
          <FormField
            {...props}
            choices={props.choiceList.map((label: string, id: number) => ({
              label,
              value: id + 1,
            }))}
            {...getFormFieldProps(props.name)}
            required
          />
        </View>
      );
    })}
    {dietReportTextFields.dietReportPage2.map((props, i) => (
      <TextField
        {...props}
        {...getTextInputProps(props.name)}
        key={`report2-textfield${i}`}
      />
    ))}
  </View>
);

export const styles = StyleSheet.create({
  spacing: {
    marginBottom: 24,
  },
});

export default Report2;
