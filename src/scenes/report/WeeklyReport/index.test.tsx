import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import axios from 'axios';

import WeeklyReport from '.';
import {
  dietReportTextFields,
  dietReportSelectFields,
} from 'constants/weeklyReport';
import { RadioButton } from 'components/form';

jest.mock('react-native-toast-message');
jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      reset: mockedNavigate,
    }),
  };
});

describe('WeeklyReport', () => {
  const validFormValues: { [_: string]: any } = {
    weight: '52',
    height: '90',
    waist_size: '100',
    changes_felt: 1,
    hunger_level: 1,
    fullness_level: 1,
    heavy_meal: 1,
    snacks: 1,
    sweet_beverages: 1,
    sugary_ingredients: 1,
    fried_snacks: 1,
    umami_snacks: 1,
    sweet_snacks: 1,
    fruits_portion: 1,
    vegetables_portion: 1,
    water_consumption: '80',
    physical_activity: [1],
    physical_activity_other: '',
    time_for_activity: 1,
    feeling_rating: 1,
    lesson_learned: 'hai',
    problem_faced_and_feedbacks: 'feedback',
  };

  it('initially has disabled next button', () => {
    const { getByText, queryByText } = render(<WeeklyReport />);

    const weightTextField = queryByText(/Berat Badan/i);
    expect(weightTextField).toBeTruthy();

    const nextButton = getByText(/Lanjut/i);
    expect(nextButton).toBeTruthy();
    fireEvent.press(nextButton);

    expect(queryByText(/Berat Badan/i)).toBeTruthy();
  });

  it('redirects to choose week for report if all form values are valid and submit success', async () => {
    const createUserReportApi = () =>
      Promise.resolve({
        status: 201,
        data: validFormValues,
      });
    mockAxios.request.mockImplementationOnce(createUserReportApi);

    const { getByText, getByPlaceholderText, getByTestId } = render(
      <WeeklyReport />,
    );

    dietReportTextFields.dietReportPage1.forEach(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validFormValues[name]);
    });
    fireEvent.press(getByText(/Lanjut/i));

    dietReportTextFields.dietReportPage2.forEach(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validFormValues[name]);
    });
    dietReportSelectFields.dietReportPage2.forEach(({ name }) => {
      const formField = getByTestId(name);
      const firstChoice = formField.findAllByType(RadioButton)[0];
      fireEvent.press(firstChoice);
    });
    fireEvent.press(getByText(/Lanjut/i));

    dietReportSelectFields.dietReportPage3.forEach(({ name }) => {
      const formField = getByTestId(name);
      const firstChoice = formField.findAllByType(RadioButton)[0];
      fireEvent.press(firstChoice);
    });
    fireEvent.press(getByText(/Lanjut/i));

    dietReportSelectFields.dietReportPage4.forEach(({ choiceList }) => {
      const firstChoice = getByText(choiceList[0]);
      fireEvent.press(firstChoice);
    });
    dietReportTextFields.dietReportPage4.forEach(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validFormValues[name]);
    });

    const submitButton = getByText('Selesai');
    await waitFor(() => fireEvent.press(submitButton));

    expect(mockedNavigate).toHaveBeenCalledTimes(1);
  });

  it('does not redirect to choose week for report if all form values are valid but submit fails', async () => {
    const createUserReportApi = () =>
      Promise.reject({
        status: 400,
        response: {
          data: 'error',
        },
      });
    mockAxios.request.mockImplementationOnce(createUserReportApi);

    const { getByText, getByPlaceholderText, getByTestId } = render(
      <WeeklyReport />,
    );

    dietReportTextFields.dietReportPage1.forEach(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validFormValues[name]);
    });
    fireEvent.press(getByText(/Lanjut/i));

    dietReportTextFields.dietReportPage2.forEach(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validFormValues[name]);
    });
    dietReportSelectFields.dietReportPage2.forEach(({ name }) => {
      const formField = getByTestId(name);
      const firstChoice = formField.findAllByType(RadioButton)[0];
      fireEvent.press(firstChoice);
    });
    fireEvent.press(getByText(/Lanjut/i));

    dietReportSelectFields.dietReportPage3.forEach(({ name }) => {
      const formField = getByTestId(name);
      const firstChoice = formField.findAllByType(RadioButton)[0];
      fireEvent.press(firstChoice);
    });
    fireEvent.press(getByText(/Lanjut/i));

    dietReportSelectFields.dietReportPage4.forEach(({ choiceList }) => {
      const firstChoice = getByText(choiceList[0]);
      fireEvent.press(firstChoice);
    });
    dietReportTextFields.dietReportPage4.forEach(({ name, placeholder }) => {
      const formField = getByPlaceholderText(placeholder as string);
      fireEvent.changeText(formField, validFormValues[name]);
    });

    const submitButton = getByText('Selesai');
    await waitFor(() => fireEvent.press(submitButton));

    expect(mockedNavigate).toHaveBeenCalledTimes(1);
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
});
