import React, { FC } from 'react';

import ChooseWeek from '../ChooseWeek';
import { useApi } from 'hooks';
import { retrieveUserReportsByIdApi } from 'services/progress';
import { Loader } from 'components/core';
import { useRoute } from '@react-navigation/native';

const ChooseWeekForNutritionist: FC = () => {
  const route = useRoute();
  const { id } = route.params as { id: number };

  const { isLoading, data } = useApi(() => retrieveUserReportsByIdApi(id));

  if (isLoading) {
    return <Loader />;
  }

  return <ChooseWeek data={data} isNutritionist />;
};

export default ChooseWeekForNutritionist;
