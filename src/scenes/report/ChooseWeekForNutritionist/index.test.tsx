import React from 'react';
import { render } from '@testing-library/react-native';

import ChooseWeekForNutritionist from '.';

const mockedNavigate = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({
      navigate: mockedNavigate,
    }),
    useRoute: () => ({
      params: {
        id: 1,
      },
    }),
  };
});

describe('ChooseWeekForNutritionist', () => {
  it('renders correctly', () => {
    render(<ChooseWeekForNutritionist />);
  });
});
