import { UserReportResponse } from 'services/progress/models';
import {
  getFeeling,
  getPhysicalActivity,
  getTimeForActivity,
} from '../../components/WeeklyReportPage/utils';
import { QuestionComment } from '../types';

export const page4 = (responseData: UserReportResponse): QuestionComment[] => [
  {
    questions: [
      {
        question:
          'Selama 1 minggu terakhir, pilih semua jenis aktivitas atau olahraga yang sudah Anda lakukan',
        answer: getPhysicalActivity(responseData.physical_activity),
      },
    ],
    comment: 'keren bingits',
  },
  {
    questions: [
      {
        question:
          'Selama 1 minggu (7 hari) terakhir, berapa total menit yang Anda habiskan untuk melakukan bergerak aktif dan olahraga di atas dalam seminggu?',
        answer: getTimeForActivity(responseData.time_for_activity),
      },
    ],
    comment: 'keren bingits',
  },
  {
    questions: [
      {
        question:
          'Sejauh ini, bagaimana perasaan Anda dalam mengikuti program?',
        answer: getFeeling(responseData.feeling_rating),
      },
    ],
    comment: 'keren bingits',
  },
  {
    questions: [
      {
        question:
          'Dalam 1 minggu terakhir, Apa saja yang sudah bisa Anda pelajari dari program ini?',
        answer: `${responseData.lesson_learned}`,
      },
    ],
    comment: 'keren bingits',
  },
  {
    questions: [
      {
        question:
          'Silahkan sampaikan disini, jika Anda mempunyai kendala atau keluhan atau kesulitan dalam mengikuti program.',
        answer: `${responseData.problem_faced_and_feedbacks}`,
      },
    ],
    comment: 'keren bingits',
  },
];
