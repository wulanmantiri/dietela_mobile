import { UserReportResponse } from 'services/progress/models';
import { QuestionComment } from '../types';

export const page2 = (reportData: UserReportResponse): QuestionComment[] => [
  {
    questions: [
      {
        question:
          'Apakah sudah mulai terasa ada perubahan ukuran baju atau celana?',
        desc: {
          one: '1. Belum terasa sama sekali',
          two: '5. Sudah sangat berubah',
        },
        answer: `${reportData.changes_felt}`,
      },
    ],
    comment: 'keren bingits',
  },
  {
    questions: [
      {
        question:
          'Secara rata-rata, sebelum waktu makan selama 1 minggu terakhir ini, dimana level rasa lapar yang Anda rasakan?',
        desc: {
          one: '1. Sangat kelaparan',
          two: '10. Sangat begah (kenyang berlebihan)',
        },
        answer: `${reportData.hunger_level}`,
      },
    ],
    comment: 'keren bingits',
  },
  {
    questions: [
      {
        question:
          'Secara rata-rata, saat berhenti makan selama 1 minggu terakhir ini, dimana level rasa kenyang yang Anda rasakan?',
        desc: {
          one: '1. Sangat kelaparan',
          two: '10. Sangat begah (kenyang berlebihan)',
        },
        answer: `${reportData.fullness_level}`,
      },
    ],
    comment: 'keren bingits',
  },
  {
    questions: [
      {
        question:
          'Selama 1 minggu terakhir, secara rata-rata, berapa kali Anda makan berat atau makan utama dalam 1 hari?',
        answer: `${reportData.heavy_meal}x/hari`,
      },
    ],
    comment: 'lere',
  },
  {
    questions: [
      {
        question:
          'Selama 1 minggu terakhir, secara rata-rata, berapa kali Anda makan cemilan dalam 1 hari?',
        answer: `${reportData.snacks}x/hari`,
      },
    ],
    comment: 'keren bingits',
  },
  {
    questions: [
      {
        question:
          'Selama 1 minggu terakhir, berapa rata-rata total gelas air putih yang Anda minum?',
        answer: `${reportData.water_consumption}`,
      },
    ],
    comment: 'keren bingits',
  },
];
