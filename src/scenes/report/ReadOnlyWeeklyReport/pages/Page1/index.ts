import { UserReportResponse } from 'services/progress/models';
import { QuestionComment } from '../types';

export const page1 = (reportData: UserReportResponse): QuestionComment[] => [
  {
    questions: [
      {
        question: 'Berat badan (kg)',
        answer: `${reportData.weight}`,
      },
    ],
    comment: 'keren bingits',
  },
  {
    questions: [
      {
        question: 'Tinggi badan (cm)',
        answer: `${reportData.height}`,
      },
    ],
    comment: 'pertahankan\nnak',
  },
  {
    questions: [
      {
        question: 'Lingkar pinggang (cm)',
        answer: `${reportData.waist_size}`,
      },
    ],
    comment: 'gils',
  },
];
