export interface QuestionItem {
  question: string;
  desc?: {
    one: string;
    two: string;
  };
  answer: string;
}

export interface QuestionComment {
  questions: QuestionItem[];
  comment: string;
}
