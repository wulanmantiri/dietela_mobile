import { UserReportResponse } from 'services/progress/models';
import { QuestionComment } from '../types';

export const page3 = (responseData: UserReportResponse): QuestionComment[] => [
  {
    questions: [
      {
        question: 'Minuman manis (satuan: gelas)',
        answer: `${responseData.sweet_beverages}`,
      },
      {
        question:
          'Gula pasir, gula aren, sirup, selai, atau madu (satuan: sendok makan)',
        answer: `${responseData.sugary_ingredients}`,
      },
      {
        question: 'Cemilan digoreng (satuan: potong)',
        answer: `${responseData.fried_snacks}`,
      },
      {
        question:
          'Makanan ringan asin atau gurih (seperti makanan ringan kemasan, ciki-cikian, keripik) (satuan: bungkus)',
        answer: `${responseData.umami_snacks}`,
      },
      {
        question:
          'Cemilan manis (seperti kue-kue manis, brownis, cake, biskuit, cokelat, wafer) (satuan: potong)',
        answer: `${responseData.sweet_snacks}`,
      },
      {
        question: 'Porsi buah',
        answer: `${responseData.fruits_portion}`,
      },
      {
        question: 'Porsi sayur',
        answer: `${responseData.vegetables_portion}`,
      },
    ],
    comment: 'nais',
  },
];
