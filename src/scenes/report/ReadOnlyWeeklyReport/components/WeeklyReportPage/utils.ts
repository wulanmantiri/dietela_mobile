const physicalActivity = [
  'Hampir tidak pernah olahraga dan/atau duduk lebih dari 9 jam perhari',
  'Jalan kaki santai',
  'Jalan kaki cepat',
  'Pemanasan',
  'Naik turun tangga',
  'Jogging',
  'Treadmill',
  'Senam Aerobic/cardio, Recovery/Scratching, Dance dll',
  'Latihan penguatan otot (strength workout, weight workout)',
  'Other',
];

export const getPhysicalActivity = (activity: number[]) => {
  return activity.map((act, i) =>
    i === activity.length - 1
      ? `- ${physicalActivity[act - 1]}`
      : `- ${physicalActivity[act - 1]}\n`,
  );
};

const timeForActivity = [
  '0 - 60 menit',
  '60 - 100 menit',
  '100 - 120 menit',
  '120 - 150 menit',
  '150 - 175 menit',
  '175 - 200 menit',
  '200 - 250 menit',
  'Lebih dari 250 menit',
];

export const getTimeForActivity = (time: number) => timeForActivity[time - 1];

const feeling = [
  'Rasanya mau menyerah saja',
  'Capek, susah, bosen, males, repot, sibuk',
  'Biasa aja, meski ada kendala tapi semua bisa diatur',
  'Lancar terus, semangat cukup stabil, gak ada masalah',
  'Super seneng, semangat banget, worry-free lah',
];

export const getFeeling = (f: number) => feeling[f - 1];
