import { render } from '@testing-library/react-native';
import React from 'react';

import WeeklyReportPage from '.';
import { QuestionComment } from '../../pages/types';

describe('WeeklyReportPage', () => {
  const questions: QuestionComment[] = [
    {
      questions: [
        {
          question: 'Berat badan (kg)',
          answer: '50',
        },
      ],
      comment: 'keren bingits',
    },
  ];

  const questions2: QuestionComment[] = [
    {
      questions: [
        {
          question:
            'Apakah sudah mulai terasa ada perubahan ukuran baju atau celana?',
          desc: {
            one: '1. Belum terasa sama sekali',
            two: '5. Sudah sangat berubah',
          },
          answer: 'Yes',
        },
      ],
      comment: 'gilaaa',
    },
  ];

  it('renders correctly without title', () => {
    render(<WeeklyReportPage questions={questions} />);
  });

  it('renders correctly with title', () => {
    const title = "Clearly\nYou don't own an air flyer";
    const { queryByText } = render(
      <WeeklyReportPage questions={questions} title={title} />,
    );

    expect(queryByText(title)).toBeTruthy();
  });

  it('renders correctly with description', () => {
    const title = "Clearly\nYou don't own an air flyer";
    const { queryByText } = render(
      <WeeklyReportPage questions={questions2} title={title} />,
    );

    const desc1 = questions2[0].questions[0].desc!.one;
    const desc2 = questions2[0].questions[0].desc!.two;

    expect(queryByText(desc1)).toBeTruthy();
    expect(queryByText(desc2)).toBeTruthy();
  });

  it('displays correct question and comment', () => {
    const title = "Clearly\nYou don't own an air flyer";
    const { queryByText } = render(
      <WeeklyReportPage questions={questions} title={title} />,
    );

    // Should display question one
    const questionOneText = questions[0].questions[0].question;
    const commentOneText = questions[0].comment;

    expect(queryByText(questionOneText)).toBeTruthy();
    expect(queryByText(commentOneText)).toBeTruthy();

    // Should NOT display question two
    const questionTwoText = questions2[0].questions[0].question;
    const commentTwoText = questions2[0].comment;

    expect(queryByText(questionTwoText)).toBeNull();
    expect(queryByText(commentTwoText)).toBeNull();
  });
});
