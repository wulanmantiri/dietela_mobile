import { InfoCard } from 'components/core';
import { Section } from 'components/layout';
import React, { FC } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { layoutStyles, typographyStyles } from 'styles';
import { QuestionComment } from '../../pages/types';

interface Props {
  questions: QuestionComment[];
  title?: string;
}

const WeeklyReportPage: FC<Props> = ({ questions, title }) => {
  return (
    <View style={layoutStyles}>
      <ScrollView>
        {title && <Text style={typographyStyles.headingMedium}>{title}</Text>}
        {questions.map((qs, ii) => (
          <Section key={`s${ii}`}>
            {qs.questions.map((q, jj) => (
              <View key={`q${jj}`} style={styles.text}>
                <View style={styles.text}>
                  <Text style={typographyStyles.bodyLarge}>{q.question}</Text>

                  {q.desc && (
                    <>
                      <Text style={typographyStyles.bodySmall}>
                        {q.desc?.one}
                      </Text>
                      <Text style={typographyStyles.bodySmall}>
                        {q.desc?.two}
                      </Text>
                    </>
                  )}
                </View>
                <InfoCard content={q.answer} />
              </View>
            ))}

            <Section>
              <Text style={typographyStyles.bodySmall}>Komentar:</Text>
              <InfoCard content={qs.comment} />
            </Section>

            <Section />
          </Section>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    marginBottom: 5,
  },
});

export default WeeklyReportPage;
