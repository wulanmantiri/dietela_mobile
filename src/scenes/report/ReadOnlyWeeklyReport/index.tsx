import { useRoute } from '@react-navigation/core';
import { CarouselPagination, EmptyDataPage, Loader } from 'components/core';
import { useApi } from 'hooks';
import React, { FC, useState } from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { retrieveUserReportCommentByReportId } from 'services/progress';
import { UserReportResponse } from 'services/progress/models';
import WeeklyReportPage from './components/WeeklyReportPage';
import { page1 } from './pages/Page1';
import { page2 } from './pages/Page2';
import { page3 } from './pages/Page3';
import { page4 } from './pages/Page4';

const ReadOnlyWeeklyReport: FC = () => {
  const [activeSlide, setActiveSlide] = useState(0);
  const route = useRoute();
  const data = route.params as UserReportResponse;
  const { isLoading, data: commentData } = useApi(() =>
    retrieveUserReportCommentByReportId(data.id),
  );

  const pageComponents = [
    <WeeklyReportPage questions={page1(data)} title="Laporan Diet Anda" />,
    <WeeklyReportPage questions={page2(data)} />,
    <WeeklyReportPage
      questions={page3(data)}
      title="Selama 1 minggu terakhir, berapa rata-rata Anda mengonsumsi jenis makanan dibawah ini selama seharian?"
    />,
    <WeeklyReportPage questions={page4(data)} />,
  ];

  if (isLoading) {
    return <Loader />;
  }

  if (commentData.length === 0) {
    return <EmptyDataPage text="Belum ada komentar dari nutrisionis" />;
  }

  return (
    <>
      <View style={[styles.flexContainer]}>
        <Carousel
          data={pageComponents}
          renderItem={({ item }: any) => item}
          sliderWidth={Dimensions.get('window').width}
          itemWidth={Dimensions.get('window').width}
          onSnapToItem={setActiveSlide}
        />
      </View>
      <CarouselPagination index={activeSlide} length={4} />
    </>
  );
};

const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
  },
});

export default ReadOnlyWeeklyReport;
