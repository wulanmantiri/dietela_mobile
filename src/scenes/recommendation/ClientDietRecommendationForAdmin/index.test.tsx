import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import axios from 'axios';

import ClientDietRecommendationForAdmin from '.';
import { mockDietRecommendation } from 'mocks/dietRecommendation';

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

jest.mock('@react-navigation/native', () => {
  return {
    useRoute: () => ({
      params: {
        id: 1,
      },
    }),
  };
});

describe('ClientDietRecommendationForAdmin', () => {
  const retrievedietRecommendationByIdApi = () =>
    Promise.resolve({
      status: 200,
      data: mockDietRecommendation,
    });

  it('fetches data from backend and renders correctly', async () => {
    mockAxios.request.mockImplementationOnce(retrievedietRecommendationByIdApi);
    render(<ClientDietRecommendationForAdmin />);
    await waitFor(() => expect(mockAxios.request).toBeCalled());
  });
});
