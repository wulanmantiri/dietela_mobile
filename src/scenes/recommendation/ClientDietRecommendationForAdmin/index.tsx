import React, { FC } from 'react';
import { useRoute } from '@react-navigation/native';

import { Loader } from 'components/core';
import ReadOnlyDietRecommendation from 'scenes/questionnaire/ReadOnlyDietRecommendation';
import { useApi } from 'hooks';
import { retrieveDietRecommendationByIdApi } from 'services/dietRecommendation';
import { DietRecommendationResponse } from 'services/dietRecommendation/models';

const ClientDietRecommendationForAdmin: FC = () => {
  const route = useRoute();
  const { id } = route.params as DietRecommendationResponse;

  const { isLoading, data } = useApi(() =>
    retrieveDietRecommendationByIdApi(id),
  );

  if (isLoading) {
    return <Loader />;
  }

  return <ReadOnlyDietRecommendation data={data} />;
};

export default ClientDietRecommendationForAdmin;
