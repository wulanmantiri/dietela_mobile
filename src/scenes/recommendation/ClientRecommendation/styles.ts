import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export const styles = StyleSheet.create({
  buttonStyle: {
    borderColor: colors.buttonYellow,
    borderWidth: 2,
  },
  titleStyle: {
    color: colors.textBlack,
  },
});
