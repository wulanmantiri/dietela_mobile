import React from 'react';
import { render, waitFor } from '@testing-library/react-native';
import axios from 'axios';

import ClientProfile from '.';
import { mockDietRecommendation } from 'mocks/dietRecommendation';

jest.mock('axios');
const mockAxios = axios as jest.Mocked<typeof axios>;

describe('ClientProfile', () => {
  const retrievedietRecommendationApi = () =>
    Promise.resolve({
      status: 200,
      data: [mockDietRecommendation],
    });

  it('fetches data from backend and renders correctly', async () => {
    mockAxios.request.mockImplementationOnce(retrievedietRecommendationApi);
    render(<ClientProfile />);
    await waitFor(() => expect(mockAxios.request).toBeCalled());
  });

  it('fetches empty list from backend and still renders correctly', async () => {
    mockAxios.request.mockImplementationOnce(() =>
      Promise.resolve({
        status: 200,
        data: [],
      }),
    );
    render(<ClientProfile />);
    await waitFor(() => expect(mockAxios.request).toBeCalled());
  });
});
