import React, { FC } from 'react';

import { Loader } from 'components/core';
import ReadOnlyDietRecommendation from 'scenes/questionnaire/ReadOnlyDietRecommendation';
import { useApi } from 'hooks';
import { retrieveDietRecommendationApi } from 'services/dietRecommendation';

const ClientRecommendation: FC = () => {
  const { isLoading, data: recommendation } = useApi(
    retrieveDietRecommendationApi,
  );

  if (isLoading) {
    return <Loader />;
  }
  return (
    <ReadOnlyDietRecommendation
      data={
        recommendation && recommendation.length > 0
          ? recommendation[0]
          : undefined
      }
    />
  );
};

export default ClientRecommendation;
