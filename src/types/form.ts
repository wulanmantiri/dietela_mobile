import { Props as TextFieldProps } from 'components/form/TextField/types';
import { Props as FormLabelProps } from 'components/form/FormLabel/types';
import { Choice } from 'components/form/MultipleChoice/types';
import { FieldType } from 'utils/form';

export type TextFieldSchema = TextFieldProps & {
  name: string;
  fieldType?: FieldType;
  max?: number;
};

export interface RadioButtonGroupSchema extends FormLabelProps {
  choices: Choice[];
  name: string;
}

export interface SelectFieldSchema extends FormLabelProps {
  name: string;
  picker?: boolean;
  placeholder?: string;
  hasOtherChoice?: boolean;
  otherChoiceValue?: number;
  choiceList: string[];
}
