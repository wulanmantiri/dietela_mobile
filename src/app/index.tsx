import React, { FC, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ThemeProvider } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import dayjs from 'dayjs';

import { DietelaCoverLoader, LogoutButton } from 'components/core';
import ContextProvider, { UserContext } from 'provider';
import { theme } from 'styles/theme';

import { screenOptions, toastConfig } from './styles';
import { getNavigation } from './schema';

dayjs.locale('id');

const Stack = createStackNavigator();

const NavigationStack: FC = () => {
  const { isAuthenticated, user, isFirstLoading } = useContext(UserContext);

  const { initialRoute, navigation } = getNavigation(isAuthenticated, user);

  if (isFirstLoading) {
    return <DietelaCoverLoader />;
  }
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={initialRoute}
        screenOptions={screenOptions}>
        {navigation.map((nav, i) => (
          <Stack.Screen
            key={`nav${i}`}
            name={nav.name}
            component={nav.component}
            options={{
              title: nav.header,
              headerShown: Boolean(nav.header),
              headerRight: LogoutButton,
            }}
          />
        ))}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const App: FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <ContextProvider>
        <NavigationStack />
        <Toast config={toastConfig} ref={Toast.setRef} />
      </ContextProvider>
    </ThemeProvider>
  );
};

export default App;
