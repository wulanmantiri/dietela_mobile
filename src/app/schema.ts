import * as ROUTES from 'constants/routes';
import {
  publicNavigation,
  nutritionistNavigation,
  adminNavigation,
  unpaidClientNavigation,
  paidClientNavigation,
  cartsExpireClientNavigation,
  takeQuizClientNavigation,
} from 'constants/navigation';
import { UserRole, AuthUserResponse } from 'services/auth/models';
import { TransactionStatus } from 'services/payment/models';

export const getNavigation = (
  isAuthenticated: boolean,
  user: AuthUserResponse,
) => {
  if (isAuthenticated) {
    if (user.role === UserRole.CLIENT) {
      if (!user.cart_id) {
        return {
          initialRoute: ROUTES.allAccessQuestionnaire,
          navigation: takeQuizClientNavigation,
        };
      }

      if (user.all_carts_are_expired) {
        return {
          initialRoute: ROUTES.allCartsExpired,
          navigation: cartsExpireClientNavigation,
        };
      }

      // Workaround: Need for trigger
      console.log(user);
      console.log(user.transaction_status === TransactionStatus.UNPAID);
      if (user.transaction_status === TransactionStatus.UNPAID) {
        return {
          initialRoute: ROUTES.checkout,
          navigation: unpaidClientNavigation,
        };
      }

      return {
        initialRoute: ROUTES.clientTabNavigation,
        navigation: paidClientNavigation,
      };
    }

    if (user.role === UserRole.NUTRITIONIST) {
      return {
        initialRoute: ROUTES.clientListForNutritionist,
        navigation: nutritionistNavigation,
      };
    }

    if (user.role === UserRole.ADMIN) {
      return {
        initialRoute: ROUTES.clientListForAdmin,
        navigation: adminNavigation,
      };
    }
  }

  return {
    initialRoute: ROUTES.initial,
    navigation: publicNavigation,
  };
};
