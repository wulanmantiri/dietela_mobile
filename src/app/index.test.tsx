import React from 'react';
import { render } from '@testing-library/react-native';

import { ErrorToast } from './styles';
import App from '.';

describe('Application', () => {
  it('renders correctly', () => {
    render(<App />);
  });

  test('error toast renders correctly', () => {
    render(<ErrorToast text1NumberOfLines={2} text2NumberOfLines={2} />);
  });
});
