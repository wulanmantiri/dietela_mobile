import 'react-native-gesture-handler';
import 'dayjs/locale/id';
import { AppRegistry } from 'react-native';
import App from './src/app';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => App);
