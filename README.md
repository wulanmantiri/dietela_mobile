## Dietela Mobile Application

[![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-mobile/badges/staging/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-mobile/-/commits/staging)
[![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-mobile/badges/staging/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-mobile/-/commits/staging)

## About Dietela

Dietela is an Android-based application that provides personalised diet and online nutritionist consultation services.
Check out Dietela app by downloading the latest artifact [here](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-mobile/-/pipelines?page=1&scope=all&ref=staging)!

## Installation Manual

* Install npm (version 6 or later).
* Install yarn by `npm install --global yarn`.
* Setup android development environment by following `React Native CLI Quickstart` guideline [here](https://reactnative.dev/docs/environment-setup).
* Clone the repository.
* Enter the project directory with `cd dietela-mobile`.
* Build the dependencies with `yarn install`.
* Run the app in development node with `yarn start`.
* On another terminal or cmd, build the app by running `yarn android`.
* View it in your physical device or Android emulator.

## Current Features

- PBI 1: Dietela Quiz & Profile Dietku (Client)

    Clients are required to fill out a quiz related to the design needs of the diet program that will be made by Dietela team

- PBI 2: Dietela Quiz & Profile Dietku (Dietela team)

    Dietela team can store client data to provide quiz results containing programs and nutritionists recommendation to clients

- PBI 3: Choose Plan
    
    Clients can continue by choosing recommended diet program that is suitable for their needs

## Developers

Team **we fall in love with PPL we cant have**

- Doan Andreas Nathanael - 1806205123
- Glenda Emanuella Sutanto - 1806133774
- Kefas Satrio Bangkit Solideantyo - 1806204972
- Muzaki Azami Khairevy - 1806205470
- Wulan Mantiri - 1806205666

## Acknowledgements
* CS UI - Software Engineering Project 2021
